﻿namespace ms311as110ui
{
    partial class FormMS311as110
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMS311as110));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelInfoVoiceLength1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelInfo1 = new System.Windows.Forms.Label();
            this.labelFile1 = new System.Windows.Forms.Label();
            this.groupBoxVoice1 = new System.Windows.Forms.GroupBox();
            this.checkBoxEXTPULLHIGH1 = new System.Windows.Forms.CheckBox();
            this.checkBoxNONRETRIG1 = new System.Windows.Forms.CheckBox();
            this.checkBoxLEVELHOLD1 = new System.Windows.Forms.CheckBox();
            this.buttonRemove1 = new System.Windows.Forms.Button();
            this.buttonLoad1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelInfoVoiceLength2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelInfo2 = new System.Windows.Forms.Label();
            this.labelFile2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxEXTPULLHIGH2 = new System.Windows.Forms.CheckBox();
            this.checkBoxNONRETRIG2 = new System.Windows.Forms.CheckBox();
            this.checkBoxLEVELHOLD2 = new System.Windows.Forms.CheckBox();
            this.buttonRemove2 = new System.Windows.Forms.Button();
            this.buttonLoad2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelInfoVoiceLength3 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelInfo3 = new System.Windows.Forms.Label();
            this.labelFile3 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checkBoxEXTPULLHIGH3 = new System.Windows.Forms.CheckBox();
            this.checkBoxNONRETRIG3 = new System.Windows.Forms.CheckBox();
            this.checkBoxLEVELHOLD3 = new System.Windows.Forms.CheckBox();
            this.buttonRemove3 = new System.Windows.Forms.Button();
            this.buttonLoad3 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.labelInfoVoiceLength4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelInfo4 = new System.Windows.Forms.Label();
            this.labelFile4 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.checkBoxEXTPULLHIGH4 = new System.Windows.Forms.CheckBox();
            this.checkBoxNONRETRIG4 = new System.Windows.Forms.CheckBox();
            this.checkBoxLEVELHOLD4 = new System.Windows.Forms.CheckBox();
            this.buttonRemove4 = new System.Windows.Forms.Button();
            this.buttonLoad4 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.labelSenseLess1 = new System.Windows.Forms.Label();
            this.labelSenseMost1 = new System.Windows.Forms.Label();
            this.labelSense1 = new System.Windows.Forms.Label();
            this.radioButtonLEDLEVEL1 = new System.Windows.Forms.RadioButton();
            this.trackBarLED1 = new System.Windows.Forms.TrackBar();
            this.radioButtonLEDFLASH1 = new System.Windows.Forms.RadioButton();
            this.radioButtonLEDLOW1 = new System.Windows.Forms.RadioButton();
            this.radioButtonLEDHIGH1 = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.labelSenseLess2 = new System.Windows.Forms.Label();
            this.labelSenseMost2 = new System.Windows.Forms.Label();
            this.labelSense2 = new System.Windows.Forms.Label();
            this.radioButtonLEVEL2 = new System.Windows.Forms.RadioButton();
            this.trackBarLED2 = new System.Windows.Forms.TrackBar();
            this.radioButtonFLASH2 = new System.Windows.Forms.RadioButton();
            this.radioButtonLEDLOW2 = new System.Windows.Forms.RadioButton();
            this.radioButtonLEDHIGH2 = new System.Windows.Forms.RadioButton();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.checkBoxAUTORPT = new System.Windows.Forms.CheckBox();
            this.checkBox3V = new System.Windows.Forms.CheckBox();
            this.checkBoxKEYOVERW = new System.Windows.Forms.CheckBox();
            this.checkBoxRANDOM = new System.Windows.Forms.CheckBox();
            this.checkBoxSEQ = new System.Windows.Forms.CheckBox();
            this.labelSPI = new System.Windows.Forms.Label();
            this.comboBoxCPUCLK = new System.Windows.Forms.ComboBox();
            this.buttonCLRLOG = new System.Windows.Forms.Button();
            this.buttonLoadConf = new System.Windows.Forms.Button();
            this.buttonSaveConf = new System.Windows.Forms.Button();
            this.buttonGENBIN = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.labelFileSize = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelVolLevel = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.labelVoiceLengthPowerOn = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelInfoPowerOn = new System.Windows.Forms.Label();
            this.labelFilePowerOn = new System.Windows.Forms.Label();
            this.buttonRemovePowerOn = new System.Windows.Forms.Button();
            this.buttonLoadPowerOn = new System.Windows.Forms.Button();
            this.labelConfigFileName = new System.Windows.Forms.Label();
            this.labelOutputFileName = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBoxVoice1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLED1)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLED2)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelInfoVoiceLength1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.labelInfo1);
            this.groupBox1.Controls.Add(this.labelFile1);
            this.groupBox1.Controls.Add(this.groupBoxVoice1);
            this.groupBox1.Controls.Add(this.buttonRemove1);
            this.groupBox1.Controls.Add(this.buttonLoad1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox1.Location = new System.Drawing.Point(16, -2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(548, 159);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "  VOICE 1  ";
            // 
            // labelInfoVoiceLength1
            // 
            this.labelInfoVoiceLength1.AutoSize = true;
            this.labelInfoVoiceLength1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoVoiceLength1.ForeColor = System.Drawing.Color.Black;
            this.labelInfoVoiceLength1.Location = new System.Drawing.Point(225, 128);
            this.labelInfoVoiceLength1.Name = "labelInfoVoiceLength1";
            this.labelInfoVoiceLength1.Size = new System.Drawing.Size(16, 16);
            this.labelInfoVoiceLength1.TabIndex = 7;
            this.labelInfoVoiceLength1.Text = "  ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(110, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "VOICE LENGTH  : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(110, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 16);
            this.label8.TabIndex = 5;
            this.label8.Text = "SAMPLE RATE : ";
            // 
            // labelInfo1
            // 
            this.labelInfo1.AutoSize = true;
            this.labelInfo1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo1.ForeColor = System.Drawing.Color.Black;
            this.labelInfo1.Location = new System.Drawing.Point(216, 110);
            this.labelInfo1.Name = "labelInfo1";
            this.labelInfo1.Size = new System.Drawing.Size(16, 16);
            this.labelInfo1.TabIndex = 4;
            this.labelInfo1.Text = "  ";
            this.labelInfo1.Click += new System.EventHandler(this.labelInfo1_Click);
            // 
            // labelFile1
            // 
            this.labelFile1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFile1.ForeColor = System.Drawing.Color.Black;
            this.labelFile1.Location = new System.Drawing.Point(114, 29);
            this.labelFile1.Name = "labelFile1";
            this.labelFile1.Size = new System.Drawing.Size(251, 81);
            this.labelFile1.TabIndex = 3;
            this.labelFile1.Text = "FILE 1 :";
            // 
            // groupBoxVoice1
            // 
            this.groupBoxVoice1.Controls.Add(this.checkBoxEXTPULLHIGH1);
            this.groupBoxVoice1.Controls.Add(this.checkBoxNONRETRIG1);
            this.groupBoxVoice1.Controls.Add(this.checkBoxLEVELHOLD1);
            this.groupBoxVoice1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBoxVoice1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxVoice1.ForeColor = System.Drawing.Color.Black;
            this.groupBoxVoice1.Location = new System.Drawing.Point(371, 29);
            this.groupBoxVoice1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBoxVoice1.Name = "groupBoxVoice1";
            this.groupBoxVoice1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBoxVoice1.Size = new System.Drawing.Size(166, 113);
            this.groupBoxVoice1.TabIndex = 2;
            this.groupBoxVoice1.TabStop = false;
            this.groupBoxVoice1.Text = "  OPTIONS  ";
            // 
            // checkBoxEXTPULLHIGH1
            // 
            this.checkBoxEXTPULLHIGH1.AutoSize = true;
            this.checkBoxEXTPULLHIGH1.Location = new System.Drawing.Point(16, 85);
            this.checkBoxEXTPULLHIGH1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxEXTPULLHIGH1.Name = "checkBoxEXTPULLHIGH1";
            this.checkBoxEXTPULLHIGH1.Size = new System.Drawing.Size(120, 20);
            this.checkBoxEXTPULLHIGH1.TabIndex = 2;
            this.checkBoxEXTPULLHIGH1.Text = "EXT-PULL-HIGH";
            this.checkBoxEXTPULLHIGH1.UseVisualStyleBackColor = true;
            this.checkBoxEXTPULLHIGH1.CheckedChanged += new System.EventHandler(this.checkBoxEXTPULLHIGH1_CheckedChanged);
            // 
            // checkBoxNONRETRIG1
            // 
            this.checkBoxNONRETRIG1.AutoSize = true;
            this.checkBoxNONRETRIG1.Location = new System.Drawing.Point(16, 57);
            this.checkBoxNONRETRIG1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxNONRETRIG1.Name = "checkBoxNONRETRIG1";
            this.checkBoxNONRETRIG1.Size = new System.Drawing.Size(134, 20);
            this.checkBoxNONRETRIG1.TabIndex = 1;
            this.checkBoxNONRETRIG1.Text = "NON-RETRIGGER";
            this.checkBoxNONRETRIG1.UseVisualStyleBackColor = true;
            this.checkBoxNONRETRIG1.CheckedChanged += new System.EventHandler(this.checkBoxNONRETRIG1_CheckedChanged);
            // 
            // checkBoxLEVELHOLD1
            // 
            this.checkBoxLEVELHOLD1.AutoSize = true;
            this.checkBoxLEVELHOLD1.Location = new System.Drawing.Point(16, 29);
            this.checkBoxLEVELHOLD1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxLEVELHOLD1.Name = "checkBoxLEVELHOLD1";
            this.checkBoxLEVELHOLD1.Size = new System.Drawing.Size(144, 20);
            this.checkBoxLEVELHOLD1.TabIndex = 0;
            this.checkBoxLEVELHOLD1.Text = "LEVEL HOLD PLAY";
            this.checkBoxLEVELHOLD1.UseVisualStyleBackColor = true;
            this.checkBoxLEVELHOLD1.CheckedChanged += new System.EventHandler(this.checkBoxLEVELHOLD1_CheckedChanged);
            // 
            // buttonRemove1
            // 
            this.buttonRemove1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRemove1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemove1.ForeColor = System.Drawing.Color.Black;
            this.buttonRemove1.Location = new System.Drawing.Point(18, 92);
            this.buttonRemove1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonRemove1.Name = "buttonRemove1";
            this.buttonRemove1.Size = new System.Drawing.Size(80, 45);
            this.buttonRemove1.TabIndex = 1;
            this.buttonRemove1.Text = "Remove";
            this.buttonRemove1.UseVisualStyleBackColor = true;
            this.buttonRemove1.Click += new System.EventHandler(this.buttonRemove1_Click);
            // 
            // buttonLoad1
            // 
            this.buttonLoad1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLoad1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoad1.ForeColor = System.Drawing.Color.Black;
            this.buttonLoad1.Location = new System.Drawing.Point(18, 29);
            this.buttonLoad1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonLoad1.Name = "buttonLoad1";
            this.buttonLoad1.Size = new System.Drawing.Size(80, 45);
            this.buttonLoad1.TabIndex = 0;
            this.buttonLoad1.Text = "Load";
            this.buttonLoad1.UseVisualStyleBackColor = true;
            this.buttonLoad1.Click += new System.EventHandler(this.buttonLoad1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelInfoVoiceLength2);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.labelInfo2);
            this.groupBox2.Controls.Add(this.labelFile2);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.buttonRemove2);
            this.groupBox2.Controls.Add(this.buttonLoad2);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox2.Location = new System.Drawing.Point(16, 161);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(548, 159);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " VOICE 2 ";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // labelInfoVoiceLength2
            // 
            this.labelInfoVoiceLength2.AutoSize = true;
            this.labelInfoVoiceLength2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoVoiceLength2.ForeColor = System.Drawing.Color.Black;
            this.labelInfoVoiceLength2.Location = new System.Drawing.Point(225, 126);
            this.labelInfoVoiceLength2.Name = "labelInfoVoiceLength2";
            this.labelInfoVoiceLength2.Size = new System.Drawing.Size(16, 16);
            this.labelInfoVoiceLength2.TabIndex = 9;
            this.labelInfoVoiceLength2.Text = "  ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(115, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "VOICE LENGTH  : ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(114, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 16);
            this.label9.TabIndex = 6;
            this.label9.Text = "SAMPLE RATE :";
            // 
            // labelInfo2
            // 
            this.labelInfo2.AutoSize = true;
            this.labelInfo2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo2.ForeColor = System.Drawing.Color.Black;
            this.labelInfo2.Location = new System.Drawing.Point(216, 106);
            this.labelInfo2.Name = "labelInfo2";
            this.labelInfo2.Size = new System.Drawing.Size(16, 16);
            this.labelInfo2.TabIndex = 4;
            this.labelInfo2.Text = "  ";
            // 
            // labelFile2
            // 
            this.labelFile2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFile2.ForeColor = System.Drawing.Color.Black;
            this.labelFile2.Location = new System.Drawing.Point(114, 32);
            this.labelFile2.Name = "labelFile2";
            this.labelFile2.Size = new System.Drawing.Size(251, 74);
            this.labelFile2.TabIndex = 3;
            this.labelFile2.Text = "FILE 2 :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxEXTPULLHIGH2);
            this.groupBox3.Controls.Add(this.checkBoxNONRETRIG2);
            this.groupBox3.Controls.Add(this.checkBoxLEVELHOLD2);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(371, 30);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Size = new System.Drawing.Size(166, 113);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "  OPTIONS  ";
            // 
            // checkBoxEXTPULLHIGH2
            // 
            this.checkBoxEXTPULLHIGH2.AutoSize = true;
            this.checkBoxEXTPULLHIGH2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxEXTPULLHIGH2.ForeColor = System.Drawing.Color.Black;
            this.checkBoxEXTPULLHIGH2.Location = new System.Drawing.Point(16, 85);
            this.checkBoxEXTPULLHIGH2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxEXTPULLHIGH2.Name = "checkBoxEXTPULLHIGH2";
            this.checkBoxEXTPULLHIGH2.Size = new System.Drawing.Size(120, 20);
            this.checkBoxEXTPULLHIGH2.TabIndex = 2;
            this.checkBoxEXTPULLHIGH2.Text = "EXT-PULL-HIGH";
            this.checkBoxEXTPULLHIGH2.UseVisualStyleBackColor = true;
            this.checkBoxEXTPULLHIGH2.CheckedChanged += new System.EventHandler(this.checkBoxEXTPULLHIGH2_CheckedChanged);
            // 
            // checkBoxNONRETRIG2
            // 
            this.checkBoxNONRETRIG2.AutoSize = true;
            this.checkBoxNONRETRIG2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNONRETRIG2.ForeColor = System.Drawing.Color.Black;
            this.checkBoxNONRETRIG2.Location = new System.Drawing.Point(16, 57);
            this.checkBoxNONRETRIG2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxNONRETRIG2.Name = "checkBoxNONRETRIG2";
            this.checkBoxNONRETRIG2.Size = new System.Drawing.Size(134, 20);
            this.checkBoxNONRETRIG2.TabIndex = 1;
            this.checkBoxNONRETRIG2.Text = "NON-RETRIGGER";
            this.checkBoxNONRETRIG2.UseVisualStyleBackColor = true;
            this.checkBoxNONRETRIG2.CheckedChanged += new System.EventHandler(this.checkBoxNONRETRIG2_CheckedChanged);
            // 
            // checkBoxLEVELHOLD2
            // 
            this.checkBoxLEVELHOLD2.AutoSize = true;
            this.checkBoxLEVELHOLD2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxLEVELHOLD2.ForeColor = System.Drawing.Color.Black;
            this.checkBoxLEVELHOLD2.Location = new System.Drawing.Point(16, 29);
            this.checkBoxLEVELHOLD2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxLEVELHOLD2.Name = "checkBoxLEVELHOLD2";
            this.checkBoxLEVELHOLD2.Size = new System.Drawing.Size(144, 20);
            this.checkBoxLEVELHOLD2.TabIndex = 0;
            this.checkBoxLEVELHOLD2.Text = "LEVEL HOLD PLAY";
            this.checkBoxLEVELHOLD2.UseVisualStyleBackColor = true;
            this.checkBoxLEVELHOLD2.CheckedChanged += new System.EventHandler(this.checkBoxLEVELHOLD2_CheckedChanged);
            // 
            // buttonRemove2
            // 
            this.buttonRemove2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRemove2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemove2.ForeColor = System.Drawing.Color.Black;
            this.buttonRemove2.Location = new System.Drawing.Point(20, 90);
            this.buttonRemove2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonRemove2.Name = "buttonRemove2";
            this.buttonRemove2.Size = new System.Drawing.Size(80, 45);
            this.buttonRemove2.TabIndex = 1;
            this.buttonRemove2.Text = "Remove";
            this.buttonRemove2.UseVisualStyleBackColor = true;
            this.buttonRemove2.Click += new System.EventHandler(this.buttonRemove2_Click);
            // 
            // buttonLoad2
            // 
            this.buttonLoad2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLoad2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoad2.ForeColor = System.Drawing.Color.Black;
            this.buttonLoad2.Location = new System.Drawing.Point(20, 32);
            this.buttonLoad2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonLoad2.Name = "buttonLoad2";
            this.buttonLoad2.Size = new System.Drawing.Size(80, 45);
            this.buttonLoad2.TabIndex = 0;
            this.buttonLoad2.Text = "Load";
            this.buttonLoad2.UseVisualStyleBackColor = true;
            this.buttonLoad2.Click += new System.EventHandler(this.buttonLoad2_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelInfoVoiceLength3);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.labelInfo3);
            this.groupBox4.Controls.Add(this.labelFile3);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.buttonRemove3);
            this.groupBox4.Controls.Add(this.buttonLoad3);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox4.Location = new System.Drawing.Point(16, 324);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Size = new System.Drawing.Size(548, 159);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "  VOICE 3  ";
            // 
            // labelInfoVoiceLength3
            // 
            this.labelInfoVoiceLength3.AutoSize = true;
            this.labelInfoVoiceLength3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoVoiceLength3.ForeColor = System.Drawing.Color.Black;
            this.labelInfoVoiceLength3.Location = new System.Drawing.Point(225, 128);
            this.labelInfoVoiceLength3.Name = "labelInfoVoiceLength3";
            this.labelInfoVoiceLength3.Size = new System.Drawing.Size(16, 16);
            this.labelInfoVoiceLength3.TabIndex = 10;
            this.labelInfoVoiceLength3.Text = "  ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(115, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "VOICE LENGTH  : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(114, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 16);
            this.label10.TabIndex = 6;
            this.label10.Text = "SAMPLE RATE :";
            // 
            // labelInfo3
            // 
            this.labelInfo3.AutoSize = true;
            this.labelInfo3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo3.ForeColor = System.Drawing.Color.Black;
            this.labelInfo3.Location = new System.Drawing.Point(222, 110);
            this.labelInfo3.Name = "labelInfo3";
            this.labelInfo3.Size = new System.Drawing.Size(16, 16);
            this.labelInfo3.TabIndex = 4;
            this.labelInfo3.Text = "  ";
            // 
            // labelFile3
            // 
            this.labelFile3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFile3.ForeColor = System.Drawing.Color.Black;
            this.labelFile3.Location = new System.Drawing.Point(114, 32);
            this.labelFile3.Name = "labelFile3";
            this.labelFile3.Size = new System.Drawing.Size(251, 77);
            this.labelFile3.TabIndex = 3;
            this.labelFile3.Text = "FILE 3 :";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkBoxEXTPULLHIGH3);
            this.groupBox5.Controls.Add(this.checkBoxNONRETRIG3);
            this.groupBox5.Controls.Add(this.checkBoxLEVELHOLD3);
            this.groupBox5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.Black;
            this.groupBox5.Location = new System.Drawing.Point(371, 32);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Size = new System.Drawing.Size(166, 113);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "  OPTIONS  ";
            // 
            // checkBoxEXTPULLHIGH3
            // 
            this.checkBoxEXTPULLHIGH3.AutoSize = true;
            this.checkBoxEXTPULLHIGH3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxEXTPULLHIGH3.ForeColor = System.Drawing.Color.Black;
            this.checkBoxEXTPULLHIGH3.Location = new System.Drawing.Point(16, 85);
            this.checkBoxEXTPULLHIGH3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxEXTPULLHIGH3.Name = "checkBoxEXTPULLHIGH3";
            this.checkBoxEXTPULLHIGH3.Size = new System.Drawing.Size(120, 20);
            this.checkBoxEXTPULLHIGH3.TabIndex = 2;
            this.checkBoxEXTPULLHIGH3.Text = "EXT-PULL-HIGH";
            this.checkBoxEXTPULLHIGH3.UseVisualStyleBackColor = true;
            this.checkBoxEXTPULLHIGH3.CheckedChanged += new System.EventHandler(this.checkBoxEXTPULLHIGH3_CheckedChanged);
            // 
            // checkBoxNONRETRIG3
            // 
            this.checkBoxNONRETRIG3.AutoSize = true;
            this.checkBoxNONRETRIG3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNONRETRIG3.ForeColor = System.Drawing.Color.Black;
            this.checkBoxNONRETRIG3.Location = new System.Drawing.Point(16, 57);
            this.checkBoxNONRETRIG3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxNONRETRIG3.Name = "checkBoxNONRETRIG3";
            this.checkBoxNONRETRIG3.Size = new System.Drawing.Size(134, 20);
            this.checkBoxNONRETRIG3.TabIndex = 1;
            this.checkBoxNONRETRIG3.Text = "NON-RETRIGGER";
            this.checkBoxNONRETRIG3.UseVisualStyleBackColor = true;
            this.checkBoxNONRETRIG3.CheckedChanged += new System.EventHandler(this.checkBoxNONRETRIG3_CheckedChanged);
            // 
            // checkBoxLEVELHOLD3
            // 
            this.checkBoxLEVELHOLD3.AutoSize = true;
            this.checkBoxLEVELHOLD3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxLEVELHOLD3.ForeColor = System.Drawing.Color.Black;
            this.checkBoxLEVELHOLD3.Location = new System.Drawing.Point(16, 30);
            this.checkBoxLEVELHOLD3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxLEVELHOLD3.Name = "checkBoxLEVELHOLD3";
            this.checkBoxLEVELHOLD3.Size = new System.Drawing.Size(144, 20);
            this.checkBoxLEVELHOLD3.TabIndex = 0;
            this.checkBoxLEVELHOLD3.Text = "LEVEL HOLD PLAY";
            this.checkBoxLEVELHOLD3.UseVisualStyleBackColor = true;
            this.checkBoxLEVELHOLD3.CheckedChanged += new System.EventHandler(this.checkBoxLEVELHOLD3_CheckedChanged);
            // 
            // buttonRemove3
            // 
            this.buttonRemove3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRemove3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemove3.ForeColor = System.Drawing.Color.Black;
            this.buttonRemove3.Location = new System.Drawing.Point(20, 92);
            this.buttonRemove3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonRemove3.Name = "buttonRemove3";
            this.buttonRemove3.Size = new System.Drawing.Size(80, 45);
            this.buttonRemove3.TabIndex = 1;
            this.buttonRemove3.Text = "Remove";
            this.buttonRemove3.UseVisualStyleBackColor = true;
            this.buttonRemove3.Click += new System.EventHandler(this.buttonRemove3_Click);
            // 
            // buttonLoad3
            // 
            this.buttonLoad3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLoad3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoad3.ForeColor = System.Drawing.Color.Black;
            this.buttonLoad3.Location = new System.Drawing.Point(20, 30);
            this.buttonLoad3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonLoad3.Name = "buttonLoad3";
            this.buttonLoad3.Size = new System.Drawing.Size(80, 45);
            this.buttonLoad3.TabIndex = 0;
            this.buttonLoad3.Text = "Load";
            this.buttonLoad3.UseVisualStyleBackColor = true;
            this.buttonLoad3.Click += new System.EventHandler(this.buttonLoad3_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.labelInfoVoiceLength4);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.labelInfo4);
            this.groupBox6.Controls.Add(this.labelFile4);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.buttonRemove4);
            this.groupBox6.Controls.Add(this.buttonLoad4);
            this.groupBox6.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox6.Location = new System.Drawing.Point(16, 487);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Size = new System.Drawing.Size(548, 159);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "  VOICE4  ";
            // 
            // labelInfoVoiceLength4
            // 
            this.labelInfoVoiceLength4.AutoSize = true;
            this.labelInfoVoiceLength4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoVoiceLength4.ForeColor = System.Drawing.Color.Black;
            this.labelInfoVoiceLength4.Location = new System.Drawing.Point(225, 132);
            this.labelInfoVoiceLength4.Name = "labelInfoVoiceLength4";
            this.labelInfoVoiceLength4.Size = new System.Drawing.Size(16, 16);
            this.labelInfoVoiceLength4.TabIndex = 12;
            this.labelInfoVoiceLength4.Text = "  ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(115, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "VOICE LENGTH  : ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(114, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 16);
            this.label11.TabIndex = 7;
            this.label11.Text = "SAMPLE RATE :";
            // 
            // labelInfo4
            // 
            this.labelInfo4.AutoSize = true;
            this.labelInfo4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo4.ForeColor = System.Drawing.Color.Black;
            this.labelInfo4.Location = new System.Drawing.Point(218, 113);
            this.labelInfo4.Name = "labelInfo4";
            this.labelInfo4.Size = new System.Drawing.Size(16, 16);
            this.labelInfo4.TabIndex = 4;
            this.labelInfo4.Text = "  ";
            // 
            // labelFile4
            // 
            this.labelFile4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFile4.ForeColor = System.Drawing.Color.Black;
            this.labelFile4.Location = new System.Drawing.Point(114, 36);
            this.labelFile4.Name = "labelFile4";
            this.labelFile4.Size = new System.Drawing.Size(251, 77);
            this.labelFile4.TabIndex = 3;
            this.labelFile4.Text = "FILE 4 :";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.checkBoxEXTPULLHIGH4);
            this.groupBox7.Controls.Add(this.checkBoxNONRETRIG4);
            this.groupBox7.Controls.Add(this.checkBoxLEVELHOLD4);
            this.groupBox7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.ForeColor = System.Drawing.Color.Black;
            this.groupBox7.Location = new System.Drawing.Point(371, 30);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox7.Size = new System.Drawing.Size(166, 113);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "  OPTIONS  ";
            // 
            // checkBoxEXTPULLHIGH4
            // 
            this.checkBoxEXTPULLHIGH4.AutoSize = true;
            this.checkBoxEXTPULLHIGH4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxEXTPULLHIGH4.ForeColor = System.Drawing.Color.Black;
            this.checkBoxEXTPULLHIGH4.Location = new System.Drawing.Point(16, 80);
            this.checkBoxEXTPULLHIGH4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxEXTPULLHIGH4.Name = "checkBoxEXTPULLHIGH4";
            this.checkBoxEXTPULLHIGH4.Size = new System.Drawing.Size(120, 20);
            this.checkBoxEXTPULLHIGH4.TabIndex = 2;
            this.checkBoxEXTPULLHIGH4.Text = "EXT-PULL-HIGH";
            this.checkBoxEXTPULLHIGH4.UseVisualStyleBackColor = true;
            this.checkBoxEXTPULLHIGH4.CheckedChanged += new System.EventHandler(this.checkBoxEXTPULLHIGH4_CheckedChanged);
            // 
            // checkBoxNONRETRIG4
            // 
            this.checkBoxNONRETRIG4.AutoSize = true;
            this.checkBoxNONRETRIG4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNONRETRIG4.ForeColor = System.Drawing.Color.Black;
            this.checkBoxNONRETRIG4.Location = new System.Drawing.Point(16, 52);
            this.checkBoxNONRETRIG4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxNONRETRIG4.Name = "checkBoxNONRETRIG4";
            this.checkBoxNONRETRIG4.Size = new System.Drawing.Size(134, 20);
            this.checkBoxNONRETRIG4.TabIndex = 1;
            this.checkBoxNONRETRIG4.Text = "NON-RETRIGGER";
            this.checkBoxNONRETRIG4.UseVisualStyleBackColor = true;
            this.checkBoxNONRETRIG4.CheckedChanged += new System.EventHandler(this.checkBoxNONRETRIG4_CheckedChanged);
            // 
            // checkBoxLEVELHOLD4
            // 
            this.checkBoxLEVELHOLD4.AutoSize = true;
            this.checkBoxLEVELHOLD4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxLEVELHOLD4.ForeColor = System.Drawing.Color.Black;
            this.checkBoxLEVELHOLD4.Location = new System.Drawing.Point(16, 23);
            this.checkBoxLEVELHOLD4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxLEVELHOLD4.Name = "checkBoxLEVELHOLD4";
            this.checkBoxLEVELHOLD4.Size = new System.Drawing.Size(144, 20);
            this.checkBoxLEVELHOLD4.TabIndex = 0;
            this.checkBoxLEVELHOLD4.Text = "LEVEL HOLD PLAY";
            this.checkBoxLEVELHOLD4.UseVisualStyleBackColor = true;
            this.checkBoxLEVELHOLD4.CheckedChanged += new System.EventHandler(this.checkBoxLEVELHOLD4_CheckedChanged);
            // 
            // buttonRemove4
            // 
            this.buttonRemove4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRemove4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemove4.ForeColor = System.Drawing.Color.Black;
            this.buttonRemove4.Location = new System.Drawing.Point(20, 96);
            this.buttonRemove4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonRemove4.Name = "buttonRemove4";
            this.buttonRemove4.Size = new System.Drawing.Size(80, 45);
            this.buttonRemove4.TabIndex = 1;
            this.buttonRemove4.Text = "Remove";
            this.buttonRemove4.UseVisualStyleBackColor = true;
            this.buttonRemove4.Click += new System.EventHandler(this.buttonRemove4_Click);
            // 
            // buttonLoad4
            // 
            this.buttonLoad4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLoad4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoad4.ForeColor = System.Drawing.Color.Black;
            this.buttonLoad4.Location = new System.Drawing.Point(20, 36);
            this.buttonLoad4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonLoad4.Name = "buttonLoad4";
            this.buttonLoad4.Size = new System.Drawing.Size(80, 45);
            this.buttonLoad4.TabIndex = 0;
            this.buttonLoad4.Text = "Load";
            this.buttonLoad4.UseVisualStyleBackColor = true;
            this.buttonLoad4.Click += new System.EventHandler(this.buttonLoad4_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.labelSenseLess1);
            this.groupBox8.Controls.Add(this.labelSenseMost1);
            this.groupBox8.Controls.Add(this.labelSense1);
            this.groupBox8.Controls.Add(this.radioButtonLEDLEVEL1);
            this.groupBox8.Controls.Add(this.trackBarLED1);
            this.groupBox8.Controls.Add(this.radioButtonLEDFLASH1);
            this.groupBox8.Controls.Add(this.radioButtonLEDLOW1);
            this.groupBox8.Controls.Add(this.radioButtonLEDHIGH1);
            this.groupBox8.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox8.Location = new System.Drawing.Point(585, 164);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox8.Size = new System.Drawing.Size(192, 207);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "   LED1 OPTIONS  ";
            // 
            // labelSenseLess1
            // 
            this.labelSenseLess1.AutoSize = true;
            this.labelSenseLess1.Enabled = false;
            this.labelSenseLess1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSenseLess1.ForeColor = System.Drawing.Color.Black;
            this.labelSenseLess1.Location = new System.Drawing.Point(150, 180);
            this.labelSenseLess1.Name = "labelSenseLess1";
            this.labelSenseLess1.Size = new System.Drawing.Size(36, 16);
            this.labelSenseLess1.TabIndex = 15;
            this.labelSenseLess1.Text = "Less";
            // 
            // labelSenseMost1
            // 
            this.labelSenseMost1.AutoSize = true;
            this.labelSenseMost1.Enabled = false;
            this.labelSenseMost1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSenseMost1.ForeColor = System.Drawing.Color.Black;
            this.labelSenseMost1.Location = new System.Drawing.Point(6, 180);
            this.labelSenseMost1.Name = "labelSenseMost1";
            this.labelSenseMost1.Size = new System.Drawing.Size(37, 16);
            this.labelSenseMost1.TabIndex = 14;
            this.labelSenseMost1.Text = "Most";
            // 
            // labelSense1
            // 
            this.labelSense1.AutoSize = true;
            this.labelSense1.Enabled = false;
            this.labelSense1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSense1.ForeColor = System.Drawing.Color.Black;
            this.labelSense1.Location = new System.Drawing.Point(3, 133);
            this.labelSense1.Name = "labelSense1";
            this.labelSense1.Size = new System.Drawing.Size(71, 16);
            this.labelSense1.TabIndex = 13;
            this.labelSense1.Text = " Sensitivity";
            this.labelSense1.Click += new System.EventHandler(this.label1_Click);
            // 
            // radioButtonLEDLEVEL1
            // 
            this.radioButtonLEDLEVEL1.AutoSize = true;
            this.radioButtonLEDLEVEL1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLEDLEVEL1.ForeColor = System.Drawing.Color.Black;
            this.radioButtonLEDLEVEL1.Location = new System.Drawing.Point(23, 105);
            this.radioButtonLEDLEVEL1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioButtonLEDLEVEL1.Name = "radioButtonLEDLEVEL1";
            this.radioButtonLEDLEVEL1.Size = new System.Drawing.Size(155, 20);
            this.radioButtonLEDLEVEL1.TabIndex = 4;
            this.radioButtonLEDLEVEL1.Text = "Flash base on Volume";
            this.radioButtonLEDLEVEL1.UseVisualStyleBackColor = true;
            this.radioButtonLEDLEVEL1.CheckedChanged += new System.EventHandler(this.radioButtonLEDLEVEL1_CheckedChanged);
            // 
            // trackBarLED1
            // 
            this.trackBarLED1.Enabled = false;
            this.trackBarLED1.Location = new System.Drawing.Point(6, 151);
            this.trackBarLED1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trackBarLED1.Maximum = 15;
            this.trackBarLED1.Name = "trackBarLED1";
            this.trackBarLED1.Size = new System.Drawing.Size(177, 45);
            this.trackBarLED1.TabIndex = 3;
            this.trackBarLED1.Scroll += new System.EventHandler(this.trackBarLED1_Scroll);
            // 
            // radioButtonLEDFLASH1
            // 
            this.radioButtonLEDFLASH1.AutoSize = true;
            this.radioButtonLEDFLASH1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLEDFLASH1.ForeColor = System.Drawing.Color.Black;
            this.radioButtonLEDFLASH1.Location = new System.Drawing.Point(23, 78);
            this.radioButtonLEDFLASH1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioButtonLEDFLASH1.Name = "radioButtonLEDFLASH1";
            this.radioButtonLEDFLASH1.Size = new System.Drawing.Size(139, 20);
            this.radioButtonLEDFLASH1.TabIndex = 2;
            this.radioButtonLEDFLASH1.Text = "Flash when Playing";
            this.radioButtonLEDFLASH1.UseVisualStyleBackColor = true;
            this.radioButtonLEDFLASH1.CheckedChanged += new System.EventHandler(this.radioButtonLEDFLASH1_CheckedChanged);
            // 
            // radioButtonLEDLOW1
            // 
            this.radioButtonLEDLOW1.AutoSize = true;
            this.radioButtonLEDLOW1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLEDLOW1.ForeColor = System.Drawing.Color.Black;
            this.radioButtonLEDLOW1.Location = new System.Drawing.Point(23, 52);
            this.radioButtonLEDLOW1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioButtonLEDLOW1.Name = "radioButtonLEDLOW1";
            this.radioButtonLEDLOW1.Size = new System.Drawing.Size(159, 20);
            this.radioButtonLEDLOW1.TabIndex = 1;
            this.radioButtonLEDLOW1.Text = "ON (Low) when playing";
            this.radioButtonLEDLOW1.UseVisualStyleBackColor = true;
            this.radioButtonLEDLOW1.CheckedChanged += new System.EventHandler(this.radioButtonLEDLOW1_CheckedChanged);
            // 
            // radioButtonLEDHIGH1
            // 
            this.radioButtonLEDHIGH1.AutoSize = true;
            this.radioButtonLEDHIGH1.Checked = true;
            this.radioButtonLEDHIGH1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLEDHIGH1.ForeColor = System.Drawing.Color.Black;
            this.radioButtonLEDHIGH1.Location = new System.Drawing.Point(23, 26);
            this.radioButtonLEDHIGH1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioButtonLEDHIGH1.Name = "radioButtonLEDHIGH1";
            this.radioButtonLEDHIGH1.Size = new System.Drawing.Size(52, 20);
            this.radioButtonLEDHIGH1.TabIndex = 0;
            this.radioButtonLEDHIGH1.TabStop = true;
            this.radioButtonLEDHIGH1.Text = "OFF";
            this.radioButtonLEDHIGH1.UseVisualStyleBackColor = true;
            this.radioButtonLEDHIGH1.CheckedChanged += new System.EventHandler(this.radioButtonLEDHIGH1_CheckedChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.labelSenseLess2);
            this.groupBox9.Controls.Add(this.labelSenseMost2);
            this.groupBox9.Controls.Add(this.labelSense2);
            this.groupBox9.Controls.Add(this.radioButtonLEVEL2);
            this.groupBox9.Controls.Add(this.trackBarLED2);
            this.groupBox9.Controls.Add(this.radioButtonFLASH2);
            this.groupBox9.Controls.Add(this.radioButtonLEDLOW2);
            this.groupBox9.Controls.Add(this.radioButtonLEDHIGH2);
            this.groupBox9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox9.Location = new System.Drawing.Point(585, 382);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox9.Size = new System.Drawing.Size(192, 210);
            this.groupBox9.TabIndex = 5;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "   LED2 OPTIONS   ";
            // 
            // labelSenseLess2
            // 
            this.labelSenseLess2.AutoSize = true;
            this.labelSenseLess2.Enabled = false;
            this.labelSenseLess2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSenseLess2.ForeColor = System.Drawing.Color.Black;
            this.labelSenseLess2.Location = new System.Drawing.Point(147, 185);
            this.labelSenseLess2.Name = "labelSenseLess2";
            this.labelSenseLess2.Size = new System.Drawing.Size(36, 16);
            this.labelSenseLess2.TabIndex = 7;
            this.labelSenseLess2.Text = "Less";
            // 
            // labelSenseMost2
            // 
            this.labelSenseMost2.AutoSize = true;
            this.labelSenseMost2.Enabled = false;
            this.labelSenseMost2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSenseMost2.ForeColor = System.Drawing.Color.Black;
            this.labelSenseMost2.Location = new System.Drawing.Point(6, 185);
            this.labelSenseMost2.Name = "labelSenseMost2";
            this.labelSenseMost2.Size = new System.Drawing.Size(37, 16);
            this.labelSenseMost2.TabIndex = 6;
            this.labelSenseMost2.Text = "Most";
            // 
            // labelSense2
            // 
            this.labelSense2.AutoSize = true;
            this.labelSense2.Enabled = false;
            this.labelSense2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSense2.ForeColor = System.Drawing.Color.Black;
            this.labelSense2.Location = new System.Drawing.Point(6, 136);
            this.labelSense2.Name = "labelSense2";
            this.labelSense2.Size = new System.Drawing.Size(67, 16);
            this.labelSense2.TabIndex = 5;
            this.labelSense2.Text = "Sensitivity";
            // 
            // radioButtonLEVEL2
            // 
            this.radioButtonLEVEL2.AutoSize = true;
            this.radioButtonLEVEL2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLEVEL2.ForeColor = System.Drawing.Color.Black;
            this.radioButtonLEVEL2.Location = new System.Drawing.Point(15, 109);
            this.radioButtonLEVEL2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioButtonLEVEL2.Name = "radioButtonLEVEL2";
            this.radioButtonLEVEL2.Size = new System.Drawing.Size(155, 20);
            this.radioButtonLEVEL2.TabIndex = 4;
            this.radioButtonLEVEL2.Text = "Flash base on Volume";
            this.radioButtonLEVEL2.UseVisualStyleBackColor = true;
            this.radioButtonLEVEL2.CheckedChanged += new System.EventHandler(this.radioButtonLEVEL2_CheckedChanged);
            // 
            // trackBarLED2
            // 
            this.trackBarLED2.Enabled = false;
            this.trackBarLED2.Location = new System.Drawing.Point(6, 156);
            this.trackBarLED2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trackBarLED2.Maximum = 15;
            this.trackBarLED2.Name = "trackBarLED2";
            this.trackBarLED2.Size = new System.Drawing.Size(177, 45);
            this.trackBarLED2.TabIndex = 3;
            this.trackBarLED2.Scroll += new System.EventHandler(this.trackBarLED2_Scroll);
            // 
            // radioButtonFLASH2
            // 
            this.radioButtonFLASH2.AutoSize = true;
            this.radioButtonFLASH2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonFLASH2.ForeColor = System.Drawing.Color.Black;
            this.radioButtonFLASH2.Location = new System.Drawing.Point(15, 81);
            this.radioButtonFLASH2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioButtonFLASH2.Name = "radioButtonFLASH2";
            this.radioButtonFLASH2.Size = new System.Drawing.Size(139, 20);
            this.radioButtonFLASH2.TabIndex = 2;
            this.radioButtonFLASH2.Text = "Flash when Playing";
            this.radioButtonFLASH2.UseVisualStyleBackColor = true;
            this.radioButtonFLASH2.CheckedChanged += new System.EventHandler(this.radioButtonFLASH2_CheckedChanged);
            // 
            // radioButtonLEDLOW2
            // 
            this.radioButtonLEDLOW2.AutoSize = true;
            this.radioButtonLEDLOW2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLEDLOW2.ForeColor = System.Drawing.Color.Black;
            this.radioButtonLEDLOW2.Location = new System.Drawing.Point(15, 55);
            this.radioButtonLEDLOW2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioButtonLEDLOW2.Name = "radioButtonLEDLOW2";
            this.radioButtonLEDLOW2.Size = new System.Drawing.Size(159, 20);
            this.radioButtonLEDLOW2.TabIndex = 1;
            this.radioButtonLEDLOW2.Text = "ON (Low) when playing";
            this.radioButtonLEDLOW2.UseVisualStyleBackColor = true;
            this.radioButtonLEDLOW2.CheckedChanged += new System.EventHandler(this.radioButtonLEDLOW2_CheckedChanged);
            // 
            // radioButtonLEDHIGH2
            // 
            this.radioButtonLEDHIGH2.AutoSize = true;
            this.radioButtonLEDHIGH2.Checked = true;
            this.radioButtonLEDHIGH2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLEDHIGH2.ForeColor = System.Drawing.Color.Black;
            this.radioButtonLEDHIGH2.Location = new System.Drawing.Point(15, 27);
            this.radioButtonLEDHIGH2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radioButtonLEDHIGH2.Name = "radioButtonLEDHIGH2";
            this.radioButtonLEDHIGH2.Size = new System.Drawing.Size(52, 20);
            this.radioButtonLEDHIGH2.TabIndex = 0;
            this.radioButtonLEDHIGH2.TabStop = true;
            this.radioButtonLEDHIGH2.Text = "OFF";
            this.radioButtonLEDHIGH2.UseVisualStyleBackColor = true;
            this.radioButtonLEDHIGH2.CheckedChanged += new System.EventHandler(this.radioButtonLEDHIGH2_CheckedChanged);
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(345, 701);
            this.textBoxLog.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.Size = new System.Drawing.Size(192, 121);
            this.textBoxLog.TabIndex = 6;
            this.textBoxLog.Text = "LOG :\r\n";
            this.textBoxLog.Visible = false;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.checkBoxAUTORPT);
            this.groupBox10.Controls.Add(this.checkBox3V);
            this.groupBox10.Controls.Add(this.checkBoxKEYOVERW);
            this.groupBox10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(795, 264);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox10.Size = new System.Drawing.Size(191, 107);
            this.groupBox10.TabIndex = 7;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "  GENERAL OPTIONS  ";
            // 
            // checkBoxAUTORPT
            // 
            this.checkBoxAUTORPT.AutoSize = true;
            this.checkBoxAUTORPT.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAUTORPT.Location = new System.Drawing.Point(23, 75);
            this.checkBoxAUTORPT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxAUTORPT.Name = "checkBoxAUTORPT";
            this.checkBoxAUTORPT.Size = new System.Drawing.Size(99, 20);
            this.checkBoxAUTORPT.TabIndex = 2;
            this.checkBoxAUTORPT.Text = "Auto Repeat";
            this.checkBoxAUTORPT.UseVisualStyleBackColor = true;
            this.checkBoxAUTORPT.CheckedChanged += new System.EventHandler(this.checkBoxAUTORPT_CheckedChanged);
            // 
            // checkBox3V
            // 
            this.checkBox3V.AutoSize = true;
            this.checkBox3V.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox3V.Location = new System.Drawing.Point(23, 48);
            this.checkBox3V.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBox3V.Name = "checkBox3V";
            this.checkBox3V.Size = new System.Drawing.Size(100, 20);
            this.checkBox3V.TabIndex = 1;
            this.checkBox3V.Text = "3V operation";
            this.checkBox3V.UseVisualStyleBackColor = true;
            this.checkBox3V.CheckedChanged += new System.EventHandler(this.checkBox3V_CheckedChanged);
            // 
            // checkBoxKEYOVERW
            // 
            this.checkBoxKEYOVERW.AutoSize = true;
            this.checkBoxKEYOVERW.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxKEYOVERW.Location = new System.Drawing.Point(23, 22);
            this.checkBoxKEYOVERW.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxKEYOVERW.Name = "checkBoxKEYOVERW";
            this.checkBoxKEYOVERW.Size = new System.Drawing.Size(101, 20);
            this.checkBoxKEYOVERW.TabIndex = 0;
            this.checkBoxKEYOVERW.Text = "Key Override";
            this.checkBoxKEYOVERW.UseVisualStyleBackColor = true;
            this.checkBoxKEYOVERW.CheckedChanged += new System.EventHandler(this.checkBoxKEYOVERW_CheckedChanged);
            // 
            // checkBoxRANDOM
            // 
            this.checkBoxRANDOM.AutoSize = true;
            this.checkBoxRANDOM.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxRANDOM.Location = new System.Drawing.Point(23, 50);
            this.checkBoxRANDOM.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxRANDOM.Name = "checkBoxRANDOM";
            this.checkBoxRANDOM.Size = new System.Drawing.Size(122, 20);
            this.checkBoxRANDOM.TabIndex = 4;
            this.checkBoxRANDOM.Text = "Random Playing";
            this.checkBoxRANDOM.UseVisualStyleBackColor = true;
            this.checkBoxRANDOM.CheckedChanged += new System.EventHandler(this.checkBoxRANDOM_CheckedChanged);
            // 
            // checkBoxSEQ
            // 
            this.checkBoxSEQ.AutoSize = true;
            this.checkBoxSEQ.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSEQ.Location = new System.Drawing.Point(23, 23);
            this.checkBoxSEQ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxSEQ.Name = "checkBoxSEQ";
            this.checkBoxSEQ.Size = new System.Drawing.Size(132, 20);
            this.checkBoxSEQ.TabIndex = 3;
            this.checkBoxSEQ.Text = "Sequence Playing";
            this.checkBoxSEQ.UseVisualStyleBackColor = true;
            this.checkBoxSEQ.CheckedChanged += new System.EventHandler(this.checkBoxSEQ_CheckedChanged);
            // 
            // labelSPI
            // 
            this.labelSPI.AutoSize = true;
            this.labelSPI.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSPI.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelSPI.Location = new System.Drawing.Point(15, 55);
            this.labelSPI.Name = "labelSPI";
            this.labelSPI.Size = new System.Drawing.Size(82, 24);
            this.labelSPI.TabIndex = 8;
            this.labelSPI.Text = "Flash : ";
            this.labelSPI.Click += new System.EventHandler(this.labelSPI_Click);
            // 
            // comboBoxCPUCLK
            // 
            this.comboBoxCPUCLK.FormattingEnabled = true;
            this.comboBoxCPUCLK.Items.AddRange(new object[] {
            "28 MHZ",
            "20 MHZ"});
            this.comboBoxCPUCLK.Location = new System.Drawing.Point(440, 681);
            this.comboBoxCPUCLK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBoxCPUCLK.Name = "comboBoxCPUCLK";
            this.comboBoxCPUCLK.Size = new System.Drawing.Size(140, 24);
            this.comboBoxCPUCLK.TabIndex = 7;
            this.comboBoxCPUCLK.Text = "CPU CLOCK..";
            this.comboBoxCPUCLK.Visible = false;
            this.comboBoxCPUCLK.SelectedIndexChanged += new System.EventHandler(this.comboBoxCPUCLK_SelectedIndexChanged);
            // 
            // buttonCLRLOG
            // 
            this.buttonCLRLOG.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCLRLOG.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCLRLOG.Location = new System.Drawing.Point(439, 685);
            this.buttonCLRLOG.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonCLRLOG.Name = "buttonCLRLOG";
            this.buttonCLRLOG.Size = new System.Drawing.Size(114, 33);
            this.buttonCLRLOG.TabIndex = 8;
            this.buttonCLRLOG.Text = "Clear Log";
            this.buttonCLRLOG.UseVisualStyleBackColor = true;
            this.buttonCLRLOG.Visible = false;
            this.buttonCLRLOG.Click += new System.EventHandler(this.buttonCLRLOG_Click);
            // 
            // buttonLoadConf
            // 
            this.buttonLoadConf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLoadConf.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadConf.ForeColor = System.Drawing.Color.Black;
            this.buttonLoadConf.Location = new System.Drawing.Point(18, 30);
            this.buttonLoadConf.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonLoadConf.Name = "buttonLoadConf";
            this.buttonLoadConf.Size = new System.Drawing.Size(85, 32);
            this.buttonLoadConf.TabIndex = 9;
            this.buttonLoadConf.Text = "Load";
            this.buttonLoadConf.UseVisualStyleBackColor = true;
            this.buttonLoadConf.Click += new System.EventHandler(this.buttonLoadConf_Click);
            // 
            // buttonSaveConf
            // 
            this.buttonSaveConf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSaveConf.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveConf.ForeColor = System.Drawing.Color.Black;
            this.buttonSaveConf.Location = new System.Drawing.Point(115, 30);
            this.buttonSaveConf.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonSaveConf.Name = "buttonSaveConf";
            this.buttonSaveConf.Size = new System.Drawing.Size(80, 32);
            this.buttonSaveConf.TabIndex = 10;
            this.buttonSaveConf.Text = "Save";
            this.buttonSaveConf.UseVisualStyleBackColor = true;
            this.buttonSaveConf.Click += new System.EventHandler(this.buttonSaveConf_Click);
            // 
            // buttonGENBIN
            // 
            this.buttonGENBIN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonGENBIN.Font = new System.Drawing.Font("Arial", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGENBIN.Location = new System.Drawing.Point(863, 613);
            this.buttonGENBIN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonGENBIN.Name = "buttonGENBIN";
            this.buttonGENBIN.Size = new System.Drawing.Size(125, 106);
            this.buttonGENBIN.TabIndex = 11;
            this.buttonGENBIN.Text = "BIN";
            this.buttonGENBIN.UseVisualStyleBackColor = true;
            this.buttonGENBIN.Click += new System.EventHandler(this.buttonGENBIN_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "m110 files (*.ms110)| *.ms110|bin files(*.bin)|*.bin| all files (*.*)|*.*";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "ms110 files (*.ms110) |*.ms110|wav Files (*.wav)|*.wav|All Fils(*.*)|*.*";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.checkBoxSEQ);
            this.groupBox11.Controls.Add(this.checkBoxRANDOM);
            this.groupBox11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(795, 179);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(192, 80);
            this.groupBox11.TabIndex = 12;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "  OPTIONS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(808, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(143, 16);
            this.label7.TabIndex = 5;
            this.label7.Text = "ONE KEY OPERATION";
            // 
            // labelFileSize
            // 
            this.labelFileSize.AutoSize = true;
            this.labelFileSize.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFileSize.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelFileSize.Location = new System.Drawing.Point(18, 28);
            this.labelFileSize.Name = "labelFileSize";
            this.labelFileSize.Size = new System.Drawing.Size(79, 24);
            this.labelFileSize.TabIndex = 13;
            this.labelFileSize.Text = "Used : ";
            this.labelFileSize.Click += new System.EventHandler(this.labelFileSize_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label13);
            this.groupBox12.Controls.Add(this.label4);
            this.groupBox12.Controls.Add(this.label6);
            this.groupBox12.Controls.Add(this.labelVolLevel);
            this.groupBox12.Controls.Add(this.trackBar1);
            this.groupBox12.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox12.Location = new System.Drawing.Point(795, 382);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox12.Size = new System.Drawing.Size(192, 129);
            this.groupBox12.TabIndex = 16;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "  VOLUME LEVEL   ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(6, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(114, 16);
            this.label13.TabIndex = 16;
            this.label13.Text = "(Default is Level 6)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(151, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "High";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(6, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 16);
            this.label6.TabIndex = 14;
            this.label6.Text = "Low";
            // 
            // labelVolLevel
            // 
            this.labelVolLevel.AutoSize = true;
            this.labelVolLevel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVolLevel.ForeColor = System.Drawing.Color.Black;
            this.labelVolLevel.Location = new System.Drawing.Point(62, 33);
            this.labelVolLevel.Name = "labelVolLevel";
            this.labelVolLevel.Size = new System.Drawing.Size(63, 19);
            this.labelVolLevel.TabIndex = 13;
            this.labelVolLevel.Text = "Level 6";
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 1;
            this.trackBar1.Location = new System.Drawing.Point(6, 55);
            this.trackBar1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trackBar1.Maximum = 8;
            this.trackBar1.Minimum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(177, 45);
            this.trackBar1.TabIndex = 3;
            this.trackBar1.Value = 6;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.labelVoiceLengthPowerOn);
            this.groupBox13.Controls.Add(this.label15);
            this.groupBox13.Controls.Add(this.label16);
            this.groupBox13.Controls.Add(this.labelInfoPowerOn);
            this.groupBox13.Controls.Add(this.labelFilePowerOn);
            this.groupBox13.Controls.Add(this.buttonRemovePowerOn);
            this.groupBox13.Controls.Add(this.buttonLoadPowerOn);
            this.groupBox13.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox13.Location = new System.Drawing.Point(584, -3);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox13.Size = new System.Drawing.Size(404, 159);
            this.groupBox13.TabIndex = 8;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "  POWER ON VOICE  ";
            // 
            // labelVoiceLengthPowerOn
            // 
            this.labelVoiceLengthPowerOn.AutoSize = true;
            this.labelVoiceLengthPowerOn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVoiceLengthPowerOn.ForeColor = System.Drawing.Color.Black;
            this.labelVoiceLengthPowerOn.Location = new System.Drawing.Point(225, 128);
            this.labelVoiceLengthPowerOn.Name = "labelVoiceLengthPowerOn";
            this.labelVoiceLengthPowerOn.Size = new System.Drawing.Size(40, 16);
            this.labelVoiceLengthPowerOn.TabIndex = 7;
            this.labelVoiceLengthPowerOn.Text = "0.00s";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(110, 128);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 16);
            this.label15.TabIndex = 6;
            this.label15.Text = "VOICE LENGTH  : ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(110, 110);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 16);
            this.label16.TabIndex = 5;
            this.label16.Text = "SAMPLE RATE : ";
            // 
            // labelInfoPowerOn
            // 
            this.labelInfoPowerOn.AutoSize = true;
            this.labelInfoPowerOn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoPowerOn.ForeColor = System.Drawing.Color.Black;
            this.labelInfoPowerOn.Location = new System.Drawing.Point(216, 110);
            this.labelInfoPowerOn.Name = "labelInfoPowerOn";
            this.labelInfoPowerOn.Size = new System.Drawing.Size(63, 16);
            this.labelInfoPowerOn.TabIndex = 4;
            this.labelInfoPowerOn.Text = "12,000Hz";
            // 
            // labelFilePowerOn
            // 
            this.labelFilePowerOn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFilePowerOn.ForeColor = System.Drawing.Color.Black;
            this.labelFilePowerOn.Location = new System.Drawing.Point(114, 29);
            this.labelFilePowerOn.Name = "labelFilePowerOn";
            this.labelFilePowerOn.Size = new System.Drawing.Size(259, 81);
            this.labelFilePowerOn.TabIndex = 3;
            this.labelFilePowerOn.Text = "FILE 1 :";
            // 
            // buttonRemovePowerOn
            // 
            this.buttonRemovePowerOn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRemovePowerOn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemovePowerOn.ForeColor = System.Drawing.Color.Black;
            this.buttonRemovePowerOn.Location = new System.Drawing.Point(18, 92);
            this.buttonRemovePowerOn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonRemovePowerOn.Name = "buttonRemovePowerOn";
            this.buttonRemovePowerOn.Size = new System.Drawing.Size(80, 45);
            this.buttonRemovePowerOn.TabIndex = 1;
            this.buttonRemovePowerOn.Text = "Remove";
            this.buttonRemovePowerOn.UseVisualStyleBackColor = true;
            this.buttonRemovePowerOn.Click += new System.EventHandler(this.buttonRemovePowerOn_Click);
            // 
            // buttonLoadPowerOn
            // 
            this.buttonLoadPowerOn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLoadPowerOn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadPowerOn.ForeColor = System.Drawing.Color.Black;
            this.buttonLoadPowerOn.Location = new System.Drawing.Point(18, 29);
            this.buttonLoadPowerOn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonLoadPowerOn.Name = "buttonLoadPowerOn";
            this.buttonLoadPowerOn.Size = new System.Drawing.Size(80, 45);
            this.buttonLoadPowerOn.TabIndex = 0;
            this.buttonLoadPowerOn.Text = "Load";
            this.buttonLoadPowerOn.UseVisualStyleBackColor = true;
            this.buttonLoadPowerOn.Click += new System.EventHandler(this.buttonLoadPowerOn_Click);
            // 
            // labelConfigFileName
            // 
            this.labelConfigFileName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConfigFileName.ForeColor = System.Drawing.Color.Black;
            this.labelConfigFileName.Location = new System.Drawing.Point(206, 19);
            this.labelConfigFileName.Name = "labelConfigFileName";
            this.labelConfigFileName.Size = new System.Drawing.Size(333, 43);
            this.labelConfigFileName.TabIndex = 13;
            this.labelConfigFileName.Text = "  ";
            this.labelConfigFileName.Click += new System.EventHandler(this.label19_Click);
            // 
            // labelOutputFileName
            // 
            this.labelOutputFileName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOutputFileName.ForeColor = System.Drawing.Color.Black;
            this.labelOutputFileName.Location = new System.Drawing.Point(15, 25);
            this.labelOutputFileName.Name = "labelOutputFileName";
            this.labelOutputFileName.Size = new System.Drawing.Size(253, 79);
            this.labelOutputFileName.TabIndex = 17;
            this.labelOutputFileName.Text = "  ";
            this.labelOutputFileName.Click += new System.EventHandler(this.label19_Click_2);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.labelOutputFileName);
            this.groupBox14.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox14.Location = new System.Drawing.Point(584, 608);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox14.Size = new System.Drawing.Size(269, 112);
            this.groupBox14.TabIndex = 8;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "  OUTPUT BIN FILE   ";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.buttonSaveConf);
            this.groupBox15.Controls.Add(this.labelConfigFileName);
            this.groupBox15.Controls.Add(this.buttonLoadConf);
            this.groupBox15.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox15.Location = new System.Drawing.Point(16, 649);
            this.groupBox15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox15.Size = new System.Drawing.Size(548, 73);
            this.groupBox15.TabIndex = 18;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "  CONFIG  FILE  ";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.labelFileSize);
            this.groupBox16.Controls.Add(this.labelSPI);
            this.groupBox16.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox16.Location = new System.Drawing.Point(795, 516);
            this.groupBox16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox16.Size = new System.Drawing.Size(193, 89);
            this.groupBox16.TabIndex = 18;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "  FILE SIZE   ";
            // 
            // FormMS311as110
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.groupBox16);
            this.Controls.Add(this.groupBox15);
            this.Controls.Add(this.groupBox14);
            this.Controls.Add(this.groupBox13);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.buttonGENBIN);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.comboBoxCPUCLK);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.buttonCLRLOG);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormMS311as110";
            this.Text = "MS110 Convert Tools V1.0";
            this.Load += new System.EventHandler(this.FormMS311as110_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxVoice1.ResumeLayout(false);
            this.groupBoxVoice1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLED1)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLED2)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxVoice1;
        private System.Windows.Forms.Button buttonRemove1;
        private System.Windows.Forms.Button buttonLoad1;
        private System.Windows.Forms.Label labelInfo1;
        private System.Windows.Forms.Label labelFile1;
        private System.Windows.Forms.CheckBox checkBoxEXTPULLHIGH1;
        private System.Windows.Forms.CheckBox checkBoxNONRETRIG1;
        private System.Windows.Forms.CheckBox checkBoxLEVELHOLD1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelInfo2;
        private System.Windows.Forms.Label labelFile2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxEXTPULLHIGH2;
        private System.Windows.Forms.CheckBox checkBoxNONRETRIG2;
        private System.Windows.Forms.CheckBox checkBoxLEVELHOLD2;
        private System.Windows.Forms.Button buttonRemove2;
        private System.Windows.Forms.Button buttonLoad2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label labelInfo3;
        private System.Windows.Forms.Label labelFile3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox checkBoxEXTPULLHIGH3;
        private System.Windows.Forms.CheckBox checkBoxNONRETRIG3;
        private System.Windows.Forms.CheckBox checkBoxLEVELHOLD3;
        private System.Windows.Forms.Button buttonRemove3;
        private System.Windows.Forms.Button buttonLoad3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label labelInfo4;
        private System.Windows.Forms.Label labelFile4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox checkBoxEXTPULLHIGH4;
        private System.Windows.Forms.CheckBox checkBoxNONRETRIG4;
        private System.Windows.Forms.CheckBox checkBoxLEVELHOLD4;
        private System.Windows.Forms.Button buttonRemove4;
        private System.Windows.Forms.Button buttonLoad4;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton radioButtonLEDLEVEL1;
        private System.Windows.Forms.TrackBar trackBarLED1;
        private System.Windows.Forms.RadioButton radioButtonLEDFLASH1;
        private System.Windows.Forms.RadioButton radioButtonLEDLOW1;
        private System.Windows.Forms.RadioButton radioButtonLEDHIGH1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton radioButtonLEVEL2;
        private System.Windows.Forms.TrackBar trackBarLED2;
        private System.Windows.Forms.RadioButton radioButtonFLASH2;
        private System.Windows.Forms.RadioButton radioButtonLEDLOW2;
        private System.Windows.Forms.RadioButton radioButtonLEDHIGH2;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox checkBoxRANDOM;
        private System.Windows.Forms.CheckBox checkBoxSEQ;
        private System.Windows.Forms.CheckBox checkBoxAUTORPT;
        private System.Windows.Forms.CheckBox checkBox3V;
        private System.Windows.Forms.CheckBox checkBoxKEYOVERW;
        private System.Windows.Forms.Button buttonCLRLOG;
        private System.Windows.Forms.Button buttonLoadConf;
        private System.Windows.Forms.Button buttonSaveConf;
        private System.Windows.Forms.Button buttonGENBIN;
        private System.Windows.Forms.ComboBox comboBoxCPUCLK;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label labelSPI;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label labelSense1;
        private System.Windows.Forms.Label labelSenseLess1;
        private System.Windows.Forms.Label labelSenseMost1;
        private System.Windows.Forms.Label labelSenseLess2;
        private System.Windows.Forms.Label labelSenseMost2;
        private System.Windows.Forms.Label labelSense2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelFileSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelInfoVoiceLength1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelInfoVoiceLength2;
        private System.Windows.Forms.Label labelInfoVoiceLength3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInfoVoiceLength4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelVolLevel;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label labelVoiceLengthPowerOn;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelInfoPowerOn;
        private System.Windows.Forms.Label labelFilePowerOn;
        private System.Windows.Forms.Button buttonRemovePowerOn;
        private System.Windows.Forms.Button buttonLoadPowerOn;
        private System.Windows.Forms.Label labelConfigFileName;
        private System.Windows.Forms.Label labelOutputFileName;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox16;
    }
}

