﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ms311as110ui
{
    
    public partial class FormMS311as110 : Form
    {
        public readonly int[] indexTable = {
    -1, -1, -1, -1, 2, 4, 6, 8,
    -1, -1, -1, -1, 2, 4, 6, 8,
};
        public readonly ushort[] StepsizeTable0 = {
0x0007,
0x0008,
0x0009,
0x000A,
0x000B,
0x000C,
0x000D,
0x000E,
0x0010,
0x0011,
0x0013,
0x0015,
0x0017,
0x0019,
0x001C,
0x001F,
0x0022,
0x0025,
0x0029,
0x002D,
0x0032,
0x0037,
0x003C,
0x0042,
0x0049,
0x0050,
0x0058,
0x0061,
0x006B,
0x0076,
0x0082,
0x008F,
0x009D,
0x00AD,
0x00BE,
0x00D1,
0x00E6,
0x00FD,
0x0117,
0x0133,
0x0151,
0x0173,
0x0198,
0x01C1,
0x01EE,
0x0220,
0x0256,
0x0292,
0x02D4,
0x031C,
0x036C,
0x03C3,
0x0424,
0x048E,
0x0502,
0x0583,
0x0610,
0x06AB,
0x0756,
0x0812,
0x08E0,
0x09C3,
0x0ABD,
0x0BD0,
0x0CFF,
0x0E4C,
0x0FBA,
0x114C,
0x1307,
0x14EE,
0x1706,
0x1954,
0x1BDC,
0x1EA5,
0x21B6,
0x2515,
0x28CA,
0x2CDF,
0x315B,
0x364B,
0x3BB9,
0x41B2,
0x4844,
0x4F7E,
0x5771,
0x602F,
0x69CE,
0x7462,
0x7FFF,
0x0101,
0x0100,
0xfffe,
0xfeff,
0x0102,
0x0303,
0x00fe,
0xfcfc,
0xfe02,
0x0506,
0x0500,
0xfaf7,
0xf8fd,
0x050c,
0x0e08,
0xfdf0,
0xe8eb,
0xfd1b,
0x3f60,
0x7272,
0x603f,
0x1bfd,
0xebe8,
0xf0fd,
0x080e,
0x0c05,
0xfdf8,
0xf7fa,
0x0005,
0x0605,
0x02fe,
0xfcfc,
0xfe00,
0x0303,
0x0201,
0xfffe,
0xfeff,
0x0001,
0x0101
};
        public static readonly int WaveNumber = 5;
        int[] startPage = new int[WaveNumber];
        int[] endPage = new int[WaveNumber];
        int[] rate = new int[WaveNumber];

        private uint bit12_coder(short[] srclin, int len, out byte[] bit12o)
        {
            uint newsize = (uint)(len * 3 + 1) / 2;
            bit12o = new byte[newsize];
            int i, j;
            for (i = 0; i < newsize; i++)
            {
                bit12o[i] = 0;
            }
            for (j = 0, i = 0; i + 1 < len; i += 2, j += 3)
            {
                bit12o[j] = (byte)(srclin[i] >> 8);
                bit12o[j + 1] = (byte)(srclin[i + 1] >> 8);
                bit12o[j + 2] = (byte)(((srclin[i] >> 4) & 0xf) | (srclin[i + 1] & 0xf0));

            }
            return newsize;

        }
        private uint adpcm_coder(short[] srclin, int len, out byte[] adpcm)
        {
            uint newsize = (uint)(len + 1) / 2;
            adpcm = new byte[newsize];


            int val;			/* Current input sample value */
            int sign;			/* Current adpcm sign bit */
            int delta;			/* Current adpcm output value */
            int diff;			/* Difference between val and valprev */
            int step;			/* Stepsize */
            int valpred;		/* Predicted output value */
            int vpdiff;			/* Current change to valpred */
            int index;			/* Current step change index */
            int outputbuffer = 0;		/* place to keep previous 4-bit value */
            int bufferstep;		/* toggle between outputbuffer/output */
            int inp = 0;
            int outp = 0;


            valpred = 0;
            index = 0;

            step = StepsizeTable0[index];

            bufferstep = 1;
            //   if(state->valprev==(short)0xf040)
            //       printf("stop here\n");

            for (; len > 0; len--)
            {
                val = (int)srclin[inp++];

                /* Step 1 - compute difference with previous value */
                diff = val - valpred;
                sign = (diff < 0) ? 8 : 0;
                if (sign != 0) diff = (-diff);

                /* Step 2 - Divide and clamp */
                /* Note:
                ** This code *approximately* computes:
                **    delta = diff*4/step;
                **    vpdiff = (delta+0.5)*step/4;
                ** but in shift step bits are dropped. The net result of this is
                ** that even if you have fast mul/div hardware you cannot put it to
                ** good use since the fixup would be too expensive.
                */
                delta = 0;
                vpdiff = (step >> 3);

                if (diff >= step)
                {
                    delta = 4;
                    diff -= step;
                    vpdiff += step;
                }
                step >>= 1;
                if (diff >= step)
                {
                    delta |= 2;
                    diff -= step;
                    vpdiff += step;
                }
                step >>= 1;
                if (diff >= step)
                {
                    delta |= 1;
                    vpdiff += step;
                }

                /* Step 3 - Update previous value */
                if (sign != 0)
                    valpred -= vpdiff;
                else
                    valpred += vpdiff;

                /* Step 4 - Clamp previous value to 16 bits */
                if (valpred > 32767)
                    valpred = 32767;
                else if (valpred < -32768)
                    valpred = -32768;

                /* Step 5 - Assemble value, update index and step values */
                delta |= sign;

                index += indexTable[delta];
                if (index < 0) index = 0;
                if (index > 88) index = 88;
                step = StepsizeTable0[index];

                /* Step 6 - Output value */
                if (bufferstep != 0)
                {
                    outputbuffer = delta & 0xf;
                }
                else
                {
                    adpcm[outp++] = (byte)(((delta & 0x0f) << 4) | outputbuffer);
                }
                bufferstep ^= 1;
            }

            /* Output last step, if needed */
            return newsize;
        }


        public FormMS311as110()
        {
            InitializeComponent();
        }
        byte []ms311as110code;
        ms110info infoMS110;
        public bool wav_valid_check(string fname, ref int length, ref int rate)
        {
            if (!WAVFile.IsWaveFile(fname))
            {
                 MessageBox.Show("This is not valid wav File", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            WAVFormat fformat = WAVFile.GetAudioFormat(fname);
            if (fformat.BitsPerSample != 16)
            {
                 MessageBox.Show("This is not 16-bit WAV File", "ERROR",MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (fformat.NumChannels != 1)
            {
                 MessageBox.Show("This is not MONO-CHANNEL File", "ERROR",MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            WAVFile thew = new WAVFile();
            string retval = thew.Open(fname, WAVFile.WAVFileMode.READ);
            if (retval.Length > 0)
            {
                 MessageBox.Show("File Open Error" + retval , "ERROR",MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            length = thew.DataSizeBytes / 2; // sample #
            rate = thew.BytesPerSec / 2; // by sample #
            thew.Close(); // close first
            return true;
        }

            private void FormMS311as110_Load(object sender, EventArgs e)
        {
            var assembly = Assembly.GetExecutingAssembly();
            //Getting names of all embedded resources
            var allResourceNames = assembly.GetManifestResourceNames();
            //Selecting first one. 
            var resourceName = allResourceNames[0];

            using (var stream = assembly.GetManifestResourceStream("ms311as110ui.Resources.MS311AS110.bin"))
            {
                if(stream == null)
                {
                    //DialogResult result;
                    MessageBox.Show("Internal error: code not found.");
                    Close();
                    return;
                }
                using (var memoryStream = new MemoryStream())
                {
                    stream.CopyTo(memoryStream);
                    ms311as110code=memoryStream.ToArray();
                }
            }
             infoMS110 = new ms110info();
        }

        private void buttonCLRLOG_Click(object sender, EventArgs e)
        {
            textBoxLog.Text = "LOG :\r\n";
        }
        string GetRelativePath(string filespec, string folder)
        {

            Uri pathUri = new Uri(filespec);
            // Folders must end in a slash
            if (!folder.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                folder += Path.DirectorySeparatorChar;
            }
            Uri folderUri = new Uri(folder);
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', Path.DirectorySeparatorChar));
        }

        private void buttonSaveConf_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Save configuration to ..";
            saveFileDialog1.FilterIndex = 1;

            if(saveFileDialog1.ShowDialog()!=DialogResult.OK)
            {
                textBoxLog.AppendText("Open configuration cancelled.\r\n");
                return;
            }
            labelConfigFileName.Text = "CONFIG FILE : \r\n" + saveFileDialog1.FileName;
            string ms110dir = Path.GetDirectoryName(saveFileDialog1.FileName);
            Directory.SetCurrentDirectory(Path.GetDirectoryName(saveFileDialog1.FileName));
            for (int i=0;i<WaveNumber;i++)
            {
                // get related dir
                if(infoMS110.filenames[i]!="")
                {
                    infoMS110.filenames[i] = GetRelativePath(infoMS110.filenames[i], ms110dir);
                }
            }
            try
            {
                System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(ms110info));

                System.IO.FileStream file = System.IO.File.Create(saveFileDialog1.FileName);

                writer.Serialize(file, infoMS110);
                file.Close();
            }
            catch(Exception esconf)
            {
                textBoxLog.AppendText("save file fail:" + esconf.Message + "\r\n");
                // after saved, convert back to abs
                for (int i = 0; i < WaveNumber; i++)
                {
                    // get related dir
                    if (infoMS110.filenames[i] != "")
                    {
                        infoMS110.filenames[i] = Path.GetFullPath(infoMS110.filenames[i]);
                    }
                }
                return;
            }
            textBoxLog.AppendText("save configuration to " + saveFileDialog1.FileName + "\r\n");
            for (int i = 0; i < WaveNumber; i++)
            {
                // get related dir
                if (infoMS110.filenames[i] != "")
                {
                    infoMS110.filenames[i] = Path.GetFullPath(infoMS110.filenames[i]);
                }
            }
        }

        private void buttonLoadConf_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Load Configuration from..";
            openFileDialog1.FileName = "";
            openFileDialog1.FilterIndex = 1;
            if(openFileDialog1.ShowDialog()!=DialogResult.OK)
            {
                textBoxLog.AppendText("Load configuration cancelled.\r\n");
                return;
            }

            labelConfigFileName.Text = "CONFIG FILE : \r\n" + openFileDialog1.FileName;
            // Now we can read the serialized book ...  
            try
            {
                System.Xml.Serialization.XmlSerializer reader =
                    new System.Xml.Serialization.XmlSerializer(typeof(ms110info));
                System.IO.StreamReader file = new System.IO.StreamReader(
                    openFileDialog1.FileName);
                infoMS110 = (ms110info)reader.Deserialize(file);
                file.Close();
            }catch(Exception erconf)
            {
                textBoxLog.AppendText("Load configuration fail.." + erconf.Message +"\r\n");
                return;
            }
            trackBar1.Value = infoMS110.voiceVolume;
            labelVolLevel.Text = "Level " + trackBar1.Value;
            Directory.SetCurrentDirectory(Path.GetDirectoryName(openFileDialog1.FileName));
            
            int rate = 8000;
            int length = 0;
            float lenInSec;
            if (infoMS110.filenames[0] != "")
            {
                infoMS110.filenames[0] = Path.GetFullPath(infoMS110.filenames[0]);
                if (infoMS110.filenames[0] == "" || !wav_valid_check(infoMS110.filenames[0], ref length, ref rate))
                {
                    labelFile1.Text = "FILE 1 :";
                    labelInfo1.Text = "  ";
                    infoMS110.length[0] = infoMS110.rate[0] = 0;
                }
                else
                {
                    labelFile1.Text = "FILE 1 :\r\n" + infoMS110.filenames[0];
                    labelInfo1.Text = rate.ToString("N0") + "Hz";
                    infoMS110.length[0] = length;
                    infoMS110.rate[0] = rate;
                    lenInSec = (float)length / (float)rate;
                    labelInfoVoiceLength1.Text = lenInSec.ToString("F2") + "s";
                }
            }else
            {
                labelFile1.Text = "FILE 1 :";
                labelInfo1.Text = "  ";
                infoMS110.length[0] = infoMS110.rate[0] = 0;
            }

            if (infoMS110.filenames[1] != "")
            {
                infoMS110.filenames[1] = Path.GetFullPath(infoMS110.filenames[1]);
                if (infoMS110.filenames[1] == "" || !wav_valid_check(infoMS110.filenames[1], ref length, ref rate))
                {
                    labelFile2.Text = "FILE 2 :";
                    labelInfo2.Text = "  ";
                    infoMS110.rate[1] = infoMS110.length[1] = 0;
                }
                else
                {
                    labelFile2.Text = "FILE 2 :\r\n" + infoMS110.filenames[1];
                    labelInfo2.Text = rate.ToString("N0") + "Hz";
                    infoMS110.rate[1] = rate;
                    infoMS110.length[1] = length;
                    lenInSec = (float)length / (float)rate;
                    labelInfoVoiceLength2.Text = lenInSec.ToString("F2") + "s";

                }
            }else
            {
                labelFile2.Text = "FILE 2: ";
                labelInfo2.Text = "  ";
                infoMS110.rate[1] = infoMS110.length[1] = 0;
            }

            if (infoMS110.filenames[2] != "")
            {
                infoMS110.filenames[2] = Path.GetFullPath(infoMS110.filenames[2]);
                if (infoMS110.filenames[2] == "" || !wav_valid_check(infoMS110.filenames[2], ref length, ref rate))
                {
                    labelFile3.Text = "FILE 3 :";
                    labelInfo3.Text = "  ";
                    infoMS110.rate[2] = infoMS110.length[2] = 0;
                }
                else
                {
                    labelFile3.Text = "FILE 3 :\r\n" + infoMS110.filenames[2];
                    labelInfo3.Text = rate.ToString("N0") + "Hz";
                    infoMS110.rate[2] = rate;
                    infoMS110.length[2] = length;
                    lenInSec = (float)length / (float)rate;
                    labelInfoVoiceLength3.Text = lenInSec.ToString("F2") + "s";
                }
            }else
            {
                labelFile3.Text = "FILE 3 :";
                labelInfo3.Text = "  ";
                infoMS110.rate[2] = infoMS110.length[2] = 0;
            }

            if (infoMS110.filenames[3] != "")
            {
                infoMS110.filenames[3] = Path.GetFullPath(infoMS110.filenames[3]);
                if (infoMS110.filenames[3] == "" || !wav_valid_check(infoMS110.filenames[3], ref length, ref rate))
                {
                    labelFile4.Text = "FILE 4 :";
                    labelInfo4.Text = "  ";
                    infoMS110.rate[3] = infoMS110.length[3] = 0;
                }
                else
                {
                    labelFile4.Text = "FILE 4 :\r\n" + infoMS110.filenames[3];
                    labelInfo4.Text = rate.ToString("N0") + "Hz";
                    infoMS110.rate[3] = rate;
                    infoMS110.length[3] = length;
                    lenInSec = (float)length / (float)rate;
                    labelInfoVoiceLength4.Text = lenInSec.ToString("F2") + "s";
                }
            }
            else
            {
                labelFile4.Text = "FILE 4 :";
                labelInfo4.Text = "  ";
                infoMS110.rate[3] = infoMS110.length[3] = 0;
            }

            if (infoMS110.filenames[4] != "")
            {
                infoMS110.filenames[4] = Path.GetFullPath(infoMS110.filenames[4]);
                if (infoMS110.filenames[4] == "" || !wav_valid_check(infoMS110.filenames[4], ref length, ref rate))
                {
                    labelFilePowerOn.Text = "POWER-ON :";
                    labelInfoPowerOn.Text = "  ";
                    infoMS110.rate[4] = infoMS110.length[4] = 0;
                }
                else
                {
                    labelFilePowerOn.Text = "POWER-ON :\r\n" + infoMS110.filenames[4];
                    labelInfoPowerOn.Text = rate.ToString("N0") + "Hz";
                    infoMS110.rate[4] = rate;
                    infoMS110.length[4] = length;
                    lenInSec = (float)length / (float)rate;
                    labelVoiceLengthPowerOn.Text = lenInSec.ToString("F2") + "s";
                }
            }
            else
            {
                labelFilePowerOn.Text = "POWER-ON :";
                labelInfoPowerOn.Text = "  ";
                infoMS110.rate[4] = infoMS110.length[4] = 0;
            }

            if (infoMS110.clkRate == 28000000)
                comboBoxCPUCLK.SelectedIndex = 0;
            else
                comboBoxCPUCLK.SelectedIndex = 1;

            //switch(infoMS110.spiSize)
            //{
            //    case 1024 * 1024 * 2: comboBoxSPISIZE.SelectedIndex = 0; break;
            //    case 1024 * 1024 * 4: comboBoxSPISIZE.SelectedIndex = 1; break;
            //    case 1024 * 1024 * 8: comboBoxSPISIZE.SelectedIndex = 2; break;
            //    case 1024 * 1024 * 16: comboBoxSPISIZE.SelectedIndex = 3; break;
            //    case 1024 * 1024 * 32: comboBoxSPISIZE.SelectedIndex = 4; break;
            //    case 1024 * 1024 * 64: comboBoxSPISIZE.SelectedIndex = 5; break;
            //    case 1024 * 1024 * 128: comboBoxSPISIZE.SelectedIndex = 6; break;
            //    default: comboBoxSPISIZE.SelectedIndex = 0; break;
            //}

            if ((infoMS110.globalOPT & 0x01) != 0)
                checkBoxKEYOVERW.Checked = true;
            else
                checkBoxKEYOVERW.Checked = false;

            if ((infoMS110.globalOPT & 0x02) != 0)
                checkBox3V.Checked = true;
            else
                checkBox3V.Checked = false;

            if ((infoMS110.globalOPT & 0x04) != 0)
                checkBoxAUTORPT.Checked = true;
            else
                checkBoxAUTORPT.Checked = false;

            if ((infoMS110.globalOPT & 0x08) != 0)
                checkBoxSEQ.Checked = true;
            else
                checkBoxSEQ.Checked = false;

            if ((infoMS110.globalOPT & 0x10) != 0)
                checkBoxRANDOM.Checked = true;
            else
                checkBoxRANDOM.Checked = false;


            if((infoMS110.ioctl[0]&0x01)!=0)
            {
                checkBoxLEVELHOLD1.Checked = true;
            }else
            {
                checkBoxLEVELHOLD1.Checked = false;
            }

            if ((infoMS110.ioctl[1] & 0x01) != 0)
            {
                checkBoxLEVELHOLD2.Checked = true;
            }
            else
            {
                checkBoxLEVELHOLD2.Checked = false;
            }

            if ((infoMS110.ioctl[2] & 0x01) != 0)
            {
                checkBoxLEVELHOLD3.Checked = true;
            }
            else
            {
                checkBoxLEVELHOLD3.Checked = false;
            }

            if ((infoMS110.ioctl[3] & 0x01) != 0)
            {
                checkBoxLEVELHOLD4.Checked = true;
            }
            else
            {
                checkBoxLEVELHOLD4.Checked = false;
            }

            if((infoMS110.ioctl[0]&0x02)!=0)
            {
                checkBoxNONRETRIG1.Checked = true;
            }else
            {
                checkBoxNONRETRIG1.Checked = false;
            }

            if ((infoMS110.ioctl[1] & 0x02) != 0)
            {
                checkBoxNONRETRIG2.Checked = true;
            }
            else
            {
                checkBoxNONRETRIG2.Checked = false;
            }

            if ((infoMS110.ioctl[2] & 0x02) != 0)
            {
                checkBoxNONRETRIG3.Checked = true;
            }
            else
            {
                checkBoxNONRETRIG3.Checked = false;
            }

            if ((infoMS110.ioctl[3] & 0x02) != 0)
            {
                checkBoxNONRETRIG4.Checked = true;
            }
            else
            {
                checkBoxNONRETRIG4.Checked = false;
            }

            if((infoMS110.ioctl[0]&0x04)!=0)
            {
                checkBoxEXTPULLHIGH1.Checked = true;
            }else
            {
                checkBoxEXTPULLHIGH1.Checked = false;
            }

            if ((infoMS110.ioctl[1] & 0x04) != 0)
            {
                checkBoxEXTPULLHIGH2.Checked = true;
            }
            else
            {
                checkBoxEXTPULLHIGH2.Checked = false;
            }

            if ((infoMS110.ioctl[2] & 0x04) != 0)
            {
                checkBoxEXTPULLHIGH3.Checked = true;
            }
            else
            {
                checkBoxEXTPULLHIGH3.Checked = false;
            }

            if ((infoMS110.ioctl[3] & 0x04) != 0)
            {
                checkBoxEXTPULLHIGH4.Checked = true;
            }
            else
            {
                checkBoxEXTPULLHIGH4.Checked = false;
            }
            trackBarLED1.Value = infoMS110.ledopt[1];
            switch (infoMS110.ledopt[0])
            {
                case 0: radioButtonLEDHIGH1.Checked = true; break;
                case 1: radioButtonLEDLOW1.Checked = true; break;
                case 2: radioButtonLEDFLASH1.Checked = true; break;
                case 4: radioButtonLEDLEVEL1.Checked = true; break;
            }
            trackBarLED2.Value = infoMS110.ledopt[3];
            switch (infoMS110.ledopt[2])
            {
                case 0: radioButtonLEDHIGH2.Checked = true; break;
                case 1: radioButtonLEDLOW2.Checked = true; break;
                case 2: radioButtonFLASH2.Checked = true; break;
                case 4: radioButtonLEVEL2.Checked = true; break;
            }

            updatePage();

        }


        private uint write_wav_to_bin(BinaryWriter bw, string wavfilei, char ulawc, out int sample_rate)
        {
            // bw is created one
            sample_rate = 0;
            byte[] adpcmdata;
            byte[] b12data;
            if (wavfilei.Length == 0)
                return 0; // no need to comment
            if (!WAVFile.IsWaveFile(wavfilei))
            {
                textBoxLog.AppendText("File " + wavfilei + " is not valid wav!!\r\n");
                return 0;
            }
            WAVFormat fformat = WAVFile.GetAudioFormat(wavfilei);
            if (fformat.NumChannels > 2)
            {
                textBoxLog.AppendText("File " + wavfilei + " has too many channels!!\r\n");
                return 0;
            }
            if (fformat.BitsPerSample != 16)
            {
                textBoxLog.AppendText("File " + wavfilei + " is not 16-bit WAV!!\r\n");
                return 0;
            }
            if (fformat.NumChannels != 1)
            {
                textBoxLog.AppendText("File " + wavfilei + " is not MONO-CHANNEL!!\r\n");
                return 0;
            }
            WAVFile thew = new WAVFile();
            string retval = thew.Open(wavfilei, WAVFile.WAVFileMode.READ);
            if (retval.Length > 0)
            {
                textBoxLog.AppendText("File open error:" + retval + "\r\n");
                return 0;
            }

            short[] shortdata = new short[thew.NumSamples];

            sample_rate = thew.SampleRateHz;

            for (int i = 0; i < thew.NumSamples; ++i)
                shortdata[i] = thew.GetNextSample_16bit();

            if (ulawc == 'N')
            {
                uint adpcm_bytes = adpcm_coder(shortdata, thew.NumSamples, out adpcmdata);
                bw.Write(adpcmdata);

                while ((adpcm_bytes & 0xff) != 0)// page aligned!!
                {
                    bw.Write((byte)0x00);// check until end
                    adpcm_bytes++;
                }

                thew.Close();
                return adpcm_bytes;
            }
            if (ulawc == '1')// 12 bit
            {
                uint c12_bytes = bit12_coder(shortdata, thew.NumSamples, out b12data);
                bw.Write(b12data);
                while ((c12_bytes & 0xff) != 0)
                {
                    bw.Write((byte)0x00);
                    c12_bytes++;
                }
                thew.Close();
                return c12_bytes;
            }
            byte[] shorttobyte = AudioFormatConverter.Convert(shortdata,
                    AudioCodec.PCM16, AudioCodec.G711U);

            // we reverse and skip 0xff
            for (int i = 0; i < shorttobyte.Length; i++)
            {
                shorttobyte[i] = (byte)(shorttobyte[i] ^ (byte)0xff);
                if (shorttobyte[i] == 0xff)
                    shorttobyte[i] = 0xfe;
            }

            bw.Write(shorttobyte);
            uint writelen = (uint)shorttobyte.Length;

            while ((writelen & 0xff) != 0)
            {
                bw.Write((byte)0x00);// ulaw ff/fe is large code
                writelen++;
            }
            //bw.Write(shorttobyte);
            thew.Close();
            return writelen;

        }
        private void buttonLoad1_Click(object sender, EventArgs e)
        {
            float VoiceLengthSecond1;
            openFileDialog1.Title = "Select VOICE-1 WAV file..";
            openFileDialog1.FileName = "";
            openFileDialog1.FilterIndex = 2;
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                textBoxLog.AppendText("Select Voice 1 WAV file cancelled.\r\n");
                return;
            }
            // check the wav file
            int rate = 8000;
            int length = 0;
            if (!wav_valid_check(openFileDialog1.FileName, ref length, ref rate))
            {
                textBoxLog.AppendText("Open WAV failed!!\r\n");
                return;
            }
            // now it is valid
            VoiceLengthSecond1 = (float) length / (float) rate;
            labelInfoVoiceLength1.Text = VoiceLengthSecond1.ToString("F2")+"s";
            labelInfo1.Text = rate.ToString("N0") + "Hz \r\n ";
            infoMS110.length[0] = length;
            infoMS110.rate[0] = rate;
            infoMS110.filenames[0] = Path.GetFullPath(openFileDialog1.FileName);
            labelFile1.Text = "FILE 1 :\r\n" + openFileDialog1.FileName;
            updatePage();
        }
        private void buttonRemove1_Click(object sender, EventArgs e)
        {
            labelFile1.Text = "FILE 1 :";
            labelInfo1.Text = "  ";
            labelInfoVoiceLength1.Text = "  ";
            infoMS110.filenames[0] = "";
            infoMS110.length[0] = infoMS110.rate[0] = 0;
            updatePage();
        }

        private void buttonLoad2_Click(object sender, EventArgs e)
        {
            float VoiceLengthSecond2;
            openFileDialog1.Title = "Select VOICE-2 WAV file..";
            openFileDialog1.FileName = "";
            openFileDialog1.FilterIndex = 2;
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                textBoxLog.AppendText("Select Voice 2 WAV file cancelled.\r\n");
                return;
            }
            // check the wav file
            int rate = 8000;
            int length = 0;
            if (!wav_valid_check(openFileDialog1.FileName, ref length, ref rate))
            {
                textBoxLog.AppendText("Open WAV failed!!\r\n");
                return;
            }
            // now it is valid
            VoiceLengthSecond2 = (float)length / (float)rate;
            labelInfoVoiceLength2.Text = VoiceLengthSecond2.ToString("F2") + "s";
            labelInfo2.Text = rate.ToString("N0") + "Hz";
            infoMS110.filenames[1] = Path.GetFullPath(openFileDialog1.FileName);
            infoMS110.length[1] = length;
            infoMS110.rate[1] = rate;
            labelFile2.Text = "FILE 2 :\r\n" + openFileDialog1.FileName;
            updatePage();
        }

        private void buttonRemove2_Click(object sender, EventArgs e)
        {
            labelFile2.Text = "FILE 2 :";
            labelInfo2.Text = "  ";
            labelInfoVoiceLength2.Text = "  ";
            infoMS110.filenames[1] = "";
            infoMS110.length[1] = 0;
            infoMS110.rate[1] = 0;
            updatePage();
        }

        private void buttonLoad3_Click(object sender, EventArgs e)
        {
            float VoiceLengthSecond3;
            openFileDialog1.Title = "Select VOICE-3 WAV file..";
            openFileDialog1.FileName = "";
            openFileDialog1.FilterIndex = 2;
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                textBoxLog.AppendText("Select Voice 3 WAV file cancelled.\r\n");
                return;
            }
            // check the wav file
            int rate = 8000;
            int length = 0;
            if (!wav_valid_check(openFileDialog1.FileName, ref length, ref rate))
            {
                textBoxLog.AppendText("Open WAV failed!!\r\n");
                return;
            }
            // now it is valid
            VoiceLengthSecond3 = (float)length / (float)rate;
            labelInfoVoiceLength3.Text = VoiceLengthSecond3.ToString("F2") + "s";
            labelInfo3.Text = rate.ToString("N0") + "Hz";
            infoMS110.length[2] = length;
            infoMS110.rate[2] = rate;
            infoMS110.filenames[2] = Path.GetFullPath(openFileDialog1.FileName);
            labelFile3.Text = "FILE 3 :\r\n" + openFileDialog1.FileName;
            updatePage();
        }

        private void buttonRemove3_Click(object sender, EventArgs e)
        {
            labelFile3.Text = "FILE 3 :";
            labelInfo3.Text = "  ";
            labelInfoVoiceLength3.Text = "  ";
            infoMS110.filenames[2] = "";
            infoMS110.length[2] = 0;
            infoMS110.rate[2] = 0;
            updatePage();
        }

        private void buttonLoad4_Click(object sender, EventArgs e)
        {
            float VoiceLengthSecond4;

            openFileDialog1.Title = "Select VOICE-4 WAV file..";
            openFileDialog1.FileName = "";
            openFileDialog1.FilterIndex = 2;
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                textBoxLog.AppendText("Select Voice 4 WAV file cancelled.\r\n");
                return;
            }
            // check the wav file
            int rate = 8000;
            int length = 0;
            if (!wav_valid_check(openFileDialog1.FileName, ref length, ref rate))
            {
                textBoxLog.AppendText("Open WAV failed!!\r\n");
                return;
            }
            // now it is valid
            VoiceLengthSecond4 = (float)length / (float)rate;
            labelInfoVoiceLength4.Text = VoiceLengthSecond4.ToString("F2") + "s";
            labelInfo4.Text = rate.ToString("N0") + "Hz";
            infoMS110.rate[3] = rate;
            infoMS110.length[3] = length;
            infoMS110.filenames[3] = Path.GetFullPath(openFileDialog1.FileName);
            labelFile4.Text = "FILE 4 : \r\n" + openFileDialog1.FileName;
            updatePage();
        }

        private void buttonRemove4_Click(object sender, EventArgs e)
        {
            labelFile4.Text = "FILE 4 :";
            labelInfo4.Text = "  ";
            labelInfoVoiceLength4.Text = "  ";
            infoMS110.filenames[3] = "";
            infoMS110.rate[3] = infoMS110.length[3] = 0;
            updatePage();
        }

        private void checkBoxKEYOVERW_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxKEYOVERW.Checked)
            {
                infoMS110.globalOPT |= 0x01;
            }else
            {
                infoMS110.globalOPT &= ~0x01;
            }
        }

        private void checkBox3V_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox3V.Checked)
            {
                infoMS110.globalOPT |= 0x02;
            }
            else
            {
                infoMS110.globalOPT &= ~0x02;
            }
        }

        private void checkBoxAUTORPT_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxAUTORPT.Checked)
            {
                infoMS110.globalOPT |= 0x04;
            }
            else
            {
                infoMS110.globalOPT &= ~0x04;
            }
        }

        private void checkBoxSEQ_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxSEQ.Checked)
            {
                checkBoxRANDOM.Checked = false;
                infoMS110.globalOPT |= 0x08;
                groupBox3.Enabled = false;
                groupBox5.Enabled = false;
                groupBox7.Enabled = false;
            }
            else
            {
                groupBox3.Enabled = true;
                groupBox5.Enabled = true;
                groupBox7.Enabled = true;
                infoMS110.globalOPT &= ~0x08;
            }
            updatePage();
        }

        private void checkBoxRANDOM_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxRANDOM.Checked)
            {
                checkBoxSEQ.Checked = false;
                groupBox3.Enabled = false;
                groupBox5.Enabled = false;
                groupBox7.Enabled = false;
                infoMS110.globalOPT |= 0x10;
            }else
            {
                groupBox3.Enabled = true;
                groupBox5.Enabled = true;
                groupBox7.Enabled = true;
                infoMS110.globalOPT &= ~0x10;
            }
        }

        //private void comboBoxSPISIZE_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (comboBoxSPISIZE.SelectedIndex < 0)
        //        infoMS110.spiSize = 2048 * 1024;
        //    else
        //    {
        //        switch(comboBoxSPISIZE.SelectedIndex)
        //        {
        //            case 0: infoMS110.spiSize = 1024 * 1024 * 2;break;
        //            case 1: infoMS110.spiSize = 1024 * 1024 * 4; break;
        //            case 2: infoMS110.spiSize = 1024 * 1024 * 8; break;
        //            case 3: infoMS110.spiSize = 1024 * 1024 * 16; break;
        //            case 4: infoMS110.spiSize = 1024 * 1024 * 32; break;
        //            case 5: infoMS110.spiSize = 1024 * 1024 * 64; break;
        //            case 6: infoMS110.spiSize = 1024 * 1024 * 128; break;
        //            default: infoMS110.spiSize = 2048 * 1024; break;
        //        }
        //    }
        //}

        private void comboBoxCPUCLK_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCPUCLK.SelectedIndex <1)
                infoMS110.clkRate = 28000000;
            else
                infoMS110.clkRate = 20000000;
        }

        private void checkBoxLEVELHOLD1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxLEVELHOLD1.Checked)
                infoMS110.ioctl[0] |= 0x01;
            else
                infoMS110.ioctl[0] &= 0xfe;
        }

        private void checkBoxLEVELHOLD2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxLEVELHOLD2.Checked)
                infoMS110.ioctl[1] |= 0x01;
            else
                infoMS110.ioctl[1] &= 0xfe;
        }

        private void checkBoxLEVELHOLD3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxLEVELHOLD3.Checked)
                infoMS110.ioctl[2] |= 0x01;
            else
                infoMS110.ioctl[2] &= 0xfe;
        }

        private void checkBoxLEVELHOLD4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxLEVELHOLD4.Checked)
                infoMS110.ioctl[3] |= 0x01;
            else
                infoMS110.ioctl[3] &= 0xfe;
        }

        private void checkBoxNONRETRIG1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxNONRETRIG1.Checked)
                infoMS110.ioctl[0] |= 0x02;
            else
                infoMS110.ioctl[0] &= 0xfd;
        }

        private void checkBoxNONRETRIG2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxNONRETRIG2.Checked)
                infoMS110.ioctl[1] |= 0x02;
            else
                infoMS110.ioctl[1] &= 0xfd;
        }

        private void checkBoxNONRETRIG3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxNONRETRIG3.Checked)
                infoMS110.ioctl[2] |= 0x02;
            else
                infoMS110.ioctl[2] &= 0xfd;
        }


        private void checkBoxNONRETRIG4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxNONRETRIG4.Checked)
                infoMS110.ioctl[3] |= 0x02;
            else
                infoMS110.ioctl[3] &= 0xfd;
        }

        private void checkBoxEXTPULLHIGH1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEXTPULLHIGH1.Checked)
                infoMS110.ioctl[0] |= 0x04;
            else
                infoMS110.ioctl[0] &= 0xfb;
        }

        private void checkBoxEXTPULLHIGH2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEXTPULLHIGH2.Checked)
                infoMS110.ioctl[1] |= 0x04;
            else
                infoMS110.ioctl[1] &= 0xfb;

        }
        private void checkBoxEXTPULLHIGH3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEXTPULLHIGH3.Checked)
                infoMS110.ioctl[2] |= 0x04;
            else
                infoMS110.ioctl[2] &= 0xfb;

        }

        private void checkBoxEXTPULLHIGH4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEXTPULLHIGH4.Checked)
                infoMS110.ioctl[3] |= 0x04;
            else
                infoMS110.ioctl[3] &= 0xfb;
        }

        private void radioButtonLEDHIGH1_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButtonLEDHIGH1.Checked)
            {
                infoMS110.ledopt[0] = 0;
                infoMS110.ledopt[1] = 0;
                trackBarLED1.Enabled = false;
                labelSense1.Enabled = false;
                labelSenseMost1.Enabled = false;
                labelSenseLess1.Enabled = false;
                radioButtonLEDLEVEL1.Text = "Flash base on Volume";
            }
        }

        private void radioButtonLEDLOW1_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButtonLEDLOW1.Checked)
            {
                infoMS110.ledopt[0] = 1;
                infoMS110.ledopt[1] = 0;
                trackBarLED1.Enabled = false;
                labelSense1.Enabled = false;
                labelSenseMost1.Enabled = false;
                labelSenseLess1.Enabled = false;
                radioButtonLEDLEVEL1.Text = "Flash base on Volume";
            }
        }

        private void radioButtonLEDFLASH1_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButtonLEDFLASH1.Checked)
            {
                infoMS110.ledopt[0] = 2;
                infoMS110.ledopt[1] = 0;
                trackBarLED1.Enabled = false;
                labelSense1.Enabled = false;
                labelSenseMost1.Enabled = false;
                labelSenseLess1.Enabled = false;
                radioButtonLEDLEVEL1.Text = "Flash base on Volume";
            }
        }

        private void radioButtonLEDLEVEL1_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButtonLEDLEVEL1.Checked)
            {
                infoMS110.ledopt[0] = 4;
                trackBarLED1.Enabled = true;
                labelSense1.Enabled = true;
                labelSenseMost1.Enabled = true;
                labelSenseLess1.Enabled = true;
                infoMS110.ledopt[1] = (byte) trackBarLED1.Value;
                radioButtonLEDLEVEL1.Text = "Flash when Vol. > Lv. " + infoMS110.ledopt[1];
            }
        }

        private void trackBarLED1_Scroll(object sender, EventArgs e)
        {
            infoMS110.ledopt[1] = (byte)trackBarLED1.Value;
            radioButtonLEDLEVEL1.Text = "Flash when Vol. > Lv. " + infoMS110.ledopt[1];
        }

        private void radioButtonLEDHIGH2_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButtonLEDHIGH2.Checked)
            {
                infoMS110.ledopt[2] = 0;
                infoMS110.ledopt[3] = 0;
                trackBarLED2.Enabled = false;
                labelSense2.Enabled = false;
                labelSenseMost2.Enabled = false;
                labelSenseLess2.Enabled = false;
                radioButtonLEVEL2.Text = "Flash base on Volume";
            }
        }

        private void radioButtonLEDLOW2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLEDLOW2.Checked)
            {
                infoMS110.ledopt[2] = 1;
                infoMS110.ledopt[3] = 0;
                trackBarLED2.Enabled = false;
                labelSense2.Enabled = false;
                labelSenseMost2.Enabled = false;
                labelSenseLess2.Enabled = false;
                radioButtonLEVEL2.Text = "Flash base on Volume";
            }
        }

        private void radioButtonFLASH2_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButtonFLASH2.Checked)
            {
                infoMS110.ledopt[2] = 2;
                infoMS110.ledopt[3] = 0;
                trackBarLED2.Enabled = false;
                labelSense2.Enabled = false;
                labelSenseMost2.Enabled = false;
                labelSenseLess2.Enabled = false;
                radioButtonLEVEL2.Text = "Flash base on Volume";
            }
        }

        private void radioButtonLEVEL2_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButtonLEVEL2.Checked)
            {
                infoMS110.ledopt[2] = 4;
                trackBarLED2.Enabled = true;
                labelSense2.Enabled = true;
                labelSenseMost2.Enabled = true;
                labelSenseLess2.Enabled = true;
                infoMS110.ledopt[3] = (byte)trackBarLED2.Value;
                radioButtonLEVEL2.Text = "Flash when Vol. > Lv. " + infoMS110.ledopt[3];
            }
        }

        private void trackBarLED2_Scroll(object sender, EventArgs e)
        {
            infoMS110.ledopt[3] = (byte)trackBarLED2.Value;
            radioButtonLEVEL2.Text = "Flash when Vol. > Lv." + infoMS110.ledopt[3];
        }

        private int calculateEndPage(int[] startPage, int[]endPage, int[] rate, int firstpage=0x11)
        {
            int page = firstpage; // from 0x1100;
            int memLen;
            int i;
            // only adpcm
            for (i = 0; i < WaveNumber; i++)
            {
                memLen = (((infoMS110.length[i] + 1) / 2) + 255) / 256; // unit is page
                startPage[i] = page;
                endPage[i] = page + memLen;
                page = endPage[i];
                if (infoMS110.rate[i] != 0)
                    rate[i] = infoMS110.clkRate / (4 * infoMS110.rate[i]) - 1;
                else
                    rate[i] = 1;
            }
            return endPage[WaveNumber-1];
        }

        bool updatePage()
        {
            int page = 0x11;
            float EstimateFileSize = 0;
            int i, totalLen;
            if ((infoMS110.globalOPT & 0x018) != 0) // both rand/seq mode
            {
                page = 0x30;
            }
            page = calculateEndPage(startPage, endPage, rate, page);
            if (page > 65536)
            {
                DialogResult res = MessageBox.Show("Total Voice Size Over 128Mb", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            EstimateFileSize = (float)page / 512;


            //if (infoMS110.length[0] == 0 && infoMS110.length[1] == 0 && infoMS110.length[2] == 0 && infoMS110.length[3] == 0 )
            for (i = 0, totalLen = 0; i < WaveNumber; i++)
                totalLen += infoMS110.length[i];
            if(totalLen==0)
            {
                labelSPI.Text = "Flash : ";
                labelFileSize.Text = "Used : ";
            }

            else
            {
                labelFileSize.Text = "Used : " + EstimateFileSize.ToString("F2") + "M";

                if (page > 32768)
                    labelSPI.Text = "Flash : 128M";
                else if (page > 16384)
                    labelSPI.Text = "Flash : 64M";
                else if (page > 8192)
                    labelSPI.Text = "Flash : 32M";
                else if (page > 4096)
                    labelSPI.Text = "Flash : 16M";
                else if (page > 2048)
                    labelSPI.Text = "Flash : 8M";
                else if (page > 1024)
                    labelSPI.Text = "Flash : 4M";
                else
                    labelSPI.Text = "Flash : 2M";
            }
            return true;
            
        }

        private void buttonGENBIN_Click(object sender, EventArgs e)
        {
            int i, totalLen;
            for (i = 0, totalLen = 0; i < WaveNumber; i++)
                totalLen += infoMS110.length[i];
            //if (infoMS110.length[0] == 0 && infoMS110.length[1] == 0 && infoMS110.length[2] == 0 && infoMS110.length[3] == 0)
            if(totalLen==0)
            {
                MessageBox.Show("No Voice File Loaded.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            saveFileDialog1.Title = "Generate the SPI FLASH BIN file..";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.FileName = "";
            if(saveFileDialog1.ShowDialog()!=DialogResult.OK)
            {
                textBoxLog.AppendText("Generate bin file cancelled.\r\n");
                return;
            }
            labelOutputFileName.Text = "BIN FILE : \r\n" + saveFileDialog1.FileName;
            try
            {
                int addrnow;
                byte[] dummy;


                // calculate address of voice start
                //int page = 0x11; // from 0x1100;
                //int memLen;
                //// only adpcm
                //for(i=0;i<4;i++)
                //{
                //    memLen = (((infoMS110.length[i] + 1) / 2)+255)/256; // unit is page
                //    startPage[i] = page;
                //    endPage[i] = page + memLen;
                //    page = endPage[i];
                //    if (infoMS110.rate[i] != 0)
                //        rate[i] = infoMS110.clkRate / (4 * infoMS110.rate[i]) - 1;
                //    else
                //        rate[i] = 1;
                //}


                //if(page>(infoMS110.spiSize/8/256))
                //{
                //    textBoxLog.AppendText("SPI size not enough.. give up\r\n");
                //    return;
                //}
                if(!updatePage())
                {
                    return;
                }

                BinaryWriter binwr = new BinaryWriter(File.Open(saveFileDialog1.FileName, FileMode.Create));
                binwr.Write(ms311as110code);
                addrnow = ms311as110code.Length;
                if(addrnow<0x1000)
                {
                    dummy = new byte[0x1000 - addrnow];
                    for (i = 0; i < dummy.Length; i++)
                        dummy[i] = 0xff;
                    binwr.Write(dummy);
                    addrnow = 0x1000;
                }
                byte global0 = (byte)infoMS110.globalOPT;
                binwr.Write(global0); addrnow++;

                // following is IO configuration
                for (i = 0; i < WaveNumber-1; i++) // poweron-msg no voice ctl
                    binwr.Write(infoMS110.ioctl[i]);
                addrnow += (WaveNumber-1);

                for (i = 0; i < 4; i++) // 4 means led*2
                {
                    // turn LEVEL to log scale
                    if(i==0 || i==2)
                        binwr.Write(infoMS110.ledopt[i]);
                    else
                    {
                        double step = Math.Log(65535.0 / 255.0) / 17.0;
                        byte threshold = (byte)(Math.Exp(step * (double)infoMS110.ledopt[i])*255.0/256.0);
                        binwr.Write(threshold);
                    }
                }
                addrnow += 4;

                // next is THRLD, 4ms, clkrate / 8 is the clock rate
                byte thrld = (byte)(256 - (infoMS110.clkRate/8) * 4 / 256000);
                binwr.Write(thrld); addrnow++;
                // next is voice mask
                byte voicemask = 0;

                for (i = 0; i < WaveNumber; i++)
                    if (infoMS110.filenames[i] == "") voicemask |= (byte)(1 << i);
                binwr.Write(voicemask); addrnow++;
                double dbdiv20 = ((double)infoMS110.voiceVolume - 6.0) * 3.02/20.0; // per step 3db
                double gain = Math.Exp(dbdiv20 * Math.Log(10.0));
                global0 = (byte)(((double)0x78) * gain);
                binwr.Write(global0); addrnow++;
                
                while (addrnow < 0x1010)
                {
                    binwr.Write((byte)0xff);
                    addrnow++;
                }
                // following 8 bytes are start addr
                for(i=0;i<WaveNumber;i++)
                {
                    binwr.Write((byte)(startPage[i] & 0xff));
                    binwr.Write((byte)(startPage[i] >>8));
                    addrnow += 2;
                }
                for (i = 0; i < WaveNumber; i++)
                {
                    binwr.Write((byte)(endPage[i] & 0xff));
                    binwr.Write((byte)(endPage[i] >> 8));
                    addrnow += 2;
                }
                for (i = 0; i < WaveNumber; i++)
                {
                    binwr.Write((byte)(rate[i] & 0xff));
                    binwr.Write((byte)(rate[i] >> 8));
                    addrnow += 2;
                }
       
                if ((infoMS110.globalOPT & 0x18) != 0) // only for seq, random mode
                {
                    dummy = new byte[0x3000 - addrnow];
                }
                else
                {
                    dummy = new byte[0x1100 - addrnow];
                }
                for (i = 0; i < dummy.Length; i++)
                    dummy[i] = 0xff;
                binwr.Write(dummy);
                
                int[] rate2=new int[WaveNumber];
                for(i=0;i<WaveNumber;i++)
                {
                    if(infoMS110.filenames[i]!="")
                        write_wav_to_bin(binwr, infoMS110.filenames[i], 'N', out rate2[i]);
                }

                binwr.Close();

            }catch(Exception errwrbin)
            {
                textBoxLog.AppendText("Output BIN File Fail.. " + errwrbin.Message+"\r\n");
                return;
            }
            textBoxLog.Text = "";
            textBoxLog.AppendText("Output BIN File:\r\n" + saveFileDialog1.FileName + "\r\n");
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void labelInfo1_Click(object sender, EventArgs e)
        {

        }

        private void labelSPI_Click(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click_1(object sender, EventArgs e)
        {

        }

        private void label19_Click_2(object sender, EventArgs e)
        {

        }

        private void labelFileSize_Click(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            labelVolLevel.Text = "Level " + trackBar1.Value;
            infoMS110.voiceVolume = trackBar1.Value;
        }

        private void buttonLoadPowerOn_Click(object sender, EventArgs e)
        {
            float VoiceLengthSecond1;
            openFileDialog1.Title = "Select POWER-ON WAV file..";
            openFileDialog1.FileName = "";
            openFileDialog1.FilterIndex = 2;
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                textBoxLog.AppendText("Select POWER-ON WAV file cancelled.\r\n");
                return;
            }
            // check the wav file
            int rate = 8000;
            int length = 0;
            if (!wav_valid_check(openFileDialog1.FileName, ref length, ref rate))
            {
                textBoxLog.AppendText("Open WAV failed!!\r\n");
                return;
            }
            // now it is valid
            VoiceLengthSecond1 = (float)length / (float)rate;
            labelVoiceLengthPowerOn.Text = VoiceLengthSecond1.ToString("F2") + "s";
            labelInfoPowerOn.Text = rate.ToString("N0") + "Hz \r\n ";
            infoMS110.length[4] = length;
            infoMS110.rate[4] = rate;
            infoMS110.filenames[4] = Path.GetFullPath(openFileDialog1.FileName);
            labelFilePowerOn.Text = "POWER-ON :\r\n" + openFileDialog1.FileName;
            updatePage();
        }

        private void buttonRemovePowerOn_Click(object sender, EventArgs e)
        {
            labelFilePowerOn.Text = "POWER-ON :";
            //labelInfo1.Text = "  ";
            labelVoiceLengthPowerOn.Text = "  ";
            infoMS110.filenames[4] = "";
            infoMS110.length[4] = infoMS110.rate[4] = 0; // bug fix
            labelInfoPowerOn.Text = "";
            updatePage();
        }
    }
    public class ms110info
    {
        public string[] filenames;

        public int globalOPT;
        public byte[] ioctl;
        public byte[] ledopt;
        public int[] length;
        public int[] rate;
        public int clkRate;
        public int spiSize;
        public int voiceVolume;
        public ms110info()
        {
            int i;
            filenames = new string[FormMS311as110.WaveNumber];
            ioctl = new byte[FormMS311as110.WaveNumber];
            ledopt = new byte[FormMS311as110.WaveNumber];
            length = new int[FormMS311as110.WaveNumber];
            rate = new int[FormMS311as110.WaveNumber];

            for (i = 0; i < FormMS311as110.WaveNumber; i++)
            {
                filenames[i] = "";
                ioctl[i] = 0;
                ledopt[i] = 0;
                length[i] = 0;
                rate[i] = 0;
            }
                
            globalOPT = 0;
            voiceVolume = 6;
            
            clkRate = 28000000;
            spiSize = 2048 * 1024;
        }


    }
}
