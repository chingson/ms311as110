﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Timers;
using System.Diagnostics;
using System.IO;
// 2016 March, remove FTDXX
using FTD2XX_NET;

namespace ms110spiWri
{
    // wrapped ftdi operation
    //public partial class sfr_info_item
    //{
    //    public string sfrname;
    //    public int sfraddr;
    //    public int forbid;
    //    public sfr_info_item(string s, int a, int f) { sfrname = s; sfraddr = a; forbid = f; }
    //};
    //public class sfrs : List<sfr_info_item>
    //{
    //    public void Add(string s, int a, int f)
    //    {
    //        Add(new sfr_info_item(s, a, f));
    //    }
    //}
    public class ftop 
    {
        public const int BREAK0_H = 0x1000;
        public const int BREAK0_M = 0x1001;
        public const int BREAK0_L = 0x1002;
        public const int BREAK1_H = 0x1003;
        public const int BREAK1_M = 0x1004;
        public const int BREAK1_L = 0x1005;
        public const int BREAK2_H = 0x1006;
        public const int BREAK2_M = 0x1007;
        public const int BREAK2_L = 0x1008;
        public const int BREAK3_H = 0x1009;
        public const int BREAK3_M = 0x100a;
        public const int BREAK3_L = 0x100b;
        public const int BREAK4_H = 0x100c;
        public const int BREAK4_M = 0x100d;
        public const int BREAK4_L = 0x100e;
        public const int BREAK5_H = 0x100f;
        public const int BREAK5_M = 0x1010;
        public const int BREAK5_L = 0x1011;
        public const int BREAK6_H = 0x1012;
        public const int BREAK6_M = 0x1013;
        public const int BREAK6_L = 0x1014;
        public const int BREAK7_H = 0x1015;
        public const int BREAK7_M = 0x1016;
        public const int BREAK7_L = 0x1017;
        public const int BREAK8_H = 0x1018;
        public const int BREAK8_M = 0x1019;
        public const int BREAK8_L = 0x101a;
        public const int BREAK9_H = 0x101b;
        public const int BREAK9_M = 0x101c;
        public const int BREAK9_L = 0x101d;
        public const int BREAKA_H = 0x101e;
        public const int BREAKA_M = 0x101f;
        public const int BREAKA_L = 0x1020;
        public const int BREAKB_H = 0x1021;
        public const int BREAKB_M = 0x1022;
        public const int BREAKB_L = 0x1023;
        public const int BREAKC_H = 0x1024;
        public const int BREAKC_M = 0x1025;
        public const int BREAKC_L = 0x1026;
        public const int BREAKD_H = 0x1027;
        public const int BREAKD_M = 0x1028;
        public const int BREAKD_L = 0x1029;
        public const int BREAKE_H = 0x102a;
        public const int BREAKE_M = 0x102b;
        public const int BREAKE_L = 0x102c;
        public const int BREAKF_H = 0x102d;
        public const int BREAKF_M = 0x102e;
        public const int BREAKF_L = 0x102f;
        public const int PCS0_H = 0x1030;
        public const int PCS0_M = 0x1031;
        public const int PCS0_L = 0x1032;
        public const int PCS1_H = 0x1033;
        public const int PCS1_M = 0x1034;
        public const int PCS1_L = 0x1035;
        public const int PCS2_H = 0x1036;
        public const int PCS2_M = 0x1037;
        public const int PCS2_L = 0x1038;
        public const int PCS3_H = 0x1039;
        public const int PCS3_M = 0x103a;
        public const int PCS3_L = 0x103b;
        public const int PCS4_H = 0x103c;
        public const int PCS4_M = 0x103d;
        public const int PCS4_L = 0x103e;
        public const int PCS5_H = 0x103f;
        public const int PCS5_M = 0x1040;
        public const int PCS5_L = 0x1041;
        public const int PCS6_H = 0x1042;
        public const int PCS6_M = 0x1043;
        public const int PCS6_L = 0x1044;
        public const int PCS7_H = 0x1045;
        public const int PCS7_M = 0x1046;
        public const int PCS7_L = 0x1047;
        public const int PCS8_H = 0x1048;
        public const int PCS8_M = 0x1049;
        public const int PCS8_L = 0x104a;
        public const int PCS9_H = 0x104b;
        public const int PCS9_M = 0x104c;
        public const int PCS9_L = 0x104d;
        public const int PCS10_H = 0x104e;
        public const int PCS10_M = 0x104f;
        public const int PCS10_L = 0x1050;
        public const int PCS11_H = 0x1051;
        public const int PCS11_M = 0x1052;
        public const int PCS11_L = 0x1053;
        public const int PCS12_H = 0x1054;
        public const int PCS12_M = 0x1055;
        public const int PCS12_L = 0x1056;
        public const int PCS13_H = 0x1057;
        public const int PCS13_M = 0x1058;
        public const int PCS13_L = 0x1059;
        public const int PCS14_H = 0x105a;
        public const int PCS14_M = 0x105b;
        public const int PCS14_L = 0x105c;
        public const int PCS15_H = 0x105d;
        public const int PCS15_M = 0x105e;
        public const int PCS15_L = 0x105f;
        public const int MTCYCLE = 0x1060;
        public const int CYCLE_H = 0x1061;
        public const int CYCLE_M = 0x1062;
        public const int CYCLE_L = 0x1063;
        public const int CYCLE_LL = 0x1064;
        public const int CHIPID_H = 0x1065;
        public const int CHIPID_L = 0x1066;
        public const int WATCHEN = 0x1067;
        public const int WATCHD = 0x1068;
        public const int WATCHAR_H = 0x1069;
        public const int WATCHAR_L = 0x106a;
        public const int PCU = 0x106b;
        public const int PCH = 0x106c;
        public const int PCL = 0x106d;
        public const int H08Sel = 0x106E; // =0 選擇 H08A , =1 選擇 H08B
        public UInt32 ftdiDeviceCount;
        public FTDI.FT_STATUS ftStatus;
        FTDI myFtdiDevice;
        HIDDev  myHid=null;


        public ms326_reg ms326reg;
        public uint fpgaid;
        
        public byte[] outfifo;
        public byte[] infifo;
        public byte[] ackchk;
        public byte[] dataque;
        public byte[] tempdata;
        public byte[] tdibytes;
        public byte[] tdobytes;
        byte[] hiddat;// = new byte[65];
        byte[] hidrb;// = new byte[65];
        public UInt32 readbytecnt;
        public UInt32 ofifocnt;
        public UInt32 dataneedread;
        public string errmsg;

        // data commands
        public const byte MSB_PVE_OUT_BYTE = 0x10;
        public const byte MSB_NVE_OUT_BYTE = 0x11;
        public const byte MSB_PVE_OUT_BIT = 0x12;
        public const byte MSB_NVE_OUT_BIT = 0x13;
        public const byte MSB_PVE_IN_BYTE = 0x20;
        public const byte MSB_NVE_IN_BYTE = 0x24;
        public const byte MSB_PVE_IN_BIT = 0x22;
        public const byte MSB_NVE_IN_BIT = 0x26;
        public const byte MSB_PNVE_IO_BYTE = 0x31;
        public const byte MSB_NPVE_IO_BYTE = 0x34;
        public const byte MSB_PNVE_IO_BIT = 0x33;
        public const byte MSB_NPVE_IO_BIT = 036;
        public const byte CKO_NO_TRANSFER_BITS = 0x8e;
        public const byte CKO_NO_TRANSFER_BYTES = 0x8f;

        public const int FT_FIFO_SIZE = 65536 * 2;


        public const int HYICE_CTR_ADDRDATA = (1 << 22);
        public const int HYICE_CTR_ICECWR = 0;
        public const int HYICE_CTR_ICECRD = (2 << 22);
        public const int HYICE_ProSTPOV = 1 << 21;
        public const int HYICE_ProMD1 = 1 << 20;
        public const int HYICE_ProMD0 = 1 << 19;
        public const int HYICE_ProCSUP = 1 << 18;
        public const int HYICE_ProCSRAM = 1 << 17;
        public const int HYICE_ProICEGO = 1 << 16;
        public const int HYICE_ProRST = 1 << 15;


        public const int ADB_DEFAULT_OUT = 0xdb;// ADB5 input, ADB2 input for MISO

        public const int ADB_TM_OUT = 0xdf; // ADB2 need output when entering test mode
        //public const int ADB_TM_OUT2 = 0xc7; // ADB3 input, connect to IO6

        public const int ACB_DEFAULT_OUT = 0xfb; // ACB2 is input, for key detect


        public int icestat_now;


// ICE constants
       



        public const byte ICE_OE = 0xdb; // adb5 is input!!
        public const byte MS326_ADB_OUT=0xfb;

        public int status;
        public bool useHID;
        public byte lbyteshadow;
        public byte hbyteshadow;

        public byte[] bit_rev_tab;

        public int jtag_ir_now;

        public readonly byte[] CRC8table={0x00, 0x07, 0x0E, 0x09, 0x1C, 0x1B, 0x12, 0x15, 0x38, 0x3F, 0x36, 0x31, 0x24, 0x23, 0x2A, 0x2D,
0x70, 0x77, 0x7E, 0x79, 0x6C, 0x6B, 0x62, 0x65, 0x48, 0x4F, 0x46, 0x41, 0x54, 0x53, 0x5A, 0x5D,
0xE0, 0xE7, 0xEE, 0xE9, 0xFC, 0xFB, 0xF2, 0xF5, 0xD8, 0xDF, 0xD6, 0xD1, 0xC4, 0xC3, 0xCA, 0xCD,
0x90, 0x97, 0x9E, 0x99, 0x8C, 0x8B, 0x82, 0x85, 0xA8, 0xAF, 0xA6, 0xA1, 0xB4, 0xB3, 0xBA, 0xBD,
0xC7, 0xC0, 0xC9, 0xCE, 0xDB, 0xDC, 0xD5, 0xD2, 0xFF, 0xF8, 0xF1, 0xF6, 0xE3, 0xE4, 0xED, 0xEA,
0xB7, 0xB0, 0xB9, 0xBE, 0xAB, 0xAC, 0xA5, 0xA2, 0x8F, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9D, 0x9A,
0x27, 0x20, 0x29, 0x2E, 0x3B, 0x3C, 0x35, 0x32, 0x1F, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0D, 0x0A,
0x57, 0x50, 0x59, 0x5E, 0x4B, 0x4C, 0x45, 0x42, 0x6F, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7D, 0x7A,
0x89, 0x8E, 0x87, 0x80, 0x95, 0x92, 0x9B, 0x9C, 0xB1, 0xB6, 0xBF, 0xB8, 0xAD, 0xAA, 0xA3, 0xA4,
0xF9, 0xFE, 0xF7, 0xF0, 0xE5, 0xE2, 0xEB, 0xEC, 0xC1, 0xC6, 0xCF, 0xC8, 0xDD, 0xDA, 0xD3, 0xD4,
0x69, 0x6E, 0x67, 0x60, 0x75, 0x72, 0x7B, 0x7C, 0x51, 0x56, 0x5F, 0x58, 0x4D, 0x4A, 0x43, 0x44,
0x19, 0x1E, 0x17, 0x10, 0x05, 0x02, 0x0B, 0x0C, 0x21, 0x26, 0x2F, 0x28, 0x3D, 0x3A, 0x33, 0x34,
0x4E, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5C, 0x5B, 0x76, 0x71, 0x78, 0x7F, 0x6A, 0x6D, 0x64, 0x63,
0x3E, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2C, 0x2B, 0x06, 0x01, 0x08, 0x0F, 0x1A, 0x1D, 0x14, 0x13,
0xAE, 0xA9, 0xA0, 0xA7, 0xB2, 0xB5, 0xBC, 0xBB, 0x96, 0x91, 0x98, 0x9F, 0x8A, 0x8D, 0x84, 0x83,
0xDE, 0xD9, 0xD0, 0xD7, 0xC2, 0xC5, 0xCC, 0xCB, 0xE6, 0xE1, 0xE8, 0xEF, 0xFA, 0xFD, 0xF4, 0xF3};

        // 2 more clock then csh
        public readonly int hycon_ice_cshn = 5;



        public readonly byte[] hycon_ice_csh = { 0x8e, 0x1, 0x80, 0xf8, ftop.ICE_OE };

        

        // the function callback check if this device can be used!!
        public delegate bool CHKDEV( ftop thedev);
        public delegate uint WRISPICB(uint addrnow);
        // the following devid has always <<1

        // address is follows:
        // ff90: mass erase, {M_ERASE,ISP_EN, EXT_FLASH, DATA_PROG, CRC_STATUS, 000}
        // FF91: PAGE ERASE, {P_ERASE,PAGEA[16:10]}, POLLING MSB WHEN PAGE ERASE
        // FF92: START ADDR HIGH (CRC & PROG)
        // FF93: START ADDR LOW
        // FF94: DATA_PROG
        // 

        // return int, 0 means success, otherwise error code


        //public int ms8951_chkiic()
        //{
        //    if (!ms8951_write2byte(0x6e, 0x01, 0x02,0x03))
        //        return -1;
        //    return 0;
        //}


        //public int ms8951_crc8(int part, ref byte crc8) // 0 is low address, 1 is high address
        //{
        //    byte chk=0xff;
        //    int count;
        //    if (part != 0)
        //    {
        //        if (!ms8951_write1byte(0x94, 0x90, 0x60))
        //            return 1;
        //        if (!ms8951_write2byte(0x96, 0x92, 0x00, 0x00))
        //            return 2;
        //        if (!ms8951_write2byte(0x96, 0xa0, 0x7f, 0xff))
        //            return 3;
        //    }
        //    else
        //    {
        //        if (!ms8951_write1byte(0x94, 0x90, 0x40))
        //            return 4;
        //        if (!ms8951_write2byte(0x96, 0x92, 0x00, 0x00))
        //            return 5;
        //        if (!ms8951_write2byte(0x96, 0xa0, 0xff, 0xff))
        //            return 6;
        //    }
        //    count = 0;
        //    do
        //    {
        //        if (!ms8951_read1byte(0x94, 0x90, ref chk))
        //            return 7;
        //        if (++count >= 100)
        //            return -1;
        //    } while ((chk & 8) != 0);

        //    if (!ms8951_write1byte(0x94, 0xa2, 0x80)) // start crc
        //        return 9;
        //    count = 0;
        //    do
        //    {
        //        if (!ms8951_read1byte(0x94, 0xa2, ref chk))
        //            return 10;
        //        if (++count >= 1000)
        //            return -2;
        //    } while ((chk & 0x80) != 0);
        //    // finally read the crc
        //    if (!ms8951_read1byte(0x94, 0xa3, ref crc8))
        //        return 11;
        //    return 0;
        //}

        //public int ms8951_writecode(byte[] code, int len, byte [] dataimg=null, int imglen=0)
        //{
        //    // minimum unit is 256 bytes
        //    byte[] writebuf;
        //    int i,addr,addrs;
        //    byte[] fullimage;
        //    byte[] fullimaged;

        //    fullimage = new byte[65536];
        //    fullimaged = new byte[32768];
        //    writebuf = new byte[256];


        //    for (i = 0; i < 65536; i++)
        //        fullimage[i] = 0xff;
        //    for (i = 0; i < 32768; i++)
        //        fullimaged[i] = 0xff;

        //    for (i = 0; i < len; i++)
        //        fullimage[i] = code[i];

        //    for (i = 0; i < imglen; i++)
        //        fullimaged[i] = dataimg[i];

        //    byte crc_correct = Crc8.ComputeChecksum(fullimage);
        //    byte crc_correctd = Crc8.ComputeChecksum(fullimaged);

        //    if (!ms8951_write1byte(0x94, 0x90, 0x40))
        //        return 1;
        //    byte chk=0xff;
        //    addr=0;
        //    do
        //    {
        //        int count = 0;
        //        addrs = addr;
        //        for (i = 0; i < 256; i++)
        //            writebuf[i] = 0xff;
        //        for (i = 0; addr < len && i < 256; i++, addr++)
        //        {
        //            writebuf[i] = code[addr];
        //        }
        //        // start address
        //        if (!ms8951_write2byte(0x96, 0x92, (byte)(addrs >> 8), (byte)(addrs & 0xff)))
        //            return 3;

        //        if (!ms8951_write256byte(0x94, 0x94, writebuf))
        //            return 4;
        //        do
        //        {
        //            if (!ms8951_read1byte(0x94, 0x90, ref chk))
        //                return 5;
        //            if (++count >= 1000)
        //                return -1;
        //        } while ((chk & 0x10) != 0);


        //    } while (addr < len);

        //    // now crc
        //    if (ms8951_crc8(0, ref chk) != 0)
        //        return 6;
        //    if (chk != crc_correct)
        //        return 7;
        //    if (imglen == 0)
        //        return 0;
        //    // data area
        //    if (!ms8951_write1byte(0x94, 0x90, 0x60))
        //        return 1;
        //    addr = 0;
        //    do
        //    {
        //        int count = 0;
        //        addrs = addr;
        //        for (i = 0; i < 256; i++)
        //            writebuf[i] = 0xff;
        //        for (i = 0; addr < imglen && i < 256; i++, addr++)
        //        {
        //            writebuf[i] = dataimg[addr];
        //        }
        //        // start address
        //        if (!ms8951_write2byte(0x96, 0x92, (byte)(addrs >> 8), (byte)(addrs & 0xff)))
        //            return 13;

        //        if (!ms8951_write256byte(0x94, 0x94, writebuf))
        //            return 14;
        //        do
        //        {
        //            if (!ms8951_read1byte(0x94, 0x90, ref chk))
        //                return 15;
        //            if (++count >= 1000)
        //                return -1;
        //        } while ((chk & 0x10) != 0);


        //    } while (addr < imglen);
        //    if (ms8951_crc8(1, ref chk) != 0)
        //        return 10;
        //    if (chk != crc_correctd)
        //        return 11;
        //    return 0;
        //}
        //public int ms8951_erase()
        //{
        //    int count;
        //    byte chk1=1;
        //    if(status==0)
        //        return 1;
        //    if (!ms8951_read1byte(0x94, 0x37, ref chk1))
        //        return 4;
        //    errmsg = "get 37h " + chk1.ToString();
        //    if (!ms8951_write1byte(0x94, 0x36, 0x00))
        //        return 3;
        //    if(!ms8951_write1byte(0x94,0x90,0xc0))
        //        return 2;

        //    byte datard=0;
        //    count =0;
        //    do{
        //        if(!ms8951_read1byte(0x94,0x90,ref datard))
        //            return 4;
        //        if(++count>=5000)
        //            return -1;
        //    }while(datard==0xc0);
        //    if(datard!=0x40)
        //        return 5;
        //    // following is check crc
        //    // 0XFF92 IS THE START ADDRESS
        //    if (ms8951_crc8(0, ref datard) != 0)
        //        return 6;
        //    if(datard!=0xde)
        //        return 12;



        //    return 0; // we erase the first 64k is fine.



        //}
        //public bool ms8951_write1byte(byte devid, byte addr, byte data, bool flush=true, byte lbdo=0, byte lbdir=0)
        //{
        //    i2c_start(lbdo, lbdir);
        //    i2c_send_byte((byte)(devid & 0xfe)); // wait ack default true, write no need restart
        //    i2c_send_byte(addr);
        //    i2c_send_byte(data);
        //    return i2c_stop(lbdo, lbdir, flush);

        //}
        //public bool ms8951_write2byte(byte devid, byte addr, byte data1,byte data2, bool flush = true, byte lbdo = 0, byte lbdir = 0)
        //{
        //    i2c_start(lbdo, lbdir);
        //    i2c_send_byte((byte)(devid & 0xfe)); // wait ack default true
        //    i2c_send_byte(addr);
        //    i2c_send_byte(data1);
        //    i2c_send_byte(data2);
        //    return i2c_stop(lbdo, lbdir, flush);

        //}
        //public bool ms8951_write256byte(byte devid, byte addr, byte[] data,bool flush = true, byte lbdo = 0, byte lbdir = 0)
        //{
        //    i2c_start(lbdo, lbdir);
        //    i2c_send_byte((byte)(devid & 0xfe)); // wait ack default true
        //    i2c_send_byte(addr);
        //    for(int i=0;i<256;i++)
        //        i2c_send_byte(data[i]);
        //    return i2c_stop(lbdo, lbdir, flush);

        //}

        //public bool ms8951_read1byte(byte devid, byte addr, ref byte datard, bool flush = true, byte lbdo = 0, byte lbdir = 0)
        //{
        //    byte readback = 0xff;
        //    i2c_start(lbdo, lbdir);
        //    i2c_send_byte((byte)(devid & 0xfe),true, false, lbdo,lbdir); // wait ack default true
        //    i2c_send_byte(addr, true,false, lbdo,lbdir);
        //    i2c_restart(lbdo, lbdir);
        //    i2c_send_byte((byte)(devid |0x01),true,false,lbdo,lbdir); // wait ack default true
        //    if (flush)
        //    {
        //        bool result = i2c_read_byte(ref readback, false, true, lbdo, lbdir); // send=true
        //        if (!result)
        //        {
        //            i2c_stop(lbdo, lbdir, true);

        //            return false;
        //        }
        //        datard = readback;
        //        return i2c_stop(lbdo, lbdir, true);
        //    }

        //    i2c_read_byte(ref readback, false, false, lbdo, lbdir);

        //    return i2c_stop(lbdo, lbdir, flush);

        //}


        //public int SSPI_tr(int cmd)
        //{
        //    set_freq(4000000); // 1MHZ enough
        //    spi_csl();
        //    byteo(0x31); // out on -ve edge, in on +ve edge
        //    byteo(2);
        //    byteo(0);
        //    byteo((byte)((cmd >> 16)&0xff));
        //    byteo((byte)((cmd >> 8)&0xff));
        //    byteo((byte)(cmd & 0xff));
        //    spi_csh();
        //    dataneedread = 3;
        //    send_cmd(true);
        //    //send_cmd();
        //    int sspird = (infifo[0] << 16) | (infifo[1] << 8) | (infifo[2]);
        //    return sspird;

        //}
        //int sspiid;
        //public int MS326_sspicmd(int cmd,int Timeout=20)
        //{
        //    int reply;
        //    cmd |= sspiid << 21;
        //    sspiid++;
        //    while (Timeout-- >=0 )
        //    {
        //        reply=SSPI_tr(cmd);
        //        if ((reply & 0xff0000) == (cmd & 0xff0000))
        //            return reply;
        //    }

        //    return -1;
        //}
        //public int swapshort(int i)
        //{
        //    return ((i >> 8) & 0xff) | ((i & 0xff) << 8);
        //}
        //public bool MS326_write_ram_block(int astart, int afin, byte[] ramdata)
        //{
        //    int i;
        //    int cmd;
        //    int reply;
        //    for (i = astart; i <= afin; i++)
        //    {
        //        // set ptr
        //        cmd = swapshort(i) | (2 << 16);
        //        reply = MS326_sspicmd(cmd);
        //        if (reply < 0)
        //            return false;
        //        cmd = (ramdata[i-astart]<<8) | (3 << 16);
        //        reply = MS326_sspicmd(cmd);
        //        if (reply < 0)
        //            return false;
        //    }
        //    for (i = astart; i <= afin; i++)
        //    {
        //        // set ptr
        //        cmd = swapshort(i) | (1 << 16); // read ram
        //        reply = MS326_sspicmd(cmd);
        //        if (reply < 0)
        //            return false;
        //        if ((reply & 0xff) != ramdata[i - astart])
        //            return false;

        //    }

        //    // after write we check it

        //    return true;
        //}

        //public bool MS326_write_sfr(int sfra, int sfrd)
        //{
        //    int cmd = (5 << 16) | (sfra << 8) | sfrd;
        //    int reply = MS326_sspicmd(cmd);
        //    if (reply < 0)
        //        return false;
        //    return true;

        //}
        //public void MS326_givepwrrun()
        //{
        //    outByteLow(0x0, ICE_OE);
        //    send_cmd();
        //    Thread.Sleep(100);
        //    outByteLow(0x40, 0x40); // only 1 is high
        //    send_cmd();
        //}
        bool send_cmd_short()
        {
            uint k = 0;
            myFtdiDevice.Write(outfifo, (int) ofifocnt, ref k);
            if (k != ofifocnt)
                return false;
            ofifocnt = 0;
            return true;
        }
        public bool send_cmd(bool skipchk = false)
        {
            uint datawriten = 0;
            uint dataread = 0;
            if (ofifocnt == 0)
                return true;
            if (dataneedread != 0)
                byteo(0x87);
            ftStatus = myFtdiDevice.Write(outfifo, (int)ofifocnt, ref datawriten);
            if (ftStatus != FTDI.FT_STATUS.FT_OK || datawriten != ofifocnt)
            {
                ofifocnt -= datawriten;
                errmsg = "not equal fo/fob";
                return false;
            }
            if (dataneedread == 0)
            {
                ofifocnt = 0;
                return true;
            }
            ftStatus = myFtdiDevice.Read(infifo, dataneedread, ref dataread);
            if (ftStatus != FTDI.FT_STATUS.FT_OK || dataread != dataneedread)
            {
                ofifocnt -= datawriten;
                errmsg = "not equal fi/fib";
                return false;
            }
            ofifocnt = 0;
            readbytecnt = 0;
            if (!skipchk)
            {
                for (uint i = 0; i < dataneedread; i++)
                {
                    if (ackchk[i] == 1) // ack check byte
                    {
                        if ((infifo[i] & 0x01) != 0)
                        {
                            errmsg = "ack check error.";
                            dataneedread = 0;
                            return false;
                        }
                    }
                    else if (ackchk[i] == 2)
                    {
                        dataque[readbytecnt++] = infifo[i];
                    }

                }
            }
            dataneedread = 0;
            myFtdiDevice.Purge(FTDI.FT_PURGE.FT_PURGE_RX | FTDI.FT_PURGE.FT_PURGE_TX);

            return true;

        }
        public void byteo(byte d)
        {
            outfifo[ofifocnt++] = d;
        }
        private int tms_count;
        private int adb_out_val;
        private int acb_out_val;
        private int cable_state = 0;
        
        public bool jtag_tms(int value, int tmssel)
        {
            if(tmssel==0)
            {
                adb_out_val = adb_out_val & 0xef;
                if (value!=0)
                    adb_out_val |= 0x10;
                acb_out_val = (tms_count >> 4) & 0xf | 0xd0;
            }
            else
            {
                adb_out_val = (adb_out_val & 0xf7);
                if (value != 0)
                    adb_out_val |= 0x8;
                acb_out_val = ((tms_count >> 4) & 0xf) | 0xc0;
            }
            adb_out_val = adb_out_val & 0x3f;
            acb_out_val |= 1;
            tms_count++;
            byteo(0x80);
            byteo((byte)adb_out_val);
            byteo(ADB_DEFAULT_OUT);
            byteo(0x82);
            byteo((byte)acb_out_val);
            byteo(ACB_DEFAULT_OUT);
            return true;
        }
        public bool jtag_tdi(int value)
        {
            if (value != 0)
                adb_out_val |= 2;
            else
                adb_out_val &= 0xfd;
            byteo(0x80);
            byteo((byte)adb_out_val);
            byteo(ADB_DEFAULT_OUT);
            return true;

        }
        public bool jtag_tck(int n)
        {
            if (n == 0)
                return true;
            if (n > 8)
                return false;
            byteo(0x8e);
            byteo((byte)(n - 1));
            return true;
        }
        public bool jtag_tck8(int n)
        {
            if (n <= 0)
                return true;
            if(n>=65536)
            {
                byteo(0x8f);
                byteo(0xff);
                byteo(0xff);
                n -= 65536;

            }
            if (n <= 0)
                return true;
            byteo(0x8f);
            byteo((byte)((n - 1) & 0xff));
            byteo((byte)(((n - 1) >> 8) & 0xff));
            return true;
        }
        public bool jtag_rst(int tmssel)
        {
            jtag_tms(1, tmssel);
            jtag_tck(5);
            send_cmd_short();
            if (tmssel == 1)
                cable_state = 0;
            return true;
        }
        public bool jtag_write_tdi(int bits, byte[] buf, bool rev=false, bool flush=false)
        {
            int i, j, k;
            i = bits / 8;
            j = bits % 8;
            if (i!=0)
            {
                byteo(0x19); // LSB first, byte out
                byteo((byte)((i - 1) & 0xff));
                byteo((byte)((((i - 1) >> 8) & 0xff)));
                for (k = 0; k < i; k++)
                {
                    byteo(((byte)(rev ? bit_rev_tab[buf[k]] : buf[k])));
                }
            }
            if (j!=0)
            {
                byteo(0x1b);// lsb first, byte out
                byteo((byte)(j - 1));
                byteo(buf[i]);
            }
            if (flush)
            {
                return send_cmd_short(); // no need to read because read only
            }
            return true;
        }
        bool jtag_dataio(int bits, byte[] buf_to_dev, byte[] buf_from_dev, int noflush=0)
        {
            int i, j;
            uint k;
            i = bits / 8;
            j = bits % 8;
            if (i!=0)
            {
                byteo(0x39); // rising edge for datain, falling edge data out
                byteo((byte)((i - 1) & 0xff));
                byteo((byte)((((i - 1) >> 8) & 0xff)));
                for (k = 0; k < i; k++)
                    byteo(buf_to_dev[k]);
            }
            if (j!=0)
            {
                byteo(0x3b);
                byteo((byte)(j - 1));
                byteo(buf_to_dev[i]);
            }
            if (noflush==0)
            {
                byteo(0x87);
                k = 0;
                myFtdiDevice.Write(outfifo, (int) ofifocnt, ref k);
                if(k!=ofifocnt)
                {
                    errmsg += "write err.";
                    return false;
                }
                ofifocnt = 0;
                if (j!=0) i++;


                k = 0;
                myFtdiDevice.Read(buf_from_dev, (uint) i, ref k);   // final byte from LSB, for example 2,1,0
                if (k != i)
                {
                    errmsg+=("read bye error 3.\n");
                    return false;
                }

                return true;
            }
            if (j!=0) i++;
            dataneedread += (uint) i;
            return true;
        }
        public bool shift_dr_task(int len, byte[] buf_to_dev, byte[] buf_from_dev,
                           int finaltms=1, int passrun=0, int tmssel=0) // passrun means pass run test idle
        {
            int i, j, len1 = 0;
            uint k;
            byte [] buf = new byte[1];
            byte [] bufo= new byte[1];
            //jtag_tms(1);
            //jtag_tck(1);  always from sel-dr-scan
            //need_read_num=0;
            jtag_tms(0, tmssel);
            //if(!tmssel)
            // jtag_tck(3);        // captured data will shift 1 more out?
            //else
            jtag_tck(2);        // shift-dr after these 2 clocks
                                //SEND_CMD;
            if (len > 1)
            {
                if (!jtag_dataio(len - 1, buf_to_dev, buf_from_dev, 1))// noflush=1
                    return false;
                len1 = (int) dataneedread;
            }
            jtag_tms(1, tmssel);
            // now consider where is the final bit
            // if len=8 final bit is byte0 lsb
            // len=9 final bit is byte1 msb
            // len=10 final bit is byte1 bit 6
            // ...
            // len=15, final bit is byte1 bit 1

            i = (len - 1) / 8;
            j = (len - 1) % 8;
            bufo[0] = buf_to_dev[i];
            bufo[0] >>= j;
            if (!jtag_dataio(1, bufo, buf, 1))// no flush=1
                return false;
            byteo(0x87);
            if (!send_cmd_short())
                return false;
            k = 0;
            myFtdiDevice.Read(buf_from_dev, dataneedread, ref k);
            //FT_Read(ftHandle, buf_from_dev, need_read_num, (unsigned long *) & k);
            if (k != dataneedread)
                return false;
            dataneedread = 0;
            buf[0] = buf_from_dev[len1];
            if (j == 0)
            {
                i++;
                buf_from_dev[i] = buf[0];
                buf_from_dev[i] >>= 7; // align to LSB
            }
            else
            {
                buf_from_dev[i] >>= 1;
                buf_from_dev[i] |= buf[0];
                // consider if j=3, len=4 final need shift left 4
                buf_from_dev[i] >>= (7 - j);
            }

            jtag_tck(1); //update dr after this clock
            if (passrun!=0)
            {
                jtag_tms(0, tmssel);
                jtag_tck(1); // run test idle    after this clock
                jtag_tms(1, tmssel);
                jtag_tck(1); // sel-dr scan

            }
            else
            {
                jtag_tms(finaltms, tmssel);
                jtag_tck(1);
            }
            return true;
        }


        public int ms326_ice_init(ref string msg)
        
        {
            TO_DEBUG_MODE();
            if (0==ms326_do_ice_command(ICECMD_RESET, 0, 1000)) // reset timeout is 2s
            {
                msg = "ice reset1 fail.";
                return 0; // fail
            }
            if (0==ms326_do_ice_command(0, 0, 1000))
            {
                msg = ("ice cannot halt");
                return 0; // fail
            }
            msg = ("ICE Now halt.");
            // reset
            if (0==ms326_do_ice_command(ICECMD_RESET, 0, 1000)) // reset timeout is 2s
            {
                msg+=("ice reset fail");
                return 0; // fail
            }
            msg+=("ICE Now reset.");
            ms326_updatePcAcc(ref msg);


            //if (!do_ice_command(ICECMD_READ_ROMP, 0))
            //{
            //    out2log("ice get romp fail");
            //    return 0;
            //}
            //romptr = data_from_ice[1] | (data_from_ice[2] << 8);
            //if (!do_ice_command(ICECMD_READ_RAMP0, 0))
            //{
            //    out2log("ice get RAMP0 fail");
            //    return 0;
            //}
            //ramptr0 = data_from_ice[1] | (data_from_ice[2] << 8);

            //update_reg();
            return 1;


        }


        public uint jtag_get_id(int bytenum=1)
        {
            uint theid;



            //    jtag_reloadfp();

            shift_ir_task(9, 0, 0, 1); // xilinx selected
            shift_dr_task(32, tdibytes, tdobytes, 1, 0, 1);    // finraltms=1 comes to seldr
            theid = (uint)((tdobytes[0]) | (tdobytes[1] << 8) | (tdobytes[2] << 16) | (tdobytes[3] << 24));
            if ((theid & 0x0fffffff) != 0x04001093)    // this is spartan6 LX9
            {
                errmsg += ("MS326 ICE Board not found.");
                return 0;
            }
            //shift_ir_task(IDCODE,0,0);
            shift_ir_task(9, 1, 0, 1); // shift ir, comes back to seldr
            tdibytes[0] = 1;
            tdibytes[1] = 2;
            tdibytes[2] = 3;
            tdibytes[3] = 4;
            shift_dr_task(32, tdibytes, tdobytes, 1, 0, 1);
            shift_dr_task(32, tdibytes, tdobytes, 1, 0, 1);
            shift_dr_task(32, tdibytes, tdobytes, 1, 0, 1);
            shift_ir_task(2, 1, 0, 1); // shift ir, comes back to seldr
            shift_dr_task(32, tdibytes, tdobytes, 1, 0, 1);
            shift_dr_task(32, tdibytes, tdobytes, 1, 0, 1);
            shift_dr_task(32, tdibytes, tdobytes, 1, 0, 1);
            shift_ir_task(2, 1, 0, 1); // shift ir, comes back to seldr
            jtag_tms(0, 1);
            jtag_tck(2); // after 2 clocks, it stop at shift-dr if only tms keeps low
            send_cmd_short();

            shift_ir_task(MS326ICE_IDCODE, 0, 0);



            shift_dr_task(32, tdibytes, tdobytes, 1, 0);
            theid = (uint)((tdobytes[0]) | (tdobytes[1] << 8) | (tdobytes[2] << 16) | (tdobytes[3] << 24));
            //theid >>= 1;
            fpgaid = theid;
            return theid;
        }


        public bool shift_ir_task(byte ir, int fromseldr=0, int toruntest=0, int tmssel=0)
        {
            int i;
            byte[] buf = new byte[1];
            if (tmssel == 0)
                i = 2;
            else
                i = 5;
            

            if (fromseldr != 0)
            {
                jtag_tms(1, tmssel);
                jtag_tck(1);
                jtag_tms(0, tmssel);
                jtag_tck(2);

            } else
            {
                jtag_rst(tmssel);

            }
            jtag_rst(tmssel);
            jtag_tms(0, tmssel);
            jtag_tck(1); // runtestidle after this clock
            jtag_tms(1, tmssel);
            jtag_tck(2); // sl ir scan after this 2 clock
            jtag_tms(0, tmssel);
            jtag_tck(2); // shift ir after this

            // first bits are using

            buf[0] = ir;
            jtag_write_tdi(i, buf);
            buf[0] >>= i;
            jtag_tms(1, tmssel);
            jtag_write_tdi(1, buf);     // exir state after over
            if (toruntest != 0)
            {
                jtag_tck(1); // update ir after this clock
                jtag_tms(0, tmssel);
                jtag_tck(1); // go to runtest after this clock
                send_cmd_short();
            } else
            {
                jtag_tck(2);
                //SEND_CMD;
            }
            jtag_ir_now = ir;
            return true;
        }

        //// read a byte in que
        //public bool i2c_read_byte( ref byte datard, bool sendack = true, bool send = false, byte lbdo = 0, byte lbdir = 0)
        //{
        //    // lbdo=0xff, means open-drain operation
        //    outByteLow((byte)((lbdo |6)&0xfe), (byte)((lbdir&0xf8)| 1)); // change to input

        //    byteo(MSB_PVE_IN_BYTE);
        //    byteo(0);
        //    byteo(0);
        //    ackchk[dataneedread++] = 2; // 2 means a byte

        //    byteo(MSB_NVE_OUT_BIT);
        //    byteo(0);// count
        //    if (sendack)
        //        byteo(0);// data
        //    else
        //        byteo(0xff);
        //    if (send)
        //    {
        //        if (!send_cmd())
        //            return false;
        //        if(readbytecnt==0)
        //            return false;
        //        datard = dataque[readbytecnt - 1];
        //    }
        //    return true;
        //}
        //public bool i2c_send_byte(byte datao, bool waitack = true, bool send = false, byte lbdo=0, byte lbdir=0)
        //{
        //    outByteLow((byte)(lbdo & 0xfc), (byte)(lbdir| 3));
        //    byteo(MSB_NVE_OUT_BYTE);
        //    byteo(0x0);
        //    byteo(0x0);
        //    byteo(datao);
        //    // output low clock
        //    outByteLow((byte)(lbdo & 0xfe), (byte)(lbdir | 1));

        //    if (waitack)
        //    {
        //        byteo(MSB_PVE_IN_BIT);
        //        byteo(0);
        //        ackchk[dataneedread] = 1;
        //        dataneedread++;
        //    }
        //    else
        //    {
        //        // just a clock is ok
        //        byteo(CKO_NO_TRANSFER_BITS);
        //        byteo(0);
        //        byteo(0);
        //    }
        //    // switch back to output
        //    outByteLow((byte)(lbdo & 0xfc), (byte)(lbdir | 3));
        //    if (send)
        //        return send_cmd();
        //    return true;
        //}

        //public bool i2c_init( uint freq)
        //{
        //    uint div = 60000000 / freq / 2 - 1;
        //    byteo(0x8a); // disable div5 by 60MHZ
        //    byteo(0x97); // disable adaptive clk
        //    byteo(0x8c); // enable 3phase clocking
        //    byteo(0x85); // turn off loopback
        //    if (!send_cmd())
        //        return false;
        //    // we use 250k clock, = 60MHz/(240)
        //    byteo(0x86);
        //    byteo((byte)(div & 0xff));
        //    byteo((byte)(div >> 8));
        //    if (!send_cmd())
        //        return false;
        //    byteo(0x9e); // OD
        //    byteo(0x07); // 3 pins are OD
        //    byteo(0x00); 
        //    if (!send_cmd())
        //        return false;
        //    return true;
        //}


        public void LEDOK()
        {
            outByteHigh((byte)((acb_out_val & 0xfc)|2), ACB_DEFAULT_OUT);
            send_cmd();
        }
        public void LEDFAIL()
        {
            outByteHigh((byte)((acb_out_val & 0xfc) | 1), ACB_DEFAULT_OUT);
            send_cmd();
        }
        public void LEDOFF()
        {
            outByteHigh((byte)((acb_out_val & 0xfc) | 3), ACB_DEFAULT_OUT);
            send_cmd();
        }
        public void outByteHigh(byte hbdo, byte hbdir)
        {
            byteo(0x82);
            byteo(hbdo);
            hbyteshadow = hbdo;
            acb_out_val = hbdo;
            byteo(hbdir);
        }
        public void outByteLow(byte lbdo, byte lbdir)
        {
            byteo(0x80);
            byteo(lbdo);
            lbyteshadow = lbdo;
            adb_out_val = lbdo;
            byteo(lbdir);
        }
        private void hid0()
        {
            //hiddat[0] = (byte)++reportid;
            hiddat[0] = 0;
        }
        public byte inByteHigh()
        {
            byteo(0x83);
            dataneedread = 1;
            send_cmd(true);
            return infifo[0];
        }
        public byte inByteLow()
        {
            if (useHID)
            {
                //hiddat[0] = (byte)++reportid;
                hid0();
                hiddat[1] = 0xa9;
                hiddat[2] = 1;
                myHid.Write(hiddat);
                myHid.Read(hidrb);
                return hidrb[2];
            }
            //return 0;
        //}
            byteo(0x81);
            dataneedread = 1;
            send_cmd(true);
            return infifo[0];
        }
        public bool set_freq(uint freq)
        {
            // freq=30/(set+1)
            if (freq > 30000000)
                return false;

            uint period = (30000000 / freq) - 1;
            if (period < 0 || period > 65535)
                return false;
            byteo(0x86);
            byteo((byte)(period & 0xff));
            byteo((byte)(period >> 8));
            return true;
        }
        //public bool i2c_start(byte lbdo=0, byte lbdir=0, bool flush=false,int timecount=20) // really out if flush is true
        //{
        //    byte newdir = (byte)(lbdir | 3);
        //    byte newdo = (byte)(lbdo | 3); // at first both high 
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++) // both high
        //        outByteLow(newdo, newdir);
        //    // next is SDA low
        //    newdo = (byte)((lbdo | 1) & 0xfd);
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++) // both high
        //        outByteLow(newdo, newdir);
        //    // finally both low
        //    newdo = (byte)(lbdo & 0xfc);
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++) // both high
        //        outByteLow(newdo, newdir);
        //    if (flush)
        //    {
        //        return send_cmd();
        //    }
        //    return true;
        //}
        //public bool i2c_restart(byte lbdo = 0, byte lbdir = 0, bool flush = false, int timecount = 20)
        //{
        //    byte newdir = (byte)(lbdir | 3);

        //    byte newdo = (byte)(lbdo & 0xfc);
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++) // both high
        //        outByteLow(newdo, newdir);

        //    newdo = (byte)((lbdo & 0xfc) | 2); // at first sda high, scl low
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++)
        //        outByteLow(newdo, newdir);
        //    // next scl high , too
        //    newdo |= 3;
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++)
        //        outByteLow(newdo, newdir);
        //    // then sda now
        //    newdo = (byte)((newdo & 0xfc) | 1);
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++)
        //        outByteLow(newdo, newdir);
        //    // finally, all low
        //    newdo &= 0xfc;
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++)
        //        outByteLow(newdo, newdir);

        //    if (flush)
        //        return send_cmd();
        //    return true;

        //}
        //public bool i2c_stop(byte lbdo = 0, byte lbdir = 0, bool flush = true, int timecount = 20)
        //{
        //    // both low first
        //    byte newdir = (byte)(lbdir |3);
        //    byte newdo = (byte)(lbdo &0xfc); // at first both low
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++) // both high
        //        outByteLow(newdo, newdir);
        //    // next is SDL high
        //    newdo = (byte)((lbdo | 1) & 0xfd);
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++) // both high
        //        outByteLow(newdo, newdir);
        //    // finally both high
        //    newdo = (byte)(lbdo |3);
        //    for (int dwcnt = 0; dwcnt < timecount; dwcnt++) // both high
        //        outByteLow(newdo, newdir);
        //    if (flush)
        //    {
        //        return send_cmd();
        //    }
        //    return true;
        //}


        public ftop()
        {
            ms326reg = new ms326_reg();
            bit_rev_tab = new byte[256];
            data_from_ice = new byte[256];
            data_to_ice = new byte[256];
            icecmd_seq = 0;
            for(int i=0;i<256;i++)
            {
                byte k = 0;
                for (int j = 0; j < 8; j++)
                    if ((i & (1 << j)) != 0)
                        k |= (byte)(1 << (7 - j));
                bit_rev_tab[i] = k;

            }
            jtag_ir_now = 0;
            ftdiDeviceCount = 0;
            tms_count = 0;
            adb_out_val = 0;
            cable_state = 0;
            acb_out_val = 0;
            ftStatus = FTDI.FT_STATUS.FT_OK;
            //myFtdiDevice = new FTDI();
            outfifo = new byte[FT_FIFO_SIZE];
            infifo = new byte[FT_FIFO_SIZE];
            ackchk = new byte[FT_FIFO_SIZE];
            dataque = new byte[FT_FIFO_SIZE];
            tempdata = new byte[FT_FIFO_SIZE];
            tdibytes = new byte[16];
            tdobytes = new byte[16];
            hiddat = new byte[65];
            hidrb = new byte[65];
            ofifocnt = 0;
            dataneedread = 0;
            status = 0;
            adb_out_val = 0x20;
            acb_out_val = 0xff;
            //sspiid = 1;
        }
        public bool closeDev()
        {
            if (status != 0)
            {
                if (useHID)
                    myHid.Close();
                else
                    myFtdiDevice.Close();
                status = 0;
                return true;
            }
            return false;
        }

        public  void spi_csh(bool mode=true)
        {
            // assume bit 3
            if(mode)
            {
                outByteLow((byte)(adb_out_val|0x08), ADB_DEFAULT_OUT);
                return;
            }
            if(!useHID)
                outByteLow((byte)(lbyteshadow|0x8),ICE_OE);
        }
        public void spi_csl(bool mode=true)
        {
            if(mode)
            {
                outByteLow((byte)(adb_out_val & 0xf7), ADB_DEFAULT_OUT);
                return;
            }
            if(!useHID)
                outByteLow((byte)(lbyteshadow&0xf7),ICE_OE);
        }



        public bool spi_send_byte_raw(uint num, ref byte[] rawdata)
        {
            uint i;
            uint j = num - 1;
            if (num <= 0)
                return true;
            byteo(MSB_NVE_OUT_BYTE);
            byteo((byte)(j & 0xff));
            byteo((byte)(j >> 8));
            for (i = 0; i < num; i++)
                byteo(rawdata[i]);
            return true;

        }
        public bool spi_read_byte_raw(int num, ref byte[] rawdata, int moren = 0, byte[] cmdmore = null)
        {
            int i, j;
            int num1;
            int addr = 0;
            if (num <= 0) return true;

            while (num > 0)
            {
                num1 = num;
                if (num > 65536)
                    num1 = 65536;
                j = num1 - 1;
                byteo(MSB_PVE_IN_BYTE);
                byteo((byte)(j & 0xff));
                byteo((byte)(j >> 8));
                for (int k = 0; k < moren; k++)
                    byteo(cmdmore[k]);

                dataneedread = (uint)num1;
                if (!send_cmd(true))
                    return false;
                for (i = 0; i < num1; i++)
                    rawdata[addr++] = infifo[i];
                num -= num1;
            }
            return true;
        }



        public bool spi_rw_byte_raw(int num, ref byte[] rawdo, ref byte[] rawdi, int moren = 0, byte[] morend = null)
        {
            int num1;
            int i, j;
            int addr = 0;
            if (num <= 0) return true;
            while (num > 0)
            {
                num1 = num;
                if (num > 65536)
                    num1 = 65536;
                j = num1 - 1;
                byteo(MSB_PNVE_IO_BYTE);
                byteo((byte)(j & 0xff));
                byteo((byte)(j >> 8));
                for (i = 0; i < num1; i++)
                {
                    byteo(rawdo[addr + i]);
                }
                //byteo(0x87); //87 will be send in send_cmd
                if (num1 == num)
                {
                    for (i = 0; i < moren; i++)
                        byteo(morend[i]);
                }
                dataneedread = (uint)num1;
                if (!send_cmd(true))
                    return false;


                for (i = 0; i < num1; i++)
                    rawdi[addr + i] = infifo[i];
                num -= num1;
                addr += num1;

            }
            return true;
        }
        public uint get_spi_size(byte id)
        {
            if (id == 0 || id == 0xff)
                return 0;
            int idh = id >> 4;
            if (idh != 1 && idh != 2 && idh != 4)
            {
                return 0;
            }
            id &= 0xf;
            // 0: 512k bit, 1:1M Bit
            // 2: 2M
            // 3: 4M, 2^(n-1) Mbit, max is 256 M
            if (id < 9)
                return (uint)(1 << (id + 16));
            if (id == 0xf)
                return 32768;

            return 0;
        }

        public bool spi_erase_sector(uint sector_addr)
        {
            byte[] cmd = { 0x20, 0, 0, 0 };
            cmd[1] = (byte)(sector_addr >> 16);
            cmd[2] = (byte)((sector_addr >> 8) & 0xff);
            cmd[3] = (byte)(sector_addr & 0xff);
            if (!spi_wait_busy(500)) return false;
            spi_write_single(0x6);
            spi_csl();
            spi_send_byte_raw(4, ref cmd);
            spi_csh();
            return spi_wait_busy(500);

        }

        public bool spi_write_page(uint addr, byte[] data2write, uint len)
        {
            if (!spi_wait_busy(500))
                return false;
            spi_write_single(0x6);

            byte[] cmd = { 0x2, 0, 0, 0 };
            cmd[1] = (byte)((addr >> 16) & 0xff);
            cmd[2] = (byte)((addr >> 8) & 0xff);
            cmd[3] = (byte)((addr & 0xff));

            spi_csl();
            spi_send_byte_raw(4, ref cmd);
            spi_send_byte_raw(len, ref data2write);
            spi_csh();
            return spi_wait_busy(100);

        }

        public bool spi_read_block(int addr, ref byte[] datard, uint len)
        {
            if (len == 0) return false;
            byte[] cmd = { 0x3, 0, 0, 0 };
            cmd[1] = (byte)((addr >> 16) & 0xff);
            cmd[2] = (byte)((addr >> 8) & 0xff);
            cmd[3] = (byte)((addr & 0xff));
            spi_csl();
            spi_send_byte_raw(4, ref cmd);
            spi_read_byte_raw((int)len, ref datard);
            spi_csh();
            return true;
        }

        public bool spi_wait_busy(int timeout)
        {
            Stopwatch sw = new Stopwatch();
            while (sw.ElapsedMilliseconds < timeout)
            {
                if ((spi_get_status() & 1) == 0)
                    return true;
            }
            return false;
        }

        public bool spi_write_status(byte newstat)
        {
            byte[] cmd = { 0x1, newstat };
            spi_write_single(0x6);
            spi_csl();
            spi_send_byte_raw(2, ref cmd);
            spi_csh();
            return spi_wait_busy(500); // 500 ms is enough
        }


        public byte spi_get_status()
        {
            byte[] cmd = { 0x5 };
            byte[] status = { 0x00 };
            spi_csl();
            spi_send_byte_raw(1, ref cmd);
            spi_read_byte_raw(1, ref status);
            spi_csh();
            return status[0];
        }
        public void spi_write_single(byte cmd)
        {
            byte[] cmds = { cmd };
            spi_csl();
            spi_send_byte_raw(1, ref cmds);
            spi_csh();
        }
        public bool burn_spi_image(byte[] image, uint size, WRISPICB cbp, uint cbpagen)
        {
            uint address = 0;
            uint lastaddr;
            uint i;
            byte[] buffer = new byte[256];

            while (address < size)
            {
                if ((address & 0xfff) == 0)
                {
                    if (!spi_erase_sector(address))
                        return false;
                }
                for (i = 0; i < 256; i++)
                    buffer[i] = 0xff;
                lastaddr = address;
                for (i = 0; i < 256 && address < size; i++)
                    buffer[i] = image[address++];
                if (!spi_write_page(lastaddr, buffer, i))
                    return false;
                if (cbp!=null && address >= cbpagen)
                {
                    cbpagen = cbp(address);
                }
            }
            return true;
        }

        public uint spi_get_idab()
        {
            byte[] cmd = { 0xab, 0x0, 0x0, 0x1 };
            byte[] reply;
            reply = new byte[3];
            spi_csh();
            spi_csl();
            spi_send_byte_raw(4, ref cmd);
            spi_read_byte_raw(3, ref reply);
            spi_csh();
            send_cmd(true); // back to high
            uint id = (uint)((reply[0] << 16) | (reply[1] << 8) | (reply[2]));
            return id;
        }

        public uint spi_get_id()
        {
            byte[] cmd = { 0x9f };
            byte[] reply;
            reply = new byte[3];
            spi_write_single(0xab);
            spi_csh();
            spi_csl();
            
            spi_send_byte_raw(1, ref cmd);
            spi_read_byte_raw(3, ref reply);
            spi_csh();
            send_cmd(true); // back to high
            uint id = (uint)((reply[0] << 16) | (reply[1] << 8) | (reply[2]));
            return id;

        }
        //public uint  MS326TM(int testmode)
        //{
        //    // MS326 testmode 0 or testmode1
        //    // generally, we enters testmode 0
        //    // that SPI can be read
        //    // enter testmode: PB2 clock, PB3 data
        //    // PB0 is tmrstb --> ADBUS4
        //    // after enters testmode0, 
        //    // PIOB4/PIOC3 is SPIMI --> ADBUS2
        //    // CS is PIOB3/PIOC0 -->ADBUS3
        //    // CK is PIOB2/PIOC1 -->ADBUS0
        //    // MO is PIOB1/PIOC2 -->ADBUS1
        //    // this function returns the ID it founds
        //    // ok, we have no power control here,
        //    // but 
        //    // reset testmode should be enough

        //    if (testmode != 0)
        //        return 0;

        //    for(int i=0;i<65536;i++)
        //        ackchk[i]=0;


        //    outByteLow(0x0, ICE_OE); // all OFF
        //    // note that ADB6 is V36VO, which is also OFF
        //    // we shall connect VDD to V36VO
        //    set_freq(5000000); // use 1MHZ;

        //    send_cmd(true);
        //    // Delay a while
        //    Thread.Sleep(100);
        //    outByteLow(0x40, ICE_OE);
        //    send_cmd();



        //    outByteLow(0xfa, ICE_OE);// clk 0, default 0
        //    outByteLow(0x7a, ICE_OE);// clk 0, default 0
        //    outByteLow(0xfa, ICE_OE);// clk 0, default 0
        //    //byteo(0x4b);
        //    //byteo(0x1);
        //    //byteo(0xc0); // give first double 1
        //    // enter testmode, we need PIOB2(ADBUS0) PIOB3(ADBUS3)
        //    for (int i=0; i < 3; i++)
        //    {
        //        byteo(0x4b);
        //        byteo(0x7);
        //        byteo(0xAA);
        //    }
        //    outByteLow(0xfa, ICE_OE);// clk 0, default 0
        //    Thread.Sleep(5);
        //    send_cmd();
        //    Thread.Sleep(5);
        //    // now try get ID
        //    uint id =spi_get_id();




        //    return id;
        //}
        //public bool MS326ICE_init()
        //{
        //    FTDI.FT_STATUS stat;
        //    stat=myFtdiDevice.ResetPort();
        //    if (stat != FTDI.FT_STATUS.FT_OK)
        //        return false;
        //    stat=myFtdiDevice.SetLatency(5);
        //    if (stat != FTDI.FT_STATUS.FT_OK)
        //        return false;
        //    stat = myFtdiDevice.SetBitMode(0, 2);
        //    if (stat != FTDI.FT_STATUS.FT_OK)
        //        return false;
        //    Thread.Sleep(50);
        //    stat = myFtdiDevice.SetTimeouts(1000, 1000);
        //    if (stat != FTDI.FT_STATUS.FT_OK)
        //        return false;

        //    return false;
        //}

        //public bool openDev(ref string msg, CHKDEV devchk)
        // to write spi flash, we simplifies it.

        public bool powerOff(ref string msg)
        {
            if(status==0)
            {
                msg = "need connect first.";
                return false;
            }
            spi_csh();
            if (!send_cmd())
            {
                myFtdiDevice.Rescan();
                Thread.Sleep(1000);
                closeDev();
                Thread.Sleep(1000);
                if (!openDev(ref msg))
                {
                    status = 0;
                    msg = "Device disconnected.\n";
                    return false;      // if connect again no return false
                }
            }
            byteo(0x80);
            byteo(0); // out data 0;
            byteo(0xff);
            byteo(0x82);
            byteo(2); // off, LED fail off
            byteo(0xdb); // special dir, skip adbus5
            if(!send_cmd_short())
            {
                status = 0;
                closeDev();
                msg = "send command fail.";
                return false;
            }
            return true;
        }
        public void setDivisor(int div)
        {
            byteo(0x86);
            byteo((byte)(div & 0xff));
            byteo((byte)((div >> 8) & 0xff));
        }
        public bool powerOffOn(ref string msg, int offtime=200)
        {
            if (status == 0)
            {
                msg = "need connect first.";
                return false;
            }
            setDivisor(3);
            spi_csh();
            if (!send_cmd())
            {
                myFtdiDevice.Rescan();
                Thread.Sleep(1000);
                closeDev();
                Thread.Sleep(1000);
                if (!openDev(ref msg))
                {
                    status = 0;
                    msg = "Device disconnected.\n";
                    return false;      // if connect again no return false
                }
            }
            byteo(0x80);
            byteo(0); // out data 0;
            byteo(0xff);
            byteo(0x82);
            byteo(2); // off
            byteo(0xdf); // special dir, skip adbus5
            send_cmd_short();


            Thread.Sleep(offtime);
            byteo(0x82); // 82 is acb, 80 is adb
            byteo(0x03); // on
            byteo(ACB_DEFAULT_OUT);

            byteo(0x80);
            byteo(0x48); // csb high; power on
            byteo(ADB_DEFAULT_OUT);

            send_cmd_short();
            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                msg = "Power off on fail";
                closeDev();
                status = 0;
                return false;
            }
            return true;

        }
        public bool ms311IO0LowPulse(ref string msg, int width=1, bool tmio=false)
        {
            if(status==0)
            {
                msg = "please connect first.\r\n";
                return false;
            }
            outByteHigh((byte)(acb_out_val & 0xfe), ACB_DEFAULT_OUT);
            if(!send_cmd())
            {
                msg = "send cmd fail.\r\n";
                return false;
            }
            Thread.Sleep(width);
            outByteHigh((byte)(acb_out_val | 0x01), ACB_DEFAULT_OUT);
            if(tmio) // change to testmode IO
            {
                outByteLow(0x48, ADB_TM_OUT);
            }
            if (!send_cmd())
            {
                msg = "send cmd fail.\r\n";
                return false;
            }

            return true;
        }

        //public bool ms311CSLOWPulse(ref string msg,int width=500)
        //{
        //    outByteLow((byte)(adb_out_val & 0xf7), ADB_DEFAULT_OUT);
        //    if (!send_cmd())
        //    {
        //        msg = "send cmd fail.\r\n";
        //        return false;
        //    }
        //    Thread.Sleep(width);
        //    outByteLow((byte)(adb_out_val |0x08), ADB_DEFAULT_OUT);
        //    if (!send_cmd())
        //    {
        //        msg = "send cmd fail.\r\n";
        //        return false;
        //    }
        //    return true;

        //}
        
        public bool ms311tm0(ref string msg)
        {
            // special ms205 testmode
            // IO0  -- ACB0
            // IO1  -- ADB0
            // IO2  -- ADB2
            // IO3  -- ADB1
            // IO4 ..  ADB4 .. a clock is required
            // IO5 .. erc out, don't care
            // IO6  -- ADB3, new CS, used after mode entered.
            int i;
            if (status != 1)
            {
                msg = "no device found.";
                return false;
            }

            
            outByteLow(0x7f, ADB_TM_OUT); 
            outByteHigh(2, ACB_DEFAULT_OUT);
            send_cmd();


            Thread.Sleep(10);
            //        OUSB;
            outByteHigh(3, ACB_DEFAULT_OUT);
            for (i = 0; i < 32; i++)
            {

                outByteLow(0xfb, ADB_TM_OUT); // IO2 LOW first
                outByteLow(0xfa, ADB_TM_OUT); // IO1 LOW
                outByteLow(0xfb, ADB_TM_OUT); // IO1 high
                outByteLow(0xff, ADB_TM_OUT); // IO2 HIGH
                outByteLow(0xfe, ADB_TM_OUT);
                outByteLow(0XFF, ADB_TM_OUT);

            }
            outByteLow(0x48, ADB_DEFAULT_OUT);
            outByteLow(0x40, ADB_DEFAULT_OUT);
            outByteLow(0x40, ADB_DEFAULT_OUT);
            outByteLow(0x40, ADB_DEFAULT_OUT);
            outByteLow(0x50, ADB_DEFAULT_OUT);
            outByteLow(0x50, ADB_DEFAULT_OUT);
            outByteLow(0x40, ADB_DEFAULT_OUT);
            outByteLow(0x40, ADB_DEFAULT_OUT);
            outByteLow(0x50, ADB_DEFAULT_OUT);
            outByteLow(0x50, ADB_DEFAULT_OUT);
            outByteLow(0x40, ADB_DEFAULT_OUT);
            outByteLow(0x40, ADB_DEFAULT_OUT);
            outByteLow(0x48, ADB_DEFAULT_OUT);

            return send_cmd(); 
            //low_mask = 0x40;
            //high_mask = 0x01;
        }
        public bool openDev(ref string msg)
        {
            
            // Create new instance of the FTDI device class
            if (status != 0)
            {
                msg = "already opened.";
                return false;
            }
            useHID = false;
            myFtdiDevice = new FTDI();

            ftStatus = myFtdiDevice.GetNumberOfDevices(ref ftdiDeviceCount);


            //if (ftdiDeviceCount == 0 || ftStatus != FTDI.FT_STATUS.FT_OK)
            //{
            //    useHID = true;
                
            //    List<HIDInfo> hidList = HIDBrowse.Browse();
                

            //    foreach (HIDInfo h in hidList)  {
            //        if(h.Vid == 0x04d9 && h.Pid==0x2009 )
            //        {
            //            myHid = new HIDDev();
            //            if(myHid.Open(h))
            //            {
            //                if (!devchk(this))
            //                {
            //                    myHid.Close();
            //                    useHID = false;
            //                    return false;
            //                }
            //                return true;
            //            }
            //        }
            //    }


            //    useHID = false;
            //    return false;
            //}

            FTDI.FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FTDI.FT_DEVICE_INFO_NODE[ftdiDeviceCount];
            ftStatus = myFtdiDevice.GetDeviceList(ftdiDeviceList);


            if(ftStatus!=FTDI.FT_STATUS.FT_OK)
            {
                msg += "err.\n";
            }

            if(ftdiDeviceCount!=0 )
            {
                // try again
                int i = 0;
                while (ftdiDeviceList[0].ID == 0 && ++i < 10) // strange bug in VM??)
                {
                    Thread.Sleep(10);
                    ftStatus = myFtdiDevice.GetDeviceList(ftdiDeviceList);
                }
                if(i>=10)
                {
                    msg += "GetDeviceListFail!!\n";
                    return false;
                }
            }


            for (UInt32 i = 0; i < ftdiDeviceCount; i++)
            {
                if (ftdiDeviceList[i].ID == (uint)0x04036014) //FT232H!!
                {
                    ftStatus = myFtdiDevice.OpenByDescription(ftdiDeviceList[i].Description);
                    if (ftStatus != FTDI.FT_STATUS.FT_OK)
                    {
                        msg += "dev " + ftdiDeviceList[i].SerialNumber + " open fail\n";
                        errmsg = msg;
                        continue;
                    }
                    // before final check , we reset
                    ftStatus = myFtdiDevice.ResetDevice();
                    //ftStatus |= myFtdiDevice.InTransferSize(65536);

                    ftStatus |= myFtdiDevice.SetBitMode(0, 0); // reset
                    ftStatus |= myFtdiDevice.SetTimeouts(1000, 1000);
                    ftStatus |= myFtdiDevice.SetBitMode(0, 0x2); // MPSSE


                    if (ftStatus != FTDI.FT_STATUS.FT_OK)
                    {
                        msg += "dev " + ftdiDeviceList[i].SerialNumber + " setbitmode fail\n";
                        errmsg = msg;
                        continue;
                    }
                    //if (!devchk(this))
                    //{
                    //    myFtdiDevice.Close();
                    //    continue;
                    //}
                    status = 1;
                    lbyteshadow = 0xff;
                    hbyteshadow = 0xff; // reset the shadow bytes
                    return true;  // we find it
                }
            }
            // if FTDI not found we try HID
            msg = "No writer found.";
            return false;
        }
        public int ms326_load_ice_code(byte[] icecode,int fpid=-1)
        {
            //int fileid;
            //int len;
            int prog_addr;
            int code_addr;
            int error_count = 0;
            //byte[] icecode;//= new byte[4096 + 128];
             // only 31 bits bscan
            byte otp_data, chk_data;
            byte [] backup = new byte[10];
            //icecode = File.ReadAllBytes(codefile);
            shift_ir_task(MS326ICE_EXTEST, 0);
            int drLen = EXTEST_DRLEN;
            if (fpid > 0 && (fpid >> 28) < 6)
                drLen--;
            for (prog_addr = 0; prog_addr <= icecode.Length-1; prog_addr++)
            {
                code_addr = prog_addr & 0xfff;
                otp_data = icecode[code_addr];
                data_to_ice[0] = (byte)(prog_addr & 0xff);    // 11 bit addr
                data_to_ice[1] = (byte)(((prog_addr >> 8) & 0x7) | (otp_data << 3)); // prog addr as 11 bits
                data_to_ice[2] = (byte)(0x08 | (otp_data >> 5));
                data_to_ice[3] = 0;

                //shift_dr_task(EXTEST_DRLEN,data_to_ice,data_from_ice,1); // end in seldr through run
                shift_dr_task(drLen, data_to_ice, data_from_ice, 1, 1); // end in seldr through run

                //if (prog_addr == len)
                //{
                //    memcpy(backup, data_to_ice, 3);
                //}
                backup[0] = data_to_ice[0];
                backup[1] = data_to_ice[1];
                backup[2] = data_to_ice[2];
                //shift_dr_task(31,data_to_ice,data_from_ice,1); // twice
            }
            //memcpy(data_to_ice,backup,3);
            for (int ii = 0; ii < data_to_ice.Length; ii++)
                data_to_ice[ii] = 0;
            shift_dr_task(EXTEST_DRLEN, data_to_ice, data_from_ice, 1);
            for (prog_addr = 0x0; prog_addr <= icecode.Length-1; prog_addr++)
            {
                code_addr = prog_addr & 0x7ff;
                otp_data = icecode[code_addr];
                data_to_ice[0] = (byte)((prog_addr + 1) & 0xff);
                data_to_ice[1] = (byte)((((prog_addr + 1) >> 8) & 0xff)); // prog addr as 11 bits
                data_to_ice[2] = 0;
                data_to_ice[3] = 0x0;
                shift_dr_task(drLen, data_to_ice, data_from_ice, 1); // end in seldr
                                                                            //shift_dr_task(31,data_to_ice,data_from_ice,1); // end in seldr 2 times
                                                                            // readback data is bits 27..20
                chk_data = (byte)((data_from_ice[2] >> 4) | (data_from_ice[3] << 4));
                if (otp_data != chk_data)
                {

                    //sprintf(szPrint, "address %04XH correct %02XH get %02XH",
                    //prog_addr, otp_data, chk_data);
                    //out2log(szPrint);
                    errmsg += "address " + prog_addr.ToString("X") + " correct " + otp_data.ToString("X2") + " get " + chk_data.ToString("X2")+"\n";
                    error_count++;
                    if (error_count >= 5)
                        break;
                }

            }
            return error_count;
        }
        public bool TO_SPI_MODE() { return shift_ir_task(MS326ICE_JTGSPI, 1, 1); }
        public bool TO_DEBUG_MODE() { return shift_ir_task(MS326ICE_DBG_CODE, 0); }
        byte[] data_from_ice;
        byte[] data_to_ice;
        //byte[] data_fro_ice;
        int icecmd_seq;
        public void cleardfi()
        {
            for (int ii = 0; ii < data_from_ice.Length; ii++)
                data_from_ice[ii] = 0;
        }

        public int ms326_do_ice_command(int command, int para=0, int timeout=100)
        {
            int i = 0;
            if (jtag_ir_now != MS326ICE_DBG_CODE)
            {
                TO_DEBUG_MODE();
            }
            //memset(data_from_ice, 0, sizeof(data_from_ice));
            cleardfi();
            data_to_ice[0] = (byte)( (command << 3) | ((icecmd_seq & 3) << 1) | 1);
            data_to_ice[1] = (byte)(para & 0xff);
            data_to_ice[2] = (byte)((para >> 8) & 0xff);
            data_to_ice[3] = (byte)((para >> 16) & 0xff); // it will be 4 bytes
            data_to_ice[4] = (byte)((para >> 24) & 0xff);

            do
            {
                shift_dr_task(ICECMD_LEN, data_to_ice, data_from_ice);
                if (i > 100)
                    Thread.Sleep(2);
            } while (data_from_ice[0] != data_to_ice[0] && ++i < timeout);
            icecmd_seq++;
            if (i < timeout)
                return (i | 3);

            return 0;

        }

        public int hycon_ice_getstat()
        {
            // read the 7 bits of ice status
            if(useHID)
            {
                // use command 0xa3 to input 2 bytes
                //                byte[] hiddat = new byte[65];
                //                byte[] hidrb = new byte[65];
                //hiddat[0] = (byte)(++reportid&0xff);
                hid0();
                hiddat[1] = 0xA7; // read status

                myHid.Write(hiddat);
                myHid.Read(hidrb);

                if (hidrb[1] != 0xfa)
                    return -1;
                
                return ((int)hidrb[2] << 14);

            }
            return 0;

            //for(int i =0; i < 20;i++)
            //{
            //    outByteLow(0xf8, ICE_OE);
            //}
            //spi_csl();
            //// w+r 2 bytes are enough
            //byteo(MSB_PNVE_IO_BYTE); //
            //byteo(1);// inout 2 bytes
            //byteo(0);
            //byteo(0x80); // bit 23 high is write
            //byteo(0x00);  // 0 should be ok
           
            //dataneedread += 2;
            //if (!send_cmd(true))
            //    return -1;
            //spi_csh();
            //return ((infifo[0] * 256 + infifo[1])<<9)&0x3f8000;

        }

        // following is HYCON ICE operations
        // we compose to single IO commands

        
        public int hycon_ice_command(int cmd, int datan, ref byte [] dataio)
        {
            // cmd: the 24 bit control 
            // dataio: the data send/receive from ICE, final bit 0 is write, 1 is read

            // write down to physical level
            if (datan > 65500) // no exceed 64k!!
                return -1;


            if(useHID)
            {
                int i;
                if((cmd&0xC00000)==0) // stat write
                {
                    //hiddat[0] =(byte) ++reportid;
                    hid0();
                    hiddat[1] = 0xa6; // A6 is write status
                    hiddat[2] = (byte)(cmd >> 14);
                    myHid.Write(hiddat);
                    myHid.Read(hidrb);
                    if (hidrb[1] != 0xfa)
                        return -1;
                    return 1;
                }
                // we seperate datan to multiple xx
                // check if single packet first logest packet is 64 - cmdx1, lenx3, statw*3 = 57
                if(datan<=57)
                {
                    //hiddat[0] = (byte)++reportid;
                    hid0();
                    hiddat[1] = 0xa4; // small single packet
                    hiddat[2] = (byte)((cmd >> 16)&0xff);
                    hiddat[3] = (byte)((cmd >> 8) & 0xff);
                    hiddat[4] = (byte)((cmd & 0xff));
                    hiddat[5] = (byte)((datan >> 16) & 0xff);
                    hiddat[6] = (byte)((datan >> 8) & 0xff);
                    hiddat[7] = (byte)((datan & 0xff));
                    for (i = 8; i < 65; i++)
                        hiddat[i] = 0;
                    if ((cmd & 1) == 0)
                    {
                        for (i = 0; i < datan; i++)
                            hiddat[i + 8] = dataio[i];
                    }
                    myHid.Write(hiddat);
                    myHid.Read(hidrb);
                    if ((cmd & 1) == 1)
                    {
                        for (i = 0; i < datan; i++)
                            dataio[i] = hidrb[i + 1];
                    }


                        return 1;


                }
                // seperate to packets 

                //hiddat[0] = (byte)++reportid;
                hid0();
                hiddat[1] = 0xa2; // a2 is the setup
                hiddat[2] = (byte)((cmd >> 16) & 0xff);
                hiddat[3] = (byte)((cmd >> 8) & 0xff);
                hiddat[4] = (byte)((cmd & 0xff));
                hiddat[5] = (byte)((datan >> 16) & 0xff);
                hiddat[6] = (byte)((datan >> 8) & 0xff);
                hiddat[7] = (byte)((datan & 0xff));
                hiddat[8] = 0x0; // timeout, set 0xff
                i = 8;
                int jj=0;
                if((cmd&1)==0) // write
                {
                    myHid.Write(hiddat); // there should be a 0xaa response
                    myHid.Read(hidrb);
                    while(datan>0)
                    {
                        int k = datan;
                        if (k > 64) k = 64; // later per packet is 63 byte with first cmd A3
                        //hiddat[0] = (byte)++reportid;
                        hid0();
                        //hiddat[1] = 0xa3;
                        for (i = 0; i < k; i++)
                            hiddat[1 + i] = dataio[jj++];
                        myHid.Write(hiddat);
                        myHid.Read(hidrb);
                        datan -= k;
                    }
                    return 1;
                }
                // otherwise read
                myHid.Write(hiddat);
                myHid.Read(hidrb);
                jj = 0;
                do
                {
                    if (datan > 0)
                    {
                        //hiddat[0] = (byte)++reportid;
                        hid0();
                        hiddat[1] = 0xa3;
                        myHid.Write(hiddat);
                        myHid.Read(hidrb);
                    }
                    int k = datan;
                    if (k > 64)
                        k = 64;
                    for (i = 0; i < k; i++)
                        dataio[jj++] = hidrb[i + 1];
                    datan -= k;
                    
                } while (datan > 0);

                return 1;
            }
           // //ofifocnt = 0;
           //// myFtdiDevice.Purge(FTDI.FT_PURGE.FT_PURGE_RX|FTDI.FT_PURGE.FT_PURGE_TX );// purge RX is ok


           // // before CSL, we set state for some period
           // for( int i=0;i<20;i++)
           //     outByteLow(0xf8, ICE_OE); 
           // spi_csl();
           // byte [] cmd3;
           // //byte[] statusrd;
           // cmd3 = new byte[3+datan];

           // if ((cmd & 0x400000) == 0)
           // {
           //     //cmd = cmd ^ HYICE_ProRST; // this bit is reversed
           //     //cmd |= 1; // prevent write?

           //     // 2 bytes is enough
           //     byteo(MSB_NVE_OUT_BYTE);
           //     byteo(1);
           //     byteo(0);
           //     byteo((byte)(cmd >> 16));
           //     byteo((byte)(cmd >> 8));
           //     spi_csh();
           //     send_cmd(true);// outonly
           //     return 1;
           // }
           // //statusrd = new byte[3+datan];
           // cmd3[0] = (byte)((cmd >> 16) & 0xff);
           // cmd3[1] = (byte)((cmd >> 8) & 0xff);
           // cmd3[2] = (byte)(cmd & 0xff);
           // if ((cmd & 1) == 0) // means write
           // {
           //     for (int i = 0; i < datan; i++)
           //         cmd3[3 + i] = dataio[i];
           // }
           // else
           // {
           //     for (int i = 0; i < datan; i++)
           //         cmd3[3 + i] = 0x0; // use 1 instead
           // }
           // byteo(MSB_PNVE_IO_BYTE); //
           // int j = datan + 3-1;
           // byteo((byte)(j & 0xff));
           // byteo((byte)(j >> 8));
           // for (int i = 0; i < datan + 3; i++)
           //     byteo(cmd3[i]);
           // // read 2 more bits
           // //if (datan > 0)
           // //{
           // byteo(MSB_PNVE_IO_BIT); // read more
           // byteo(1);
           // byteo(0);
           // spi_csh();
           // dataneedread = (uint)(3 + datan+1); // final bits are in 1 byte
           // if (!send_cmd(true))
           //     return -1;
           

           
           
           // if ((infifo[datan + 3] & 0x2) != 0)
           //     infifo[datan + 3] = 0x80;
           // else
           //     infifo[datan + 3] = 0;
           // int backdata = (infifo[0] << 16) | (infifo[1] << 8) | (infifo[2]);
           // if ((cmd & 1) != 0) // this is read
           // {
           //     int i;
           //     for (i = datan - 1; i >= 0; i--)
           //     {
           //         if ((infifo[i + 4] & 0x80) != 0)
           //         {
           //             dataio[i] = (byte)(((infifo[i + 3] << 1) | 1) & 0xff);
           //         } else
           //         {
           //             dataio[i] = (byte)(((infifo[i + 3] << 1) ) & 0xff);
           //         }
           //     }
           // }
           // //byteo(MSB_PNVE_IO_BIT); // read more
           // //byteo(1);
           // //byteo(0);
           // /*
           // dataneedread = 1;
           // send_cmd(true);
           // if ((infifo[0] & 0x2) != 0)
           //     dataio[datan - 1] |= 1;

           //    */
           // //}
           // /*
           // spi_csh();
           // if (!send_cmd(true))
           //     return -1;
           //     */
            return 1;
        }

        public bool hycon_ice_rst()
        {
            //icestat_now = hycon_ice_getstat();
            icestat_now = 0;
            if (hycon_ice_command( (icestat_now| HYICE_CTR_ICECWR | HYICE_ProMD0| HYICE_ProMD1)&(~ HYICE_ProRST), 0, ref tempdata ) == -1)
                return false;
            icestat_now = hycon_ice_getstat();

            if (hycon_ice_command(icestat_now|HYICE_CTR_ICECWR | HYICE_ProMD0 | HYICE_ProMD1|HYICE_ProRST , 0, ref tempdata) == -1)
                return false;
            //icestat_now = hycon_ice_getstat();

            byte[] h08a = { 0 };
            hycon_ice_write_ram(H08Sel, 1, ref h08a);

            return true;
        }
        
        public bool hycon_ice_read_ram(int addr, int n, ref byte[] readdata)
        {
            icestat_now = hycon_ice_getstat();
            if (hycon_ice_command((icestat_now| HYICE_CTR_ICECWR | HYICE_ProCSRAM)&(~HYICE_ProCSUP), 0, ref tempdata) == -1)
                return false;
            if (hycon_ice_command (HYICE_CTR_ADDRDATA |  ((addr & 0x1FFfFF) << 1) | 1, n, ref readdata) == -1)
                return false;
            return true;
        }
        public bool hycon_ice_write_ram(int addr, int n, ref byte[] writedata)
        {
            icestat_now = hycon_ice_getstat();
            if (hycon_ice_command((icestat_now|HYICE_CTR_ICECWR | HYICE_ProCSRAM)&(~HYICE_ProCSUP), 0, ref tempdata) == -1)
                return false;
            if (hycon_ice_command(HYICE_CTR_ADDRDATA |  ((addr & 0x1fffff) << 1), n, ref writedata) == -1)
                return false;
            return true;
        }
        public bool hycon_ice_read_rom(int addr, int n, ref byte[] readdata)
        {
            icestat_now = hycon_ice_getstat();
            if (hycon_ice_command((icestat_now|HYICE_CTR_ICECWR |  HYICE_ProCSUP)&(~HYICE_ProCSRAM), 0, ref tempdata) == -1)
                return false;
            if (hycon_ice_command(HYICE_CTR_ADDRDATA |  ((addr & 0x1fFFfF) << 1) | 1, n, ref readdata) == -1)
                return false;
            return true;
        }
        public bool hycon_ice_write_rom(int addr, int n, ref byte[] writedata)
        {
            icestat_now = hycon_ice_getstat();
            if (hycon_ice_command((icestat_now | HYICE_CTR_ICECWR | HYICE_ProCSUP) & (~HYICE_ProCSRAM), 0, ref tempdata) == -1)
                return false;
            if (hycon_ice_command(HYICE_CTR_ADDRDATA |  ((addr & 0x1fffff) << 1), n, ref writedata) == -1)
                return false;
            return true;
        }
        public void cpu_step(int n, bool sei=false)
        {
            //int iord;
            byte[] cyclebyte = new byte[1];
            byte[] inte1 = new byte[1];
            byte[] inte1a = new byte[1];
            hycon_ice_read_ram(CYCLE_LL, 1, ref cyclebyte);
            cyclebyte[0] += (byte)(n);
            hycon_ice_write_ram(MTCYCLE, 1, ref cyclebyte);

            if(sei)
            {
                hycon_ice_read_ram(0x23, 1, ref inte1);
                inte1a[0] = (byte) (inte1[0]&0x7f);
                hycon_ice_write_ram(0x23, 1, ref inte1a);
                
            }
            //hycon_ice_command(HYICE_CTR_ICECWR | HYICE_ProMD0 | HYICE_ProMD1 | HYICE_ProCSUP, 0, ref tempdata);
            icestat_now = hycon_ice_getstat();
            hycon_ice_command((icestat_now|HYICE_CTR_ICECWR | HYICE_ProMD0 | HYICE_ProMD1 | HYICE_ProCSUP|  HYICE_ProICEGO)&(~HYICE_ProCSRAM), 0, ref tempdata);

            for (int i = 0; i < 100; i++)
            {
               if ((inByteLow() & 4) == 0)
                    break;
                Thread.Sleep(1);
            }
            if(sei)
                hycon_ice_write_ram(0x23, 1, ref inte1);
            icestat_now = hycon_ice_getstat();
            hycon_ice_command((icestat_now)&(~HYICE_ProICEGO), 0, ref tempdata);
        }
        public void cpu_go()
        {
            icestat_now = hycon_ice_getstat();
            hycon_ice_command((icestat_now | HYICE_CTR_ICECWR | HYICE_ProMD1 | HYICE_ProCSUP | HYICE_ProICEGO | HYICE_ProSTPOV) & (~HYICE_ProCSRAM) & (~HYICE_ProMD0), 0, ref tempdata);
        }
        public void cpu_halt()
        {
            icestat_now = hycon_ice_getstat();
            hycon_ice_command((icestat_now | HYICE_CTR_ICECWR) & (~HYICE_ProICEGO), 0, ref tempdata);

        }

       
        public bool ms326_ice_go(int timeout)
        {
            int para;
            int count = 0;
            //viewmode = 0; // no more special A
            if (0==ms326_do_ice_command(ICECMD_WRITE_RAMP0, ms326reg.ms326_ramptr0))
            {
                errmsg=("write ramp0 failed");
                return false;
            }
            /*
            if(!do_ice_command(ICECMD_WRITE_ROMP,romptr))
            {
                    out2log("write romptr failed");
                    return 0;
            } */
            para = (ms326reg.ms326_acc) | (ms326reg.ms326_pc << 8) | (ms326reg.ms326_carry << 24) | (ms326reg.ms326_ov << 25);
            if (0==ms326_do_ice_command(ICECMD_WRITE_REG, para))
            {
                errmsg=("write pc acc failed");
                return false;
            }
            // here is low level

            cleardfi();
            data_to_ice[0] = data_to_ice[1] = data_to_ice[2] = data_to_ice[3] = 0;//clear
            shift_dr_task(ICECMD_LEN, data_to_ice, data_from_ice);
            // we need send command here
            send_cmd();

            if (timeout==0)
                return true;
            while (count < timeout)
            {
                shift_dr_task(ICECMD_LEN, data_to_ice, data_from_ice);
                if (data_from_ice[0] == 1) // BP entered
                    break;
                Thread.Sleep(2);
                count += 2;
            }
            if (count >= timeout)
                return false;
            errmsg = "";
            if (!ms326_updatePcAcc(ref errmsg))
                return false;

            return true;
        }

        // check if bp hit return int
        // -1: fail check
        // 0: no hit
        // 1: hit
        public int ms326_checkIfBpHit()
        {
            if(!shift_dr_task(ICECMD_LEN, data_to_ice, data_from_ice, 1))
            {
                errmsg = "shift dr error.";
                return -1;
            }
            if (data_from_ice[0] != 1)
                return 0;
            errmsg = "";
            if (!ms326_updatePcAcc(ref errmsg))
                return -1;
            return 1;

        }
        public bool ms326_updatePcAcc(ref string msg)
        {
            ms326reg.ms326_pc = data_from_ice[1] | ((data_from_ice[2]) << 8);

            ms326reg.ms326_pc &= 0x7fff;
            ms326reg.ms326_carry = (data_from_ice[4]) & 1;
            ms326reg.ms326_ov = (data_from_ice[4] >> 1) & 1;
            ms326reg.ms326_stack_level = ((data_from_ice[4] >> 2) + 1) & 7;
            ms326reg.ms326_acc = data_from_ice[3];
            ms326reg.ms326_next_pch = ((data_from_ice[2] & 0x80) >> 4) | (data_from_ice[4] >> 5);
            if (0==ms326_do_ice_command(ICECMD_READ_ROMP, 0))
            {
                msg=("ice get romp fail");
                
                return false;
            }
            ms326reg.ms326_romptr = data_from_ice[1] | (data_from_ice[2] << 8);


            if (0==ms326_do_ice_command(ICECMD_READ_RAMP0, 0))
            {
                msg=("ice get RAMP0 fail");
                
                return false;
            }
            ms326reg.ms326_ramptr0 = data_from_ice[1] | (data_from_ice[2] << 8);
            //ramptr0=data_from_ice[1]|(data_from_ice[2]<<8);
            //ramptr0&=0x7FF;

            return true;
        }
        //public byte inByteLowHid()
        //{
        //    hiddat[0] = (byte)++reportid;
        //    hiddat[1] = 0xa9;
        //    hiddat[2] = 1;
        //    myHid.Write(hiddat);
        //    myHid.Read(hidrb);
        //    return hidrb[2]; // PORT2 RB2 is same as FTDI

        //}
        public const int MS326ICE_IRLEN = 3;
        public const int MS326ICE_BSLEN = 59;
        public const byte MS326ICE_EXTEST = 0;
        public const byte MS326ICE_IDCODE = 2;
        public const byte MS326ICE_DBG_CODE = 5;
        public const byte MS326ICE_EVENTMODE = 6;
        public const byte MS326ICE_TRACEMODE = 3;
        public const byte MS326ICE_JTGSPI = 4;
        public const byte MS326ICE_SAMPLE_PRELOAD = 1;
        public const int EVENT_CHAIN_LEN = 78;
        public const int TRACE_CHAIN_LEN = 31;

        public const byte ICECMD_LEN = 40;

        public const byte ICECMD_HALT = 0;
        public const byte ICECMD_READ_ROMP = 1;
        public const byte ICECMD_WRITE_ROMP = 2;
        public const byte ICECMD_READ_SFR = 3;
        public const byte ICECMD_WRITE_SFR = 4;
        public const byte ICECMD_READ_RAM = 5;
        public const byte ICECMD_WRITE_RAM = 6;
        //public const byte ICECMD_READ_ROM 7;
        //public const byte ICECMD_WRITE_ROM 8;
        public const byte ICECMD_RESET = 7;
        public const byte ICECMD_WRITE_REG = 8;
        public const byte ICECMD_READ_RAMP0 = 10;
        public const byte ICECMD_WRITE_RAMP0 = 9;
        public const byte ICECMD_READ_RAM_CONT = 11;
        public const byte ICECMD_STEP = 12;
        //public const byte ICECMD_SPI 14;
        public const int SFR_A_MAX = 143;
        public const int EXTEST_DRLEN = 29;

    }

    public class ms326_reg
    {
        public int ms326_acc;
        public int ms326_pc;
        public int ms326_carry;
        public int ms326_ov;
        public int ms326_romptr;
        public int ms326_ramptr0;
        public int ms326_ramptr1;
        public int ms326_stack_level;
        public int ms326_next_pch;
        public ms326_reg()
        {
            ms326_acc = 0;
            ms326_pc = 0;
            ms326_carry = 0;
            ms326_ov = 0;
            ms326_romptr = 0xffff;
            ms326_ramptr0 = 0x87ff;
            ms326_ramptr1 = 0x87ff;
            ms326_stack_level = 0;
            ms326_next_pch = 0;

        }
    }
}
