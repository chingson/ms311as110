﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;

namespace ms110spiWri
{
    public partial class FormMS110WRI : Form
    {
        public FormMS110WRI()
        {
            InitializeComponent();
        }
        ftop myft;
        bool connected = false;
        uint spiSize;
        byte[] bin2spi;
        bool success;
        byte[] calfreq;
        public delegate void PERCENTWROTE(int percent);
        public int targetFreq;

        private void FormMS110WRI_Load(object sender, EventArgs e)
        {
            var assembly = Assembly.GetExecutingAssembly();
            //Getting names of all embedded resources
            var allResourceNames = assembly.GetManifestResourceNames();
            //Selecting first one. 
            var resourceName = allResourceNames[0];

            using (var stream = assembly.GetManifestResourceStream("ms110spiWri.Resources.calfreq.bin"))
            {
                if (stream == null)
                {
                    //DialogResult result;
                    MessageBox.Show("Internal error: code not found.");
                    Close();
                    return;
                }
                using (var memoryStream = new MemoryStream())
                {
                    stream.CopyTo(memoryStream);
                    calfreq = memoryStream.ToArray();
                }
            }
        }

        private void buttonGetID_Click(object sender, EventArgs e)
        {
            string msg = "";
            success = false;
            if(!connected)
            {
                myft = new ftop();
                
                if(!myft.openDev(ref msg))
                {
                    textBoxLog.AppendText(msg + "\r\n");
                    return;
                }
                textBoxLog.AppendText("Writer (device FT232H) found.\r\n");

                connected = true;
            }
            if (!myft.powerOffOn(ref msg,500))
            {
                textBoxLog.AppendText(msg + "\r\n");
                myft.closeDev();
                return;
            }
            myft.LEDOFF();
            Thread.Sleep(10); // after power on, delay 10ms

            if (!myft.ms311tm0(ref msg))
            {
                textBoxLog.AppendText(msg + "\r\n");
                myft.closeDev();
                return;
            }
            uint spiid = myft.spi_get_id();
            textBoxLog.AppendText("GET SPI ID " + spiid.ToString("X6")+".\r\n");
            spiSize = myft.get_spi_size((byte)(spiid & 0xff));
            textBoxLog.AppendText("Get SPI_SIZE " + spiSize + " Bytes, or "+spiSize*8/(1024*1024) +" MBITS.\r\n");

            if (spiSize > 0)
                success = true;
        }

        private void buttonAssignBinFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.FilterIndex = 1;
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                textBoxLog.AppendText("Open Bin File Cancelled.\r\n");
                return;
            }
            try
            {
                bin2spi = File.ReadAllBytes(openFileDialog1.FileName);
            } catch(Exception ebinrd)
            {
                textBoxLog.AppendText("read file error.." + ebinrd.Message);
                return;
            }
            textBoxLog.AppendText("File " + openFileDialog1.FileName + " assigned.\r\n");
            textBoxLog.AppendText("Size is " + bin2spi.Length + " bytes.\r\n");
        }

        public bool eraseSPICHIP(ref string msg)
        {
            if (!myft.spi_write_status(0x81))
            {
                textBoxLog.AppendText("write status fail.\r\n");
                return false;
            }
            myft.spi_write_single(0x06);
            myft.spi_write_single(0xc7); // chip erase
            return myft.spi_wait_busy(2000);
        }

        private void buttonErase_Click(object sender, EventArgs e)
        {
            success = false;
            if(!connected || spiSize==0)
            {
                textBoxLog.AppendText("Please connect first.\r\n");
                return;
            }
            textBoxLog.AppendText("Erasing spi flash now ...\r\n");
            textBoxLog.Update();
            //if(!myft.spi_write_status(0x81))
            //{
            //    textBoxLog.AppendText("write status fail.\r\n");
            //    return;
            //}
            //myft.spi_write_single(0x06);
            //myft.spi_write_single(0xc7); // chip erase
            //myft.spi_wait_busy(2000);
            string msg = "";
            if(!eraseSPICHIP(ref msg))
            {
                textBoxLog.AppendText("Erase CHIP fail.. " + msg + "\r\n");
                return;
            }
            textBoxLog.AppendText("SPI erased.\r\n");
            success = true;
      
        }

        
        public bool writeSPIFlash(ref string msg, byte [] content, PERCENTWROTE callback=null)
        {
            byte[] buff = new byte[256];
            int i, j;

            for (i = 0; i < content.Length; i += 256)
            {
                for (j = 0; j < buff.Length; j++)
                    buff[j] = 0xff;
                //buff[0] = 0x02;
                //buff[1] = (byte)((i >> 16) & 0xff);
                //buff[2] = (byte)((i >> 8) & 0xff);
                //buff[3] = (byte)(i & 0xff);
                for (j = 0; j < 256 && (i + j) < content.Length; j++)
                    buff[ j] = content[i + j];
                //myft.spi_write_single(0x06);
                //myft.spi_csl();
                //myft.spi_send_byte_raw(256 + 4, ref buff);
                //myft.spi_csh();
                //if (!myft.spi_wait_busy(100)) // per page
                //{
                //    msg=("SPI page write fail!!\r\n");
                //    return false;
                //}
                if(!myft.spi_write_page((uint)i,buff,256 ))
                {
                    msg = ("SPI page write fail!!\r\n");
                    return false;
                }
                if((i%32768)==0)
                    callback?.Invoke(i * 100 / content.Length);

            }
           
            //myft.spi_csl();
            //byte[] rdcmd = { 0x03, 0x00, 0x00, 0x00 };
            //myft.spi_send_byte_raw(4, ref rdcmd);
            byte[] spird = new byte[content.Length];
            //myft.spi_read_byte_raw(content.Length, ref spird);
            //myft.spi_csh();
            //myft.send_cmd();
            if(!myft.spi_read_block(0, ref spird, (uint)content.Length))
            {
                msg = ("spi read back operation fail.\r\n");
                return false;
            }
            for (i = 0; i < content.Length; i++)
            {
                if (content[i] != spird[i])
                {
                    msg=("spi read back content is different, operation failed.\r\n");
                    return false;
                }
            }
            callback?.Invoke(100);
            return true;
        }
        public void showProgress(int percent)
        {
            progressBar1.Value = percent;
            progressBar1.Refresh();
        }
        private void buttonWriteSPI_Click(object sender, EventArgs e)
        {
            success = false;
            if (!connected || spiSize == 0)
            {
                textBoxLog.AppendText("Please get ID first.\r\n");
                return;
            }
            if(bin2spi==null || bin2spi.Length==0)
            {
                textBoxLog.AppendText("Please assign file first.\r\n");
                return;
            }
            if(spiSize<bin2spi.Length)
            {
                textBoxLog.AppendText("SPI size too small!!\r\n");
                return;
            }
            string msg = "";
            if(checkBoxWaitFreq.Checked)
            {
                // update to the file
                if(bin2spi.Length>8192 && targetFreq > 28000000*0.7 &&
                    targetFreq < 28000000*1.3)
                {
                    // if internal osc is slow, the rate(period) need be smaller
                    int coef = (int)(targetFreq * 4096.0 *(100.0+trackBarFreq.Value)/100.0 / 28000000.0 );
                    bin2spi[4096 + 46] = (byte)(coef & 0xff);
                    bin2spi[4096 + 47] = (byte)(coef >> 8);
                    textBoxLog.AppendText("Original ADJ is "+((double)(targetFreq)/28000000.0).ToString("F3")+
                        ".\r\n After Extra adj, the ratio is "+ ((double)(targetFreq)*(100.0+trackBarFreq.Value)/100.0 / 28000000.0).ToString("F3")+
                        ".\r\nGive rate adj " + coef.ToString("X4")+"\r\n");
                }
            }
            if(!writeSPIFlash(ref msg, bin2spi, showProgress))
            {
                textBoxLog.AppendText(msg);
                return;
            }
            textBoxLog.AppendText("spi content wrote and checked OK.\r\n");
            success = true;
        }

        private void buttonCheckFreq_Click(object sender, EventArgs e)
        {
            success = false;
            if (!connected || spiSize == 0)
            {
                textBoxLog.AppendText("Please get ID first.\r\n");
                return;
            }
            string msg = "";
            if(!myft.spi_erase_sector(0)) // erase sector for small code
            {
                textBoxLog.AppendText("erase sector0 fail..\r\n");
                return;
            }
            if (!myft.spi_erase_sector(0x1000)) // erase sector for small code
            {
                textBoxLog.AppendText("erase sector1 fail..\r\n");
                return;
            }
            if (!writeSPIFlash(ref msg, calfreq))
            {
                textBoxLog.AppendText("write test code fail.." + msg + "\r\n");
                return;
            }
            textBoxLog.AppendText("write test code done..\r\n");
            if(!myft.ms311IO0LowPulse(ref msg,10)) // first low is to leave test mode
            //if(!myft.powerOffOn(ref msg,200))
            {
                textBoxLog.AppendText("io0 pulse fail.." + msg + "\r\n");
                return;
            }
            Thread.Sleep(50); // delay 50ms?
            if(!myft.ms311IO0LowPulse(ref msg,500,true)) // second low is to measure width
            {
                textBoxLog.AppendText("io0 width pulse fail.." + msg + "\r\n");
                return;
            }

            Thread.Sleep(300);// no chk ready, just read

            //byte rb = 0xff;
            //int count=0;
            //byte[] chk = new byte[1];
            //while((rb & 0x08)!=0 && count<5000) // adb3 connect to IO6
            //{
            //    rb = myft.inByteLow();
            //    //if(!myft.spi_read_byte_raw(1,ref chk))
            //    //{
            //    //    textBoxLog.AppendText("read back fail..\r\n");
            //    //    return;
            //    //}
            //    //rb = chk[0];
            //    ++count;
            //}
            //if(count>=5000)
            //{
            //    textBoxLog.AppendText("io2 timeout fail..");
            //    return;
            //}
            ////if (!myft.powerOffOn(ref msg, 500))
            //if (!myft.ms311IO0LowPulse(ref msg,10))
            //{
            //    textBoxLog.AppendText("re-power fail.." + msg + "\r\n");
            //    return;
            //}
            //Thread.Sleep(500);
            if (!myft.ms311tm0(ref msg)) // test mode again
            {
                textBoxLog.AppendText("test mode second time fail..");
                return;
            }
            uint spiid = myft.spi_get_id();
            textBoxLog.AppendText("GET SPI ID " + spiid.ToString("X6") + ".\r\n");
            spiSize = myft.get_spi_size((byte)(spiid & 0xff));
            textBoxLog.AppendText("Get SPI_SIZE " + spiSize + " Bytes, or " + spiSize * 8 / (1024 * 1024) + " MBITS.\r\n");
            if (spiSize < 8192)
                return;
            byte[] timerCount=new byte[16];
            myft.spi_read_block(0x1000, ref timerCount, 16);
            textBoxLog.AppendText("timer count is ");
            int i;
            for (i = 0; i < 8; i++)
                textBoxLog.AppendText(timerCount[i].ToString("X2") + ",");
            textBoxLog.AppendText("\r\n");
            targetFreq = (timerCount[0] + timerCount[1] * 256 + timerCount[2] * 65536) * 8 * 2*256;
            textBoxLog.AppendText("Estimate FREQ is " + targetFreq + "\r\n");
            if (targetFreq < 28000000 * 1.3 && targetFreq > 28000000 * 0.7)
                success = true;
            else
                textBoxLog.AppendText("Freq Out of Range.\r\n");

        }

        private void buttonReadSPIFlash_Click(object sender, EventArgs e)
        {
            if (!connected || spiSize == 0)
            {
                textBoxLog.AppendText("Please get ID first.\r\n");
                return;
            }
            //string msg = "";
            byte[] datard = new byte[spiSize];
            if(!myft.spi_read_block(0,ref datard, spiSize))
            {
                textBoxLog.AppendText("SPI read fail.\r\n");
                return;
            }
            if(saveFileDialog1.ShowDialog()!=DialogResult.OK)
            {
                textBoxLog.AppendText("save to file cancelled.\r\n");
                return;
            }
            try
            {
                File.WriteAllBytes(saveFileDialog1.FileName, datard);
            }catch(Exception  efwr)
            {
                textBoxLog.AppendText("write file fail.." + efwr.Message + "\r\n");
                return;
            }
            textBoxLog.AppendText("read data wrote to " + saveFileDialog1.FileName + "\r\n");

        }

        private void buttonFreeRun_Click(object sender, EventArgs e)
        {
            if (!connected || spiSize == 0)
            {
                textBoxLog.AppendText("Please get ID first.\r\n");
                return;
            }
            string msg = "";
            if(!myft.ms311IO0LowPulse(ref msg))
            {
                textBoxLog.AppendText("io0 pulse fail.\r\n");
                return;
            }
            connected = false;
            myft.outByteLow(0x4f, 0xc0);
            myft.outByteHigh(0x0, 0xfe);
            myft.send_cmd();
            myft.closeDev();
        }

        private void buttonPoweroff_Click(object sender, EventArgs e)
        {
            success = false;
            if (!connected || spiSize == 0)
            {
                textBoxLog.AppendText("Please get ID first.\r\n");
                return;
            }
            string msg = "";
            if(!myft.powerOff(ref msg))
            {
                textBoxLog.AppendText("power off fail "+msg +".\r\n");
                return;
            }
            spiSize = 0;
            success = true;
            textBoxLog.AppendText("now powered off.\r\n");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxACB2.Checked)
                timer1.Enabled = true;
            else
                timer1.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // auto operation
            labelWaitGetID.Text = "Get-ID:Wait..";
            checkBoxWaitFreq.Text = "FREQ:Wait..";
            checkBoxWaitErase.Text = "ERASE:Wait..";
            checkBoxWaitWrite.Text = "WRITE:Wait..";
            checkBoxVerify.Text = "Verify:Wait..";
            checkBoxWaitOff.Text = "POWER-OFF:Wait";
            Refresh();
            
            
            buttonGetID_Click(sender, e);
            if(!success)
            {
                labelWaitGetID.Text = "Get-ID:FAIL!!";
                myft.LEDFAIL();
                return;
            }
            labelWaitGetID.Text = "Get-ID:OK."; labelWaitGetID.Refresh();
            if(checkBoxWaitFreq.Checked)
            {
                buttonCheckFreq_Click(sender, e);
                if(!success)
                {
                    checkBoxWaitFreq.Text = "FREQ:FAIL!!";
                    myft.LEDFAIL();
                    return;
                }
                checkBoxWaitFreq.Text = "FREQ:OK.";
                checkBoxWaitFreq.Refresh();
            }
            
            

            if(checkBoxWaitErase.Checked)
            {
                buttonErase_Click(sender, e);
                if(!success)
                {
                    checkBoxWaitErase.Text = "ERASE:FAIL!!";
                    myft.LEDFAIL();
                    return;
                }
                checkBoxWaitErase.Text = "ERASE:OK.";
                checkBoxWaitErase.Refresh();     
            }

            if(checkBoxWaitWrite.Checked)
            {
                buttonWriteSPI_Click(sender, e);
                if(!success)
                {
                    checkBoxWaitWrite.Text = "WRITE:FAIL!!";
                    myft.LEDFAIL();
                    return;
                }
                checkBoxWaitWrite.Text = "WRITE:OK.";
                checkBoxWaitWrite.Refresh();
            }

            if(checkBoxVerify.Checked)
            {
                buttonVerify_Click(sender, e);
                if(!success)
                {
                    checkBoxVerify.Text = "Verify:FAIL!!";
                    myft.LEDFAIL();
                    return;
                }
                checkBoxVerify.Text = "Verify:OK.";
                checkBoxVerify.Refresh();
            }
            if(checkBoxWaitOff.Checked)
            {
                buttonPoweroff_Click(sender, e);
                if(!success)
                {
                    checkBoxWaitOff.Text = "POWER-OFF:FAIL!!";
                    myft.LEDFAIL();
                    return;
                }
                checkBoxWaitOff.Text = "POWER-OFF:OK.";
                //checkBoxWaitOff.Refresh();
            }
            myft.LEDOK();


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (bin2spi == null || bin2spi.Length < 8192 || !connected)
                return;
            if((myft.inByteHigh()&0x04)==0)
            {
                button1_Click(sender, e);
            }
        }

        private void trackBarFreq_Scroll(object sender, EventArgs e)
        {
            labelExtraAdj.Text = "Extra Adj:" + trackBarFreq.Value+"%";
        }

        private void buttonVerify_Click(object sender, EventArgs e)
        {
            success = false;
            if (!connected || spiSize == 0 || bin2spi.Length==0)
            {
                textBoxLog.AppendText("Please connect and assign BIN first.\r\n");
                return;
            }
            textBoxLog.AppendText("Verify spi flash now ...\r\n");
            textBoxLog.Update();
            byte[] datard = new byte[bin2spi.Length];
            if (!myft.spi_read_block(0, ref datard, (uint)bin2spi.Length))
            {
                textBoxLog.AppendText("SPI read fail.\r\n");
                return;
            }
            int i;
            for(i=0;i< bin2spi.Length; i++)
            {
                if(datard[i]!=bin2spi[i] && i != (4096+46) && i!=(4096+47))
                {
                    textBoxLog.AppendText("SPI flash verify fail.\r\n");
                    return;
                }
            }
            textBoxLog.AppendText("SPI flash verify OK.\r\n");
            success = true;
        }

        private void checkBoxWaitWrite_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxWaitWrite.Checked)
            {
                checkBoxVerify.Checked = false;
            }
        }

        private void checkBoxVerify_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxVerify.Checked)
            {
                checkBoxWaitFreq.Checked = false;
                checkBoxWaitErase.Checked = false;
                checkBoxWaitWrite.Checked = false;
            }
        }

        private void checkBoxWaitErase_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxWaitErase.Checked)
            {
                checkBoxVerify.Checked = false;
            }
        }

        private void checkBoxWaitFreq_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxWaitFreq.Checked)
            {
                checkBoxVerify.Checked = false;
            }
        }
    }
}
