﻿namespace ms110spiWri
{
    partial class FormMS110WRI
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonGetID = new System.Windows.Forms.Button();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.buttonAssignBinFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonErase = new System.Windows.Forms.Button();
            this.buttonWriteSPI = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.buttonCheckFreq = new System.Windows.Forms.Button();
            this.buttonReadSPIFlash = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.buttonFreeRun = new System.Windows.Forms.Button();
            this.buttonPoweroff = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelExtraAdj = new System.Windows.Forms.Label();
            this.trackBarFreq = new System.Windows.Forms.TrackBar();
            this.labelWaitGetID = new System.Windows.Forms.Label();
            this.checkBoxWaitOff = new System.Windows.Forms.CheckBox();
            this.checkBoxWaitWrite = new System.Windows.Forms.CheckBox();
            this.checkBoxWaitErase = new System.Windows.Forms.CheckBox();
            this.checkBoxWaitFreq = new System.Windows.Forms.CheckBox();
            this.checkBoxACB2 = new System.Windows.Forms.CheckBox();
            this.buttonAuto = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonVerify = new System.Windows.Forms.Button();
            this.checkBoxVerify = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFreq)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonGetID
            // 
            this.buttonGetID.Location = new System.Drawing.Point(12, 60);
            this.buttonGetID.Name = "buttonGetID";
            this.buttonGetID.Size = new System.Drawing.Size(100, 31);
            this.buttonGetID.TabIndex = 0;
            this.buttonGetID.Text = "GET ID PWR ON";
            this.buttonGetID.UseVisualStyleBackColor = true;
            this.buttonGetID.Click += new System.EventHandler(this.buttonGetID_Click);
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(352, 12);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.Size = new System.Drawing.Size(250, 264);
            this.textBoxLog.TabIndex = 1;
            this.textBoxLog.Text = "Log:";
            // 
            // buttonAssignBinFile
            // 
            this.buttonAssignBinFile.Location = new System.Drawing.Point(12, 23);
            this.buttonAssignBinFile.Name = "buttonAssignBinFile";
            this.buttonAssignBinFile.Size = new System.Drawing.Size(100, 31);
            this.buttonAssignBinFile.TabIndex = 2;
            this.buttonAssignBinFile.Text = "Assign BIN FILE";
            this.buttonAssignBinFile.UseVisualStyleBackColor = true;
            this.buttonAssignBinFile.Click += new System.EventHandler(this.buttonAssignBinFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "\"BIN files  (*.bin)|*.bin| ALL FILES(*.*)|*.*";
            // 
            // buttonErase
            // 
            this.buttonErase.Location = new System.Drawing.Point(12, 134);
            this.buttonErase.Name = "buttonErase";
            this.buttonErase.Size = new System.Drawing.Size(100, 31);
            this.buttonErase.TabIndex = 3;
            this.buttonErase.Text = "Erase SPI FLASH";
            this.buttonErase.UseVisualStyleBackColor = true;
            this.buttonErase.Click += new System.EventHandler(this.buttonErase_Click);
            // 
            // buttonWriteSPI
            // 
            this.buttonWriteSPI.Location = new System.Drawing.Point(12, 171);
            this.buttonWriteSPI.Name = "buttonWriteSPI";
            this.buttonWriteSPI.Size = new System.Drawing.Size(100, 31);
            this.buttonWriteSPI.TabIndex = 4;
            this.buttonWriteSPI.Text = "Write SPI FLASH";
            this.buttonWriteSPI.UseVisualStyleBackColor = true;
            this.buttonWriteSPI.Click += new System.EventHandler(this.buttonWriteSPI_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(127, 325);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(475, 27);
            this.progressBar1.TabIndex = 5;
            // 
            // buttonCheckFreq
            // 
            this.buttonCheckFreq.Location = new System.Drawing.Point(12, 97);
            this.buttonCheckFreq.Name = "buttonCheckFreq";
            this.buttonCheckFreq.Size = new System.Drawing.Size(100, 31);
            this.buttonCheckFreq.TabIndex = 6;
            this.buttonCheckFreq.Text = "Check FREQ";
            this.buttonCheckFreq.UseVisualStyleBackColor = true;
            this.buttonCheckFreq.Click += new System.EventHandler(this.buttonCheckFreq_Click);
            // 
            // buttonReadSPIFlash
            // 
            this.buttonReadSPIFlash.Location = new System.Drawing.Point(12, 245);
            this.buttonReadSPIFlash.Name = "buttonReadSPIFlash";
            this.buttonReadSPIFlash.Size = new System.Drawing.Size(100, 31);
            this.buttonReadSPIFlash.TabIndex = 7;
            this.buttonReadSPIFlash.Text = "Read SPI FLASH";
            this.buttonReadSPIFlash.UseVisualStyleBackColor = true;
            this.buttonReadSPIFlash.Click += new System.EventHandler(this.buttonReadSPIFlash_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "BIN Files (*.bin)|*.bin|ALL Files (*.*)|*.*";
            // 
            // buttonFreeRun
            // 
            this.buttonFreeRun.Location = new System.Drawing.Point(12, 321);
            this.buttonFreeRun.Name = "buttonFreeRun";
            this.buttonFreeRun.Size = new System.Drawing.Size(100, 31);
            this.buttonFreeRun.TabIndex = 8;
            this.buttonFreeRun.Text = "Free Run";
            this.buttonFreeRun.UseVisualStyleBackColor = true;
            this.buttonFreeRun.Click += new System.EventHandler(this.buttonFreeRun_Click);
            // 
            // buttonPoweroff
            // 
            this.buttonPoweroff.Location = new System.Drawing.Point(12, 284);
            this.buttonPoweroff.Name = "buttonPoweroff";
            this.buttonPoweroff.Size = new System.Drawing.Size(100, 31);
            this.buttonPoweroff.TabIndex = 9;
            this.buttonPoweroff.Text = "Power Off";
            this.buttonPoweroff.UseVisualStyleBackColor = true;
            this.buttonPoweroff.Click += new System.EventHandler(this.buttonPoweroff_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxVerify);
            this.groupBox1.Controls.Add(this.labelExtraAdj);
            this.groupBox1.Controls.Add(this.trackBarFreq);
            this.groupBox1.Controls.Add(this.labelWaitGetID);
            this.groupBox1.Controls.Add(this.checkBoxWaitOff);
            this.groupBox1.Controls.Add(this.checkBoxWaitWrite);
            this.groupBox1.Controls.Add(this.checkBoxWaitErase);
            this.groupBox1.Controls.Add(this.checkBoxWaitFreq);
            this.groupBox1.Controls.Add(this.checkBoxACB2);
            this.groupBox1.Controls.Add(this.buttonAuto);
            this.groupBox1.Location = new System.Drawing.Point(127, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 314);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "AUTO Operations";
            // 
            // labelExtraAdj
            // 
            this.labelExtraAdj.AutoSize = true;
            this.labelExtraAdj.Location = new System.Drawing.Point(115, 71);
            this.labelExtraAdj.Name = "labelExtraAdj";
            this.labelExtraAdj.Size = new System.Drawing.Size(71, 12);
            this.labelExtraAdj.TabIndex = 12;
            this.labelExtraAdj.Text = "Extra Adj: 0%";
            // 
            // trackBarFreq
            // 
            this.trackBarFreq.Location = new System.Drawing.Point(103, 100);
            this.trackBarFreq.Minimum = -10;
            this.trackBarFreq.Name = "trackBarFreq";
            this.trackBarFreq.Size = new System.Drawing.Size(104, 45);
            this.trackBarFreq.TabIndex = 11;
            this.trackBarFreq.Scroll += new System.EventHandler(this.trackBarFreq_Scroll);
            // 
            // labelWaitGetID
            // 
            this.labelWaitGetID.AutoSize = true;
            this.labelWaitGetID.Location = new System.Drawing.Point(6, 64);
            this.labelWaitGetID.Name = "labelWaitGetID";
            this.labelWaitGetID.Size = new System.Drawing.Size(73, 12);
            this.labelWaitGetID.TabIndex = 10;
            this.labelWaitGetID.Text = "GETID:Wait ..";
            // 
            // checkBoxWaitOff
            // 
            this.checkBoxWaitOff.AutoSize = true;
            this.checkBoxWaitOff.Checked = true;
            this.checkBoxWaitOff.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxWaitOff.Location = new System.Drawing.Point(8, 287);
            this.checkBoxWaitOff.Name = "checkBoxWaitOff";
            this.checkBoxWaitOff.Size = new System.Drawing.Size(118, 16);
            this.checkBoxWaitOff.TabIndex = 9;
            this.checkBoxWaitOff.Text = "POWER OFF:Wait..";
            this.checkBoxWaitOff.UseVisualStyleBackColor = true;
            // 
            // checkBoxWaitWrite
            // 
            this.checkBoxWaitWrite.AutoSize = true;
            this.checkBoxWaitWrite.Checked = true;
            this.checkBoxWaitWrite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxWaitWrite.Location = new System.Drawing.Point(6, 174);
            this.checkBoxWaitWrite.Name = "checkBoxWaitWrite";
            this.checkBoxWaitWrite.Size = new System.Drawing.Size(92, 16);
            this.checkBoxWaitWrite.TabIndex = 8;
            this.checkBoxWaitWrite.Text = "WRITE:Wait..";
            this.checkBoxWaitWrite.UseVisualStyleBackColor = true;
            this.checkBoxWaitWrite.CheckedChanged += new System.EventHandler(this.checkBoxWaitWrite_CheckedChanged);
            // 
            // checkBoxWaitErase
            // 
            this.checkBoxWaitErase.AutoSize = true;
            this.checkBoxWaitErase.Checked = true;
            this.checkBoxWaitErase.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxWaitErase.Location = new System.Drawing.Point(6, 137);
            this.checkBoxWaitErase.Name = "checkBoxWaitErase";
            this.checkBoxWaitErase.Size = new System.Drawing.Size(91, 16);
            this.checkBoxWaitErase.TabIndex = 7;
            this.checkBoxWaitErase.Text = "ERASE:Wait..";
            this.checkBoxWaitErase.UseVisualStyleBackColor = true;
            this.checkBoxWaitErase.CheckedChanged += new System.EventHandler(this.checkBoxWaitErase_CheckedChanged);
            // 
            // checkBoxWaitFreq
            // 
            this.checkBoxWaitFreq.AutoSize = true;
            this.checkBoxWaitFreq.Checked = true;
            this.checkBoxWaitFreq.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxWaitFreq.Location = new System.Drawing.Point(6, 100);
            this.checkBoxWaitFreq.Name = "checkBoxWaitFreq";
            this.checkBoxWaitFreq.Size = new System.Drawing.Size(84, 16);
            this.checkBoxWaitFreq.TabIndex = 6;
            this.checkBoxWaitFreq.Text = "FREQ:Wait..";
            this.checkBoxWaitFreq.UseVisualStyleBackColor = true;
            this.checkBoxWaitFreq.CheckedChanged += new System.EventHandler(this.checkBoxWaitFreq_CheckedChanged);
            // 
            // checkBoxACB2
            // 
            this.checkBoxACB2.AutoSize = true;
            this.checkBoxACB2.Location = new System.Drawing.Point(121, 26);
            this.checkBoxACB2.Name = "checkBoxACB2";
            this.checkBoxACB2.Size = new System.Drawing.Size(89, 16);
            this.checkBoxACB2.TabIndex = 4;
            this.checkBoxACB2.Text = "ACB2 Button";
            this.checkBoxACB2.UseVisualStyleBackColor = true;
            this.checkBoxACB2.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // buttonAuto
            // 
            this.buttonAuto.Location = new System.Drawing.Point(6, 18);
            this.buttonAuto.Name = "buttonAuto";
            this.buttonAuto.Size = new System.Drawing.Size(100, 31);
            this.buttonAuto.TabIndex = 3;
            this.buttonAuto.Text = "Auto";
            this.buttonAuto.UseVisualStyleBackColor = true;
            this.buttonAuto.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // buttonVerify
            // 
            this.buttonVerify.Location = new System.Drawing.Point(12, 208);
            this.buttonVerify.Name = "buttonVerify";
            this.buttonVerify.Size = new System.Drawing.Size(100, 31);
            this.buttonVerify.TabIndex = 11;
            this.buttonVerify.Text = "Verify SPI FLASH";
            this.buttonVerify.UseVisualStyleBackColor = true;
            this.buttonVerify.Click += new System.EventHandler(this.buttonVerify_Click);
            // 
            // checkBoxVerify
            // 
            this.checkBoxVerify.AutoSize = true;
            this.checkBoxVerify.Location = new System.Drawing.Point(6, 211);
            this.checkBoxVerify.Name = "checkBoxVerify";
            this.checkBoxVerify.Size = new System.Drawing.Size(85, 16);
            this.checkBoxVerify.TabIndex = 13;
            this.checkBoxVerify.Text = "Verify:Wait..";
            this.checkBoxVerify.UseVisualStyleBackColor = true;
            this.checkBoxVerify.CheckedChanged += new System.EventHandler(this.checkBoxVerify_CheckedChanged);
            // 
            // FormMS110WRI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 364);
            this.Controls.Add(this.buttonVerify);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonPoweroff);
            this.Controls.Add(this.buttonFreeRun);
            this.Controls.Add(this.buttonReadSPIFlash);
            this.Controls.Add(this.buttonCheckFreq);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonWriteSPI);
            this.Controls.Add(this.buttonErase);
            this.Controls.Add(this.buttonAssignBinFile);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.buttonGetID);
            this.Name = "FormMS110WRI";
            this.Text = "MS110 SPIFLASH WRITER";
            this.Load += new System.EventHandler(this.FormMS110WRI_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFreq)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGetID;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Button buttonAssignBinFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonErase;
        private System.Windows.Forms.Button buttonWriteSPI;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button buttonCheckFreq;
        private System.Windows.Forms.Button buttonReadSPIFlash;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button buttonFreeRun;
        private System.Windows.Forms.Button buttonPoweroff;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelWaitGetID;
        private System.Windows.Forms.CheckBox checkBoxWaitOff;
        private System.Windows.Forms.CheckBox checkBoxWaitWrite;
        private System.Windows.Forms.CheckBox checkBoxWaitErase;
        private System.Windows.Forms.CheckBox checkBoxWaitFreq;
        private System.Windows.Forms.CheckBox checkBoxACB2;
        private System.Windows.Forms.Button buttonAuto;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelExtraAdj;
        private System.Windows.Forms.TrackBar trackBarFreq;
        private System.Windows.Forms.Button buttonVerify;
        private System.Windows.Forms.CheckBox checkBoxVerify;
    }
}

