#ifndef __MS311AS110_h__
#define __MS311AS110_h__

// key-overwrite, 3v, repeat, seq mode, randum
//#define MS110_GLOBALOPT (*((const unsigned char*) 0x1000))
#define MS110_GLOBALOPT config[0]

#define MS110_KEYOVERWRITE (MS110_GLOBALOPT&1)
#define MS110_USE3V (MS110_GLOBALOPT&2)
#define MS110_AUTORPT (MS110_GLOBALOPT&4)
#define MS110_SEQMODE (MS110_GLOBALOPT&8)
#define MS110_RANDMODE (MS110_GLOBALOPT&0x10)
#define MS110_RANDORSEQMODE (MS110_GLOBALOPT&0x18)

// IO0~IO3 controls, PULLH, LEVHOLD, 0x1001~0x1004
#define MS110_IOCTRLS (config+1)
#define MS110_LEVELHOLD (0x1)
#define MS110_NONRETRIGGER (0x2)
#define MS110_EXTPULLHIGH(x) (config[1+x]&0x04)

// LED options
#define MS110_LED0OPT config[5]
#define MS110_LED0LEV config[6]
#define MS110_LED1OPT config[7]
#define MS110_LED1LEV config[8]
#define MS110_LED_PLAY_ON 0x01
#define MS110_LED_PLAY_FLASH 0x02
#define MS110_LED_PLAY_LEV 0x04

// THRLD, target is 4MS period
#define MS110_THRLD config[9]
// present will set 0, no present is 1 for bit 0..3
#define MS110_VOICE_DIS config[10]
#define MS110_VOICE_VOL config[11]

#define MS110_STARTPAGES ((unsigned short *)(config+16))
#define MS110_ENDPAGES ((unsigned short *)(config+26))
#define MS110_RATES ((unsigned short *)(config+36))
#define MS110_RATE_ADJ ((unsigned short *)(config+46))

#define SYS_IDLE 0
#define SYS_PLAY0 0x10
#define SYS_PLAY1 0x11
#define SYS_PLAY2 0x12
#define SYS_PLAY3 0x13

#define KEYS_NOKEY 0
#define KEYS_DEB 1
#define KEYS_WAIT_RELEASE 2

#define IO_KEY0 0x01
#define IO_KEY1 0x02
#define IO_KEY2 0x04
#define IO_KEY3 0x08

#define KEY_CODE0 0x1
#define KEY_CODE1 0x2
#define KEY_CODE2 0x3
#define KEY_CODE3 0x4

// debunce 10ms should be enough, 3*4=12ms
#define KEY_WAIT 3
// after 0.1s sleep
#define TIME_TO_SLEEP 5

#define LED0_ON led0_on()
#define LED1_ON led1_on()
#define LED0_OFF led0_off()
#define LED1_OFF led1_off()
#define LED0_TOGGLE {IODIR|=0x20;IO^=0x20;}
#define LED1_TOGGLE {IODIR|=0x40;IO^=0x40;}

// 250/6~=41
#define LED_TIMER_RELOAD 41

#endif