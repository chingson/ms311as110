;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #bbc754a74 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"C:\work\ms311as110\sw"
;;	.file	"MS311AS110.c"
	.module MS311AS110
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:FMS311AS110$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$init$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$init$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$led1_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$led1_on$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$led1_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$led1_off$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$led0_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$led0_on$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$led0_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$led0_off$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$getLastID$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$getLastID$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$get_key$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$get_key$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$key_machine$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$key_machine$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$timer_routine$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$timer_routine$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$enter_idle_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$enter_idle_mode$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$startPlay$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$startPlay$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$sys_idle$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$sys_idle$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$sys_play$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$sys_play$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$main$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$main$0$0({2}DF,SI:S),C,0,0,0,0,0
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
	.globl _main
;--------------------------------------------------------
; reset vector 
;--------------------------------------------------------
	.area STARTUP	(code,REL,CON) 
	jmp	__sdcc_gsinit_startup
.area CODE (code,REL,CON) ; MS311AS110-code 
.globl _main

;--------------------------------------------------------
	.FUNC _main:$PNUM 0:$C:_init:$C:_startPlay:$C:_led0_on:$C:_led1_on\
:$C:_timer_routine:$C:_sys_idle:$C:_sys_play\

;--------------------------------------------------------
;	.line	401; "MS311AS110.c"	init();
_main:	;Function start
	CALL	_init
;	;.line	402; "MS311AS110.c"	if (((IO & 0x80) == 0x0) && !(MS110_VOICE_DIS & 0x10))
	LDA	_IO
	JMI	_00635_DS_
	LDA	#0x0a
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x10
	JNZ	_00635_DS_
_00617_DS_:
;	;.line	404; "MS311AS110.c"	while (!(IO & 0x80))
	LDA	_IO
	JPL	_00617_DS_
;	;.line	406; "MS311AS110.c"	if ((IO & 0x0f) == 0x0f)
	LDA	_IO
	AND	#0x0f
	XOR	#0x0f
	JNZ	_00635_DS_
;	;.line	408; "MS311AS110.c"	if (startPlay(4))
	LDA	#0x04
	CALL	_startPlay
	JZ	_00635_DS_
;	;.line	410; "MS311AS110.c"	system_state = 0x14;
	LDA	#0x14
	STA	_system_state
;	;.line	411; "MS311AS110.c"	if (MS110_LED0OPT & MS110_LED_PLAY_ON)
	LDA	#0x05
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	SHR	
	JNC	_00621_DS_
;	;.line	413; "MS311AS110.c"	LED0_ON;
	CALL	_led0_on
_00621_DS_:
;	;.line	415; "MS311AS110.c"	if (MS110_LED1OPT & MS110_LED_PLAY_ON)
	LDA	#0x07
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	SHR	
	JNC	_00635_DS_
;	;.line	417; "MS311AS110.c"	LED1_ON;
	CALL	_led1_on
_00635_DS_:
;	;.line	425; "MS311AS110.c"	timer_routine();
	CALL	_timer_routine
;	;.line	426; "MS311AS110.c"	if (system_state == SYS_IDLE)
	LDA	_system_state
	JNZ	_00632_DS_
;	;.line	427; "MS311AS110.c"	sys_idle();
	CALL	_sys_idle
	JMP	_00635_DS_
_00632_DS_:
;	;.line	429; "MS311AS110.c"	sys_play();
	CALL	_sys_play
	JMP	_00635_DS_
;	;.line	431; "MS311AS110.c"	}
	RET	
; exit point of _main
	.ENDFUNC _main
.globl _sys_play

;--------------------------------------------------------
	.FUNC _sys_play:$PNUM 0:$C:_api_play_job:$C:_led0_on:$C:_led0_off:$C:_led1_on\
:$C:_led1_off:$C:_api_play_stop:$C:_startPlay:$C:_enter_idle_mode:$C:_api_enter_stdby_mode\
:$L:r0x119B:$L:r0x119C:$L:r0x119D:$L:r0x119E:$L:r0x119F\

;--------------------------------------------------------
;	.line	264; "MS311AS110.c"	unsigned char result = api_play_job();
_sys_play:	;Function start
	CALL	_api_play_job
	STA	r0x119B
;	;.line	265; "MS311AS110.c"	BYTE poweronmsg = ((system_state & 0x0f) == 4);
	LDA	#0x0f
	AND	_system_state
	STA	r0x119C
	XOR	#0x04
	LDC	_Z
	CLRA	
	ROL	
	STA	r0x119E
;	;.line	266; "MS311AS110.c"	if (poweronmsg)
	JZ	_00436_DS_
;	;.line	268; "MS311AS110.c"	playmode = 0;
	CLRA	
	STA	r0x119F
	JMP	_00437_DS_
_00436_DS_:
;	;.line	270; "MS311AS110.c"	else if (MS110_RANDORSEQMODE)
	CLRA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x18
	JZ	_00433_DS_
;	;.line	272; "MS311AS110.c"	playmode = MS110_IOCTRLS[0];
	LDA	#0x01
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x119F
	JMP	_00437_DS_
_00433_DS_:
;	;.line	276; "MS311AS110.c"	playmode = MS110_IOCTRLS[system_state & 0x0f];
	LDA	r0x119C
	INCA	
	STA	r0x119C
	CLRA	
	ADDC	#0x10
	STA	r0x119D
	LDA	r0x119C
	STA	_ROMPL
	LDA	r0x119D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x119F
_00437_DS_:
;	;.line	278; "MS311AS110.c"	if (result == 2) // update LED
	LDA	r0x119B
	XOR	#0x02
	JNZ	_00455_DS_
;	;.line	280; "MS311AS110.c"	if (MS110_LED0OPT & MS110_LED_PLAY_LEV)
	LDA	#0x05
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x04
	JZ	_00442_DS_
;	;.line	282; "MS311AS110.c"	if (PWRH >= MS110_LED0LEV) // PWRH 16 level per 3db
	LDA	#0x06
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x119C
	SETB	_C
	LDA	_PWRH
	SUBB	r0x119C
	JNC	_00439_DS_
;	;.line	284; "MS311AS110.c"	LED0_ON;
	CALL	_led0_on
	JMP	_00442_DS_
_00439_DS_:
;	;.line	288; "MS311AS110.c"	LED0_OFF;
	CALL	_led0_off
_00442_DS_:
;	;.line	291; "MS311AS110.c"	if (MS110_LED1OPT & MS110_LED_PLAY_LEV)
	LDA	#0x07
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x04
	JZ	_00456_DS_
;	;.line	293; "MS311AS110.c"	if (PWRH >= MS110_LED1LEV)
	LDA	#0x08
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x119C
	SETB	_C
	LDA	_PWRH
	SUBB	r0x119C
	JNC	_00444_DS_
;	;.line	295; "MS311AS110.c"	LED1_ON;
	CALL	_led1_on
	JMP	_00456_DS_
_00444_DS_:
;	;.line	299; "MS311AS110.c"	LED1_OFF;
	CALL	_led1_off
	JMP	_00456_DS_
_00455_DS_:
;	;.line	303; "MS311AS110.c"	else if (!result) // play finish
	LDA	r0x119B
	JNZ	_00456_DS_
;	;.line	306; "MS311AS110.c"	api_play_stop();
	CALL	_api_play_stop
;	;.line	308; "MS311AS110.c"	if ( (MS110_GLOBALOPT & MS110_AUTORPT ) && ((!poweronmsg) || ((MS110_VOICE_DIS&0x0f)==0x0f)))
	CLRA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x119C
	AND	#0x04
	AND	r0x119C
	JZ	_00449_DS_
	LDA	r0x119E
	JZ	_00448_DS_
	LDA	#0x0a
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x0f
	XOR	#0x0f
	JNZ	_00449_DS_
_00448_DS_:
;	;.line	310; "MS311AS110.c"	startPlay(system_state & 0x0f);
	LDA	_system_state
	AND	#0x0f
	CALL	_startPlay
;	;.line	311; "MS311AS110.c"	return;
	JMP	_00486_DS_
_00449_DS_:
;	;.line	313; "MS311AS110.c"	enter_idle_mode(); // other options should enter idle mode
	CALL	_enter_idle_mode
;	;.line	314; "MS311AS110.c"	return;
	JMP	_00486_DS_
_00456_DS_:
;	;.line	316; "MS311AS110.c"	if (!led_timer)
	LDA	_led_timer
	JNZ	_00462_DS_
;	;.line	318; "MS311AS110.c"	led_timer = LED_TIMER_RELOAD;
	LDA	#0x29
	STA	_led_timer
;	;.line	319; "MS311AS110.c"	if (MS110_LED0OPT & MS110_LED_PLAY_FLASH)
	LDA	#0x05
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x02
	JZ	_00458_DS_
;	;.line	320; "MS311AS110.c"	LED0_TOGGLE;
	LDA	_IODIR
	ORA	#0x20
	STA	_IODIR
	LDA	_IO
	XOR	#0x20
	STA	_IO
_00458_DS_:
;	;.line	321; "MS311AS110.c"	if (MS110_LED1OPT & MS110_LED_PLAY_FLASH)
	LDA	#0x07
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x02
	JZ	_00462_DS_
;	;.line	322; "MS311AS110.c"	LED1_TOGGLE;
	LDA	_IODIR
	ORA	#0x40
	STA	_IODIR
	LDA	_IO
	XOR	#0x40
	STA	_IO
_00462_DS_:
;	;.line	326; "MS311AS110.c"	if ((playmode & MS110_LEVELHOLD) && !key_state && !(playmode & MS110_NONRETRIGGER) && !poweronmsg)
	LDA	r0x119F
	SHR	
	JNC	_00464_DS_
	LDA	_key_state
	JNZ	_00464_DS_
	LDA	r0x119F
	AND	#0x02
	ORA	r0x119E
	JNZ	_00464_DS_
;	;.line	328; "MS311AS110.c"	enter_idle_mode();
	CALL	_enter_idle_mode
;	;.line	329; "MS311AS110.c"	return;
	JMP	_00486_DS_
_00464_DS_:
;	;.line	332; "MS311AS110.c"	if (key_code)
	LDA	_key_code
;	;.line	342; "MS311AS110.c"	if (((BYTE)(key_code - 1)) == ((BYTE)(system_state & 0x0f)) && !(MS110_RANDORSEQMODE) // rand/seq mode, view as different key
	JZ	_00482_DS_
	DECA	
	STA	r0x119C
	LDA	_system_state
	AND	#0x0f
	XOR	r0x119C
	JNZ	_00478_DS_
	CLRA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x18
	JNZ	_00478_DS_
;	;.line	345; "MS311AS110.c"	if (playmode & MS110_NONRETRIGGER)
	LDA	r0x119F
	AND	#0x02
	JZ	_00469_DS_
;	;.line	347; "MS311AS110.c"	key_code = 0;
	CLRA	
	STA	_key_code
;	;.line	349; "MS311AS110.c"	return; // continue play
	JMP	_00486_DS_
_00469_DS_:
;	;.line	353; "MS311AS110.c"	enter_idle_mode();
	CALL	_enter_idle_mode
;	;.line	354; "MS311AS110.c"	key_code = 0; // stop play!!
	CLRA	
	STA	_key_code
;	;.line	355; "MS311AS110.c"	return;
	JMP	_00486_DS_
_00478_DS_:
;	;.line	360; "MS311AS110.c"	if (MS110_KEYOVERWRITE)
	CLRA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	SHR	
	JNC	_00475_DS_
;	;.line	362; "MS311AS110.c"	enter_idle_mode();
	CALL	_enter_idle_mode
;	;.line	363; "MS311AS110.c"	return; // key key-code to play new voice
	JMP	_00486_DS_
_00475_DS_:
;	;.line	365; "MS311AS110.c"	else if (playmode & MS110_NONRETRIGGER)
	LDA	r0x119F
	AND	#0x02
	JZ	_00472_DS_
;	;.line	367; "MS311AS110.c"	key_code = 0;
	CLRA	
	STA	_key_code
;	;.line	368; "MS311AS110.c"	return; // continue play
	JMP	_00486_DS_
_00472_DS_:
;	;.line	372; "MS311AS110.c"	enter_idle_mode();
	CALL	_enter_idle_mode
;	;.line	373; "MS311AS110.c"	key_code = 0;
	CLRA	
	STA	_key_code
;	;.line	374; "MS311AS110.c"	return; // key key-code to play new voice
	JMP	_00486_DS_
_00482_DS_:
;	;.line	391; "MS311AS110.c"	if (key_state)
	LDA	_key_state
	JZ	_00484_DS_
;	;.line	392; "MS311AS110.c"	api_enter_stdby_mode(0, 0, 0);
	CLRA	
	STA	_api_enter_stdby_mode_STK01
	STA	_api_enter_stdby_mode_STK00
	CALL	_api_enter_stdby_mode
	JMP	_00486_DS_
_00484_DS_:
;	;.line	395; "MS311AS110.c"	api_enter_stdby_mode(0x0f, 0, 0); // keep timer
	CLRA	
	STA	_api_enter_stdby_mode_STK01
	STA	_api_enter_stdby_mode_STK00
	LDA	#0x0f
	CALL	_api_enter_stdby_mode
_00486_DS_:
;	;.line	397; "MS311AS110.c"	}
	RET	
; exit point of _sys_play
	.ENDFUNC _sys_play
.globl _sys_idle

;--------------------------------------------------------
	.FUNC _sys_idle:$PNUM 0:$C:_getLastID:$C:_api_rec_prepare:$C:_timer_routine:$C:_api_enter_stdby_mode\
:$C:__shl_short:$C:_api_rec_stop:$C:_startPlay:$C:_led0_on:$C:_led1_on\
:$C:_api_normal_sleep:$C:_api_enter_dsleep_mode\
:$L:r0x118D:$L:r0x118E:$L:r0x118F:$L:r0x1190
;--------------------------------------------------------
;	.line	190; "MS311AS110.c"	if (key_code)
_sys_idle:	;Function start
	LDA	_key_code
;	;.line	192; "MS311AS110.c"	unsigned char songID = key_code - 1;
	JZ	_00336_DS_
	DECA	
	STA	r0x118D
;	;.line	194; "MS311AS110.c"	if (MS110_RANDMODE)
	CLRA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x118E
	AND	#0x10
	JZ	_00327_DS_
;	;.line	198; "MS311AS110.c"	getLastID();
	CALL	_getLastID
;	;.line	199; "MS311AS110.c"	api_rec_prepare(API_AD_OSR128, 0xff, API_EN5K_ON);
	LDA	#0x10
	STA	_api_rec_prepare_STK01
	LDA	#0xff
	STA	_api_rec_prepare_STK00
	LDA	#0x80
	CALL	_api_rec_prepare
_00318_DS_:
;	;.line	202; "MS311AS110.c"	sleep_timer = 5; // 20ms enough?
	LDA	#0x05
	STA	_sleep_timer
_00314_DS_:
;	;.line	203; "MS311AS110.c"	while (sleep_timer)
	LDA	_sleep_timer
	JZ	_00316_DS_
;	;.line	205; "MS311AS110.c"	timer_routine();
	CALL	_timer_routine
;	;.line	206; "MS311AS110.c"	api_enter_stdby_mode(0, 0, 0);
	CLRA	
	STA	_api_enter_stdby_mode_STK01
	STA	_api_enter_stdby_mode_STK00
	CALL	_api_enter_stdby_mode
	JMP	_00314_DS_
_00316_DS_:
;	;.line	209; "MS311AS110.c"	songID = (ADL >> 3) & 3;
	LDA	_ADL
	SHR	
	SHR	
	SHR	
	AND	#0x03
	STA	r0x118D
;	;.line	210; "MS311AS110.c"	} while ((lastID == songID) || ((1 << songID) & MS110_VOICE_DIS));
	XOR	_lastID
	JZ	_00318_DS_
	CLRA	
	STA	STK00
	INCA	
	STA	_PTRCL
	LDA	r0x118D
	CALL	__shl_short
	LDA	STK00
	STA	r0x1190
	LDA	_PTRCL
	STA	r0x118F
	LDA	#0x0a
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	r0x118F
	STA	r0x118F
	CLRA	
	AND	r0x1190
	ORA	r0x118F
	JNZ	_00318_DS_
;	;.line	212; "MS311AS110.c"	api_rec_stop(0);
	CLRA	
	CALL	_api_rec_stop
;	;.line	213; "MS311AS110.c"	lastID = songID;
	LDA	r0x118D
	STA	_lastID
	JMP	_00328_DS_
_00327_DS_:
;	;.line	215; "MS311AS110.c"	else if (MS110_SEQMODE)
	LDA	r0x118E
	AND	#0x08
	JZ	_00328_DS_
;	;.line	219; "MS311AS110.c"	getLastID();
	CALL	_getLastID
_00321_DS_:
;	;.line	222; "MS311AS110.c"	lastID = (lastID + 1) & 3;
	LDA	_lastID
	INCA	
	AND	#0x03
;	;.line	223; "MS311AS110.c"	} while ((1 << lastID) & MS110_VOICE_DIS);
	STA	_lastID
	CLRA	
	STA	STK00
	INCA	
	STA	_PTRCL
	LDA	_lastID
	CALL	__shl_short
	LDA	_PTRCL
	STA	r0x118F
	LDA	#0x0a
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	r0x118F
	STA	r0x118F
	CLRA	
	AND	STK00
	ORA	r0x118F
	JNZ	_00321_DS_
;	;.line	224; "MS311AS110.c"	songID = lastID;
	LDA	_lastID
	STA	r0x118D
_00328_DS_:
;	;.line	226; "MS311AS110.c"	if (!startPlay(songID))
	LDA	r0x118D
	CALL	_startPlay
	JNZ	_00330_DS_
;	;.line	228; "MS311AS110.c"	key_code = 0;
	CLRA	
	STA	_key_code
;	;.line	229; "MS311AS110.c"	return;
	JMP	_00344_DS_
_00330_DS_:
;	;.line	231; "MS311AS110.c"	system_state = songID | 0x10;
	LDA	r0x118D
	ORA	#0x10
	STA	_system_state
;	;.line	233; "MS311AS110.c"	led_timer = LED_TIMER_RELOAD;
	LDA	#0x29
	STA	_led_timer
;	;.line	234; "MS311AS110.c"	if (MS110_LED0OPT & MS110_LED_PLAY_ON)
	LDA	#0x05
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	SHR	
	JNC	_00332_DS_
;	;.line	236; "MS311AS110.c"	LED0_ON;
	CALL	_led0_on
_00332_DS_:
;	;.line	238; "MS311AS110.c"	if (MS110_LED1OPT & MS110_LED_PLAY_ON)
	LDA	#0x07
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	SHR	
	JNC	_00334_DS_
;	;.line	240; "MS311AS110.c"	LED1_ON;
	CALL	_led1_on
_00334_DS_:
;	;.line	242; "MS311AS110.c"	key_code = 0;
	CLRA	
	STA	_key_code
;	;.line	243; "MS311AS110.c"	return;
	JMP	_00344_DS_
_00336_DS_:
;	;.line	245; "MS311AS110.c"	if (!sleep_timer)
	LDA	_sleep_timer
	JNZ	_00342_DS_
;	;.line	247; "MS311AS110.c"	unsigned char k = (IO & 0x8f);
	LDA	_IO
	AND	#0x8f
	STA	r0x118D
;	;.line	248; "MS311AS110.c"	if (MS110_USE3V || (k != 0x8f))
	CLRA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x02
	JNZ	_00337_DS_
	LDA	r0x118D
	ADD	#0x71
	JZ	_00338_DS_
_00337_DS_:
;	;.line	249; "MS311AS110.c"	api_normal_sleep(0x8f, k ^ 0x8f, 1);
	LDA	#0x8f
	XOR	r0x118D
	STA	r0x118D
	LDA	#0x01
	STA	_api_normal_sleep_STK01
	LDA	r0x118D
	STA	_api_normal_sleep_STK00
	LDA	#0x8f
	CALL	_api_normal_sleep
	JMP	_00339_DS_
_00338_DS_:
;	;.line	252; "MS311AS110.c"	api_enter_dsleep_mode();
	CALL	_api_enter_dsleep_mode
_00339_DS_:
;	;.line	254; "MS311AS110.c"	sleep_timer = TIME_TO_SLEEP;
	LDA	#0x05
	STA	_sleep_timer
	JMP	_00344_DS_
_00342_DS_:
;	;.line	258; "MS311AS110.c"	api_enter_stdby_mode(0x0f, 0, 0); // timer will wake it  up
	CLRA	
	STA	_api_enter_stdby_mode_STK01
	STA	_api_enter_stdby_mode_STK00
	LDA	#0x0f
	CALL	_api_enter_stdby_mode
_00344_DS_:
;	;.line	260; "MS311AS110.c"	}
	RET	
; exit point of _sys_idle
	.ENDFUNC _sys_idle
.globl _startPlay

;--------------------------------------------------------
	.FUNC _startPlay:$PNUM 1:$C:__mulint:$C:__mullong:$C:_api_set_vol:$C:_api_play_start\
:$L:r0x116D:$L:r0x116F:$L:r0x116E:$L:r0x1170:$L:r0x1171\
:$L:r0x1172:$L:r0x1173:$L:r0x1174:$L:r0x1175:$L:r0x1176\

;--------------------------------------------------------
;	.line	178; "MS311AS110.c"	unsigned char startPlay(unsigned char id)
_startPlay:	;Function start
	STA	r0x116D
;	;.line	180; "MS311AS110.c"	USHORT rate = MS110_RATES[id];
	LDA	#0x02
	STA	__mulint_STK02
	CLRA	
	STA	__mulint_STK01
	LDA	r0x116D
	STA	__mulchar_STK00
	CLRA	
	CALL	__mulint
	STA	r0x116F
	LDA	STK00
	STA	r0x116E
	ADD	#0x24
	STA	r0x116D
	LDA	#0x10
	ADDC	r0x116F
	STA	r0x1170
	LDA	r0x116D
	STA	_ROMPL
	LDA	r0x1170
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1171
	LDA	@_ROMPINC
	STA	r0x1172
;	;.line	181; "MS311AS110.c"	USHORT rateadj = *MS110_RATE_ADJ;
	LDA	#0x2e
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116D
	LDA	@_ROMPINC
	STA	r0x1170
;	;.line	182; "MS311AS110.c"	if (rateadj > 2048 && rateadj < 8192)               // it should not exceed 50%
	SETB	_C
	CLRA	
	SUBB	r0x116D
	LDA	#0x08
	SUBB	r0x1170
	JC	_00292_DS_
	CLRB	_C
	LDA	r0x1170
	ADDC	#0xe0
	JC	_00292_DS_
;	;.line	183; "MS311AS110.c"	rate = (((unsigned long)rate) * rateadj) >> 12; // 4096 is 1.0
	LDA	r0x116D
	STA	__mullong_STK06
	LDA	r0x1170
	STA	__mullong_STK05
	CLRA	
	STA	__mullong_STK04
	STA	__mullong_STK03
	LDA	r0x1171
	STA	__mullong_STK02
	LDA	r0x1172
	STA	__mullong_STK01
	CLRA	
	STA	__mullong_STK00
	CALL	__mullong
	STA	r0x1174
	LDA	STK01
	SWA	
	AND	#0x0f
	STA	r0x1175
	LDA	STK00
	SWA	
	PUSH	
	AND	#0xf0
	ORA	r0x1175
	STA	r0x1175
	POP	
	AND	#0x0f
	STA	r0x1176
	LDA	r0x1174
	SWA	
	PUSH	
	AND	#0xf0
	ORA	r0x1176
	STA	r0x1176
	POP	
	AND	#0x0f
	LDA	r0x1175
	STA	r0x1171
	LDA	r0x1176
	STA	r0x1172
_00292_DS_:
;	;.line	184; "MS311AS110.c"	api_set_vol(API_PAGV_DEFAULT, MS110_VOICE_VOL);
	LDA	#0x0b
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_api_set_vol_STK00
	LDA	#0x3f
	CALL	_api_set_vol
;	;.line	185; "MS311AS110.c"	return api_play_start(MS110_STARTPAGES[id], MS110_ENDPAGES[id], rate, 0, API_DAOSR_HIGH);
	LDA	#0x10
	ADD	r0x116E
	STA	r0x116D
	LDA	#0x10
	ADDC	r0x116F
	STA	r0x1170
	LDA	r0x116D
	STA	_ROMPL
	LDA	r0x1170
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1173
	LDA	@_ROMPINC
	STA	r0x1174
	LDA	#0x1a
	ADD	r0x116E
	STA	r0x116E
	LDA	#0x10
	ADDC	r0x116F
	STA	r0x116F
	LDA	r0x116E
	STA	_ROMPL
	LDA	r0x116F
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116D
	LDA	@_ROMPINC
	STA	r0x1170
	LDA	#0x04
	STA	_api_play_start_STK06
	CLRA	
	STA	_api_play_start_STK05
	LDA	r0x1171
	STA	_api_play_start_STK04
	LDA	r0x1172
	STA	_api_play_start_STK03
	LDA	r0x116D
	STA	_api_play_start_STK02
	LDA	r0x1170
	STA	_api_play_start_STK01
	LDA	r0x1173
	STA	_api_play_start_STK00
	LDA	r0x1174
	CALL	_api_play_start
;	;.line	186; "MS311AS110.c"	}
	RET	
; exit point of _startPlay
	.ENDFUNC _startPlay
.globl _enter_idle_mode

;--------------------------------------------------------
	.FUNC _enter_idle_mode:$PNUM 0:$C:_api_play_stop:$C:_led0_off:$C:_led1_off\

;--------------------------------------------------------
;	.line	156; "MS311AS110.c"	api_play_stop();
_enter_idle_mode:	;Function start
	CALL	_api_play_stop
;	;.line	158; "MS311AS110.c"	if (MS110_RANDORSEQMODE && (system_state & 0x0f) != 4)
	CLRA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x18
	JZ	_00267_DS_
	LDA	#0x0f
	AND	_system_state
	XOR	#0x04
	JZ	_00267_DS_
;	;.line	160; "MS311AS110.c"	if (idInSPIFlashOffset == 0xff)
	LDA	_idInSPIFlashOffset
	INCA	
	JNC	_00265_DS_
;	;.line	163; "MS311AS110.c"	API_SPI_ERASE(0x20);
	CLRA	
	STA	_SPIH
	LDA	#0x20
	STA	_SPIM
	CLRA	
	STA	_SPIL
	INCA	
	STA	_SPIOP
	LDA	#0x02
	STA	_SPIOP
_00265_DS_:
;	;.line	167; "MS311AS110.c"	API_SPI_READ_PAGE(0x20, 1); // read page 0x2000 to 0x100~0x1FF
	CLRA	
	STA	_SPIH
	LDA	#0x20
	STA	_SPIM
	CLRA	
	STA	_SPIL
	LDA	#0x48
	STA	_SPIOP
;	;.line	168; "MS311AS110.c"	ROMPL = idInSPIFlashOffset + 1;
	LDA	_idInSPIFlashOffset
	INCA	
	STA	_ROMPL
;	;.line	169; "MS311AS110.c"	ROMPH = 0x81;
	LDA	#0x81
	STA	_ROMPH
;	;.line	170; "MS311AS110.c"	ROMPINC = lastID;
	LDA	_lastID
	STA	@_ROMPINC
;	;.line	171; "MS311AS110.c"	API_SPI_WRITE_PAGE(0x20, 1);
	CLRA	
	STA	_SPIH
	LDA	#0x20
	STA	_SPIM
	CLRA	
	STA	_SPIL
	INCA	
	STA	_SPIOP
	LDA	#0x44
	STA	_SPIOP
_00267_DS_:
;	;.line	173; "MS311AS110.c"	system_state = SYS_IDLE;
	CLRA	
	STA	_system_state
;	;.line	174; "MS311AS110.c"	LED0_OFF;
	CALL	_led0_off
;	;.line	175; "MS311AS110.c"	LED1_OFF;
	CALL	_led1_off
;	;.line	176; "MS311AS110.c"	sleep_timer = TIME_TO_SLEEP;
	LDA	#0x05
	STA	_sleep_timer
;	;.line	177; "MS311AS110.c"	}
	RET	
; exit point of _enter_idle_mode
	.ENDFUNC _enter_idle_mode
.globl _timer_routine

;--------------------------------------------------------
	.FUNC _timer_routine:$PNUM 0:$C:_key_machine\

;--------------------------------------------------------
;	.line	145; "MS311AS110.c"	if (!TOV)
_timer_routine:	;Function start
	LDC	_TOV
;	;.line	146; "MS311AS110.c"	return;
	JNC	_00259_DS_
;	;.line	147; "MS311AS110.c"	TOV = 0;
	CLRB	_TOV
;	;.line	148; "MS311AS110.c"	key_machine();
	CALL	_key_machine
;	;.line	149; "MS311AS110.c"	if (sleep_timer)
	LDA	_sleep_timer
;	;.line	150; "MS311AS110.c"	--sleep_timer;
	JZ	_00256_DS_
	DECA	
	STA	_sleep_timer
_00256_DS_:
;	;.line	151; "MS311AS110.c"	if (led_timer)
	LDA	_led_timer
;	;.line	152; "MS311AS110.c"	--led_timer;
	JZ	_00259_DS_
	DECA	
	STA	_led_timer
_00259_DS_:
;	;.line	153; "MS311AS110.c"	}
	RET	
; exit point of _timer_routine
	.ENDFUNC _timer_routine
.globl _key_machine

;--------------------------------------------------------
	.FUNC _key_machine:$PNUM 0:$C:_get_key\
:$L:r0x1168
;--------------------------------------------------------
;	.line	112; "MS311AS110.c"	k = get_key();
_key_machine:	;Function start
	CALL	_get_key
	STA	r0x1168
;	;.line	113; "MS311AS110.c"	switch (key_state)
	SETB	_C
	LDA	#0x02
	SUBB	_key_state
	JNC	_00221_DS_
	LDA	_key_state
	CALL	_00247_DS_
_00247_DS_:
	SHL	
	STA	_PTRCL
	LDAT	#_00248_DS_
	STA	_STACKL
	LDA	_PTRCL
	INCA	
	LDAT	#_00248_DS_
	STA	_STACKH
	RET	
_00248_DS_:
	.DW	#_00208_DS_
	.DW	#_00212_DS_
	.DW	#_00217_DS_
_00208_DS_:
;	;.line	116; "MS311AS110.c"	if (!key_code && k)
	LDA	_key_code
	JNZ	_00221_DS_
;	;.line	118; "MS311AS110.c"	key_code_pre = k;
	LDA	r0x1168
	JZ	_00221_DS_
	STA	_key_code_pre
;	;.line	119; "MS311AS110.c"	key_state = KEYS_DEB;
	LDA	#0x01
	STA	_key_state
;	;.line	120; "MS311AS110.c"	key_timer = KEY_WAIT;
	LDA	#0x03
	STA	_key_timer
;	;.line	122; "MS311AS110.c"	break;
	JMP	_00221_DS_
_00212_DS_:
;	;.line	124; "MS311AS110.c"	if (k != key_code_pre)
	LDA	_key_code_pre
	XOR	r0x1168
	JZ	_00214_DS_
;	;.line	126; "MS311AS110.c"	key_state = KEYS_NOKEY;
	CLRA	
	STA	_key_state
;	;.line	127; "MS311AS110.c"	break;
	JMP	_00221_DS_
_00214_DS_:
;	;.line	129; "MS311AS110.c"	if (!--key_timer)
	LDA	_key_timer
	DECA	
	STA	_key_timer
	JNZ	_00221_DS_
;	;.line	131; "MS311AS110.c"	sleep_timer = TIME_TO_SLEEP;
	LDA	#0x05
	STA	_sleep_timer
;	;.line	132; "MS311AS110.c"	key_code = key_code_pre;
	LDA	_key_code_pre
	STA	_key_code
;	;.line	133; "MS311AS110.c"	key_state = KEYS_WAIT_RELEASE;
	LDA	#0x02
	STA	_key_state
;	;.line	135; "MS311AS110.c"	break;
	JMP	_00221_DS_
_00217_DS_:
;	;.line	137; "MS311AS110.c"	if (!k)
	LDA	r0x1168
	JNZ	_00221_DS_
;	;.line	138; "MS311AS110.c"	key_state = KEYS_NOKEY;
	CLRA	
	STA	_key_state
_00221_DS_:
;	;.line	141; "MS311AS110.c"	}
	RET	
; exit point of _key_machine
	.ENDFUNC _key_machine
.globl _get_key

;--------------------------------------------------------
	.FUNC _get_key:$PNUM 0:$L:r0x1162
;--------------------------------------------------------
;	.line	96; "MS311AS110.c"	unsigned char k = (IO | MS110_VOICE_DIS) & 0x0f;
_get_key:	;Function start
	LDA	#0x0a
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	ORA	_IO
	AND	#0x0f
	STA	r0x1162
;	;.line	97; "MS311AS110.c"	if (k == 0xe)
	XOR	#0x0e
	JNZ	_00172_DS_
;	;.line	98; "MS311AS110.c"	return KEY_CODE0;
	LDA	#0x01
	JMP	_00181_DS_
_00172_DS_:
;	;.line	99; "MS311AS110.c"	if (MS110_RANDORSEQMODE)
	CLRA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x18
	JZ	_00174_DS_
;	;.line	100; "MS311AS110.c"	return 0;
	CLRA	
	JMP	_00181_DS_
_00174_DS_:
;	;.line	101; "MS311AS110.c"	if (k == 0xd)
	LDA	r0x1162
	XOR	#0x0d
	JNZ	_00176_DS_
;	;.line	102; "MS311AS110.c"	return KEY_CODE1;
	LDA	#0x02
	JMP	_00181_DS_
_00176_DS_:
;	;.line	103; "MS311AS110.c"	if (k == 0xb)
	LDA	r0x1162
	XOR	#0x0b
	JNZ	_00178_DS_
;	;.line	104; "MS311AS110.c"	return KEY_CODE2;
	LDA	#0x03
	JMP	_00181_DS_
_00178_DS_:
;	;.line	105; "MS311AS110.c"	if (k == 0x7)
	LDA	r0x1162
	XOR	#0x07
	JNZ	_00180_DS_
;	;.line	106; "MS311AS110.c"	return KEY_CODE3;
	LDA	#0x04
	JMP	_00181_DS_
_00180_DS_:
;	;.line	107; "MS311AS110.c"	return 0;
	CLRA	
_00181_DS_:
;	;.line	108; "MS311AS110.c"	}
	RET	
; exit point of _get_key
	.ENDFUNC _get_key
.globl _getLastID

;--------------------------------------------------------
	.FUNC _getLastID:$PNUM 0
;--------------------------------------------------------
;	.line	84; "MS311AS110.c"	API_SPI_READ_PAGE(0x20, 1); // read page 0x2000 to 0x100~0x1FF
_getLastID:	;Function start
	CLRA	
	STA	_SPIH
	LDA	#0x20
	STA	_SPIM
	CLRA	
	STA	_SPIL
	LDA	#0x48
	STA	_SPIOP
;	;.line	85; "MS311AS110.c"	ROMPUW = 0x8100;            // RAM 8100
	CLRA	
	STA	_ROMPUW
	LDA	#0x81
	STA	(_ROMPUW + 1)
_00164_DS_:
;	;.line	88; "MS311AS110.c"	lastID = ROMPINC & 3;
	LDA	@_ROMPINC
	AND	#0x03
	STA	_lastID
;	;.line	89; "MS311AS110.c"	if (ROMP & 0x80)
	LDA	@_ROMP
	JMI	_00166_DS_
;	;.line	91; "MS311AS110.c"	} while (ROMPH == 0x81);
	LDA	_ROMPH
	ADD	#0x7f
	JZ	_00164_DS_
_00166_DS_:
;	;.line	92; "MS311AS110.c"	idInSPIFlashOffset = ROMPL - 1;
	LDA	_ROMPL
	DECA	
	STA	_idInSPIFlashOffset
;	;.line	93; "MS311AS110.c"	}
	RET	
; exit point of _getLastID
	.ENDFUNC _getLastID
.globl _led0_off

;--------------------------------------------------------
	.FUNC _led0_off:$PNUM 0
;--------------------------------------------------------
;	.line	79; "MS311AS110.c"	IO|=0x20;
_led0_off:	;Function start
	SETB	_IO5
;	;.line	80; "MS311AS110.c"	IODIR&=0xdf;
	LDA	#0xdf
	AND	_IODIR
	STA	_IODIR
;	;.line	81; "MS311AS110.c"	}
	RET	
; exit point of _led0_off
	.ENDFUNC _led0_off
.globl _led0_on

;--------------------------------------------------------
	.FUNC _led0_on:$PNUM 0
;--------------------------------------------------------
;	.line	74; "MS311AS110.c"	IODIR|=0x20;
_led0_on:	;Function start
	LDA	_IODIR
	ORA	#0x20
	STA	_IODIR
;	;.line	75; "MS311AS110.c"	IO&=0xdf;
	CLRB	_IO5
;	;.line	76; "MS311AS110.c"	}
	RET	
; exit point of _led0_on
	.ENDFUNC _led0_on
.globl _led1_off

;--------------------------------------------------------
	.FUNC _led1_off:$PNUM 0
;--------------------------------------------------------
;	.line	69; "MS311AS110.c"	IO|=0x40;
_led1_off:	;Function start
	SETB	_IO6
;	;.line	70; "MS311AS110.c"	IODIR&=0xbf;
	LDA	#0xbf
	AND	_IODIR
	STA	_IODIR
;	;.line	71; "MS311AS110.c"	}
	RET	
; exit point of _led1_off
	.ENDFUNC _led1_off
.globl _led1_on

;--------------------------------------------------------
	.FUNC _led1_on:$PNUM 0
;--------------------------------------------------------
;	.line	64; "MS311AS110.c"	IODIR|=0x40;
_led1_on:	;Function start
	LDA	_IODIR
	ORA	#0x40
	STA	_IODIR
;	;.line	65; "MS311AS110.c"	IO&=0xbf;
	CLRB	_IO6
;	;.line	66; "MS311AS110.c"	}
	RET	
; exit point of _led1_on
	.ENDFUNC _led1_on
.globl _init

;--------------------------------------------------------
	.FUNC _init:$PNUM 0:$C:_api_timer_on\

;--------------------------------------------------------
;	.line	23; "MS311AS110.c"	if(MS110_USE3V)
_init:	;Function start
	CLRA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x02
	JZ	_00106_DS_
;	;.line	25; "MS311AS110.c"	LVDCON=0x20; // LDO PASS, before switch to ERC
	LDA	#0x20
	STA	_LVDCON
;	;.line	26; "MS311AS110.c"	ROMPUW=0x8100;
	CLRA	
	STA	_ROMPUW
	LDA	#0x81
	STA	(_ROMPUW + 1)
;	;.line	27; "MS311AS110.c"	ROMPINC=0;
	CLRA	
	STA	@_ROMPINC
;	;.line	28; "MS311AS110.c"	ROMPINC=0;
	STA	@_ROMPINC
;	;.line	29; "MS311AS110.c"	ROMPINC=0;
	CLRA	
	STA	@_ROMPINC
;	;.line	30; "MS311AS110.c"	ROMPINC=0;
	STA	@_ROMPINC
_00106_DS_:
;	;.line	32; "MS311AS110.c"	API_USE_ERC;
	LDA	_RCCON
	AND	#0x98
	ORA	#0x03
	STA	_RCCON
;	;.line	44; "MS311AS110.c"	api_timer_on(MS110_THRLD);
	LDA	#0x09
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	CALL	_api_timer_on
;	;.line	45; "MS311AS110.c"	IO = 0xff;    // output value is high!!
	LDA	#0xff
	STA	_IO
;	;.line	46; "MS311AS110.c"	IOR = 0xff;
	STA	_IOR
;	;.line	47; "MS311AS110.c"	IODIR = 0x00; // 6,5 is LED, out low when need
	CLRA	
	STA	_IODIR
;	;.line	50; "MS311AS110.c"	if (MS110_EXTPULLHIGH(0))
	INCA	
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x04
	JZ	_00108_DS_
;	;.line	51; "MS311AS110.c"	IOR &= 0xfe;
	LDA	#0xfe
	AND	_IOR
	STA	_IOR
_00108_DS_:
;	;.line	52; "MS311AS110.c"	if (MS110_EXTPULLHIGH(1))
	LDA	#0x02
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x04
	JZ	_00110_DS_
;	;.line	53; "MS311AS110.c"	IOR &= 0xfd;
	LDA	#0xfd
	AND	_IOR
	STA	_IOR
_00110_DS_:
;	;.line	54; "MS311AS110.c"	if (MS110_EXTPULLHIGH(2))
	LDA	#0x03
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x04
	JZ	_00112_DS_
;	;.line	55; "MS311AS110.c"	IOR &= 0xfb;
	LDA	#0xfb
	AND	_IOR
	STA	_IOR
_00112_DS_:
;	;.line	56; "MS311AS110.c"	if (MS110_EXTPULLHIGH(3))
	LDA	#0x04
	STA	_ROMPL
	LDA	#0x10
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x04
	JZ	_00114_DS_
;	;.line	57; "MS311AS110.c"	IOR &= 0xf7;
	LDA	#0xf7
	AND	_IOR
	STA	_IOR
_00114_DS_:
;	;.line	58; "MS311AS110.c"	sleep_timer = TIME_TO_SLEEP;
	LDA	#0x05
	STA	_sleep_timer
;	;.line	59; "MS311AS110.c"	}
	RET	
; exit point of _init
	.ENDFUNC _init
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$init$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$led1_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$led1_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$led0_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$led0_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$getLastID$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$get_key$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$key_machine$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$timer_routine$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$enter_idle_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$startPlay$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$sys_idle$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$sys_play$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$main$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$IOR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IODIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IO$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAHL$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$init_io_state$0$0({1}SC:U),E,0,0
	;--cdb--S:G$system_state$0$0({1}SC:U),E,0,0
	;--cdb--S:G$key_state$0$0({1}SC:U),E,0,0
	;--cdb--S:G$key_code$0$0({1}SC:U),E,0,0
	;--cdb--S:G$key_code_pre$0$0({1}SC:U),E,0,0
	;--cdb--S:G$key_timer$0$0({1}SC:U),E,0,0
	;--cdb--S:G$led_timer$0$0({1}SC:U),E,0,0
	;--cdb--S:G$sleep_timer$0$0({1}SC:U),E,0,0
	;--cdb--S:G$idInSPIFlashOffset$0$0({1}SC:U),E,0,0
	;--cdb--S:G$lastID$0$0({1}SC:U),E,0,0
	;--cdb--S:LMS311AS110.get_key$k$65536$52({1}SC:U),R,0,0,[r0x1162]
	;--cdb--S:LMS311AS110.key_machine$k$65536$54({1}SC:U),R,0,0,[r0x1168]
	;--cdb--S:LMS311AS110.startPlay$id$65536$68({1}SC:U),R,0,0,[r0x116D]
	;--cdb--S:LMS311AS110.startPlay$rate$65536$69({2}SI:U),R,0,0,[r0x1171,r0x1172]
	;--cdb--S:LMS311AS110.startPlay$rateadj$65536$69({2}SI:U),R,0,0,[r0x116D,r0x1170]
	;--cdb--S:LMS311AS110.sys_idle$songID$131072$72({1}SC:U),R,0,0,[r0x118D]
	;--cdb--S:LMS311AS110.sys_idle$k$131072$81({1}SC:U),R,0,0,[r0x118D]
	;--cdb--S:LMS311AS110.sys_play$playmode$65536$85({1}SC:U),R,0,0,[r0x119F]
	;--cdb--S:LMS311AS110.sys_play$result$65536$85({1}SC:U),R,0,0,[r0x119B]
	;--cdb--S:LMS311AS110.sys_play$poweronmsg$65536$85({1}SC:U),R,0,0,[r0x119E]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_api_rec_prepare
	.globl	_api_rec_stop
	.globl	_api_set_vol
	.globl	_api_play_start
	.globl	_api_play_job
	.globl	_api_play_stop
	.globl	_api_timer_on
	.globl	_api_enter_stdby_mode
	.globl	_api_enter_dsleep_mode
	.globl	_api_normal_sleep
	.globl	__mullong
	.globl	_IOR
	.globl	_IODIR
	.globl	_IO
	.globl	_IOWK
	.globl	_IOWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERG
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_DMAL
	.globl	_DMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_IROMDLH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_RAMP0
	.globl	_RAMP0LH
	.globl	_RAMP1LH
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_DMAHL
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPLH
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPUW
	.globl	_SPIMH
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_TOV
	.globl	_init_io_state
	.globl	__mulint
	.globl	__shl_short
	.globl	__sdcc_gsinit_startup
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_init
	.globl	_led1_on
	.globl	_led1_off
	.globl	_led0_on
	.globl	_led0_off
	.globl	_getLastID
	.globl	_get_key
	.globl	_key_machine
	.globl	_timer_routine
	.globl	_enter_idle_mode
	.globl	_startPlay
	.globl	_sys_idle
	.globl	_sys_play
	.globl	_system_state
	.globl	_key_state
	.globl	_key_code
	.globl	_key_code_pre
	.globl	_key_timer
	.globl	_led_timer
	.globl	_sleep_timer
	.globl	_idInSPIFlashOffset
	.globl	_lastID
	.globl	_memcpy

	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00

.area UDATA (DATA,REL,CON)
WSAVE:	.ds 1
STK06:	.ds	 1
STK05:	.ds	 1
STK04:	.ds	 1
STK03:	.ds	 1
STK02:	.ds	 1
STK01:	.ds	 1
STK00:	.ds	 1

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_key_code:	.ds	1

	.area DSEG(DATA)
_key_code_pre:	.ds	1

	.area DSEG(DATA)
_key_timer:	.ds	1

	.area DSEG(DATA)
_led_timer:	.ds	1

	.area DSEG(DATA)
_sleep_timer:	.ds	1

	.area DSEG(DATA)
_idInSPIFlashOffset:	.ds	1

	.area DSEG(DATA)
_lastID:	.ds	1

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_MS311AS110_0	udata
r0x1162:	.ds	1
r0x1168:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x118D:	.ds	1
r0x118E:	.ds	1
r0x118F:	.ds	1
r0x1190:	.ds	1
r0x119B:	.ds	1
r0x119C:	.ds	1
r0x119D:	.ds	1
r0x119E:	.ds	1
r0x119F:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
	.globl __mulint_STK02
	.globl __mulint_STK01
	.globl __mulchar_STK00
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
	.globl _api_set_vol_STK00
	.globl _api_play_start_STK06
	.globl _api_play_start_STK05
	.globl _api_play_start_STK04
	.globl _api_play_start_STK03
	.globl _api_play_start_STK02
	.globl _api_play_start_STK01
	.globl _api_play_start_STK00
	.globl _api_rec_prepare_STK01
	.globl _api_rec_prepare_STK00
	.globl _api_enter_stdby_mode_STK01
	.globl _api_enter_stdby_mode_STK00
	.globl _api_normal_sleep_STK01
	.globl _api_normal_sleep_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	IDATAROM	(CODE) ;MS311AS110-1-data
_system_state_shadow:
	.db #0x00	; 0


	.area	IDATAROM	(CODE) ;MS311AS110-3-data
_key_state_shadow:
	.db #0x00	; 0

;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------

	.area	IDATA	(DATA) ;MS311AS110-0-data
_system_state:
	.ds	1


	.area	IDATA	(DATA) ;MS311AS110-2-data
_key_state:
	.ds	1

	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x11A2:NULL+0:-1:1
	;--cdb--W:r0x11A3:NULL+0:-1:1
	;--cdb--W:r0x11A3:NULL+0:0:0
	;--cdb--W:r0x119D:NULL+0:-1:1
	;--cdb--W:r0x119B:NULL+0:-1:1
	;--cdb--W:r0x119C:NULL+0:-1:1
	;--cdb--W:r0x119C:NULL+0:4507:0
	;--cdb--W:r0x11A0:NULL+0:0:0
	;--cdb--W:r0x11A0:NULL+0:-1:1
	;--cdb--W:r0x119D:NULL+0:0:0
	;--cdb--W:r0x118D:NULL+0:-1:1
	;--cdb--W:r0x118F:NULL+0:-1:1
	;--cdb--W:r0x1190:NULL+0:-1:1
	;--cdb--W:r0x118E:NULL+0:-1:1
	;--cdb--W:r0x118E:NULL+0:4493:0
	;--cdb--W:r0x118F:NULL+0:1:0
	;--cdb--W:r0x118F:NULL+0:0:0
	;--cdb--W:r0x1190:NULL+0:0:0
	;--cdb--W:r0x1190:NULL+0:14:0
	;--cdb--W:r0x1191:NULL+0:4494:0
	;--cdb--W:r0x1192:NULL+0:4497:0
	;--cdb--W:r0x1192:NULL+0:0:0
	;--cdb--W:r0x1192:NULL+0:-1:1
	;--cdb--W:r0x1193:NULL+0:0:0
	;--cdb--W:r0x1193:NULL+0:-1:1
	;--cdb--W:r0x1191:NULL+0:-1:1
	;--cdb--W:r0x116D:NULL+0:-1:1
	;--cdb--W:r0x1177:NULL+0:-1:1
	;--cdb--W:r0x1178:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:13:0
	;--cdb--W:r0x1173:NULL+0:4461:0
	;--cdb--W:r0x1173:NULL+0:4465:0
	;--cdb--W:r0x1173:NULL+0:14:0
	;--cdb--W:r0x1174:NULL+0:4464:0
	;--cdb--W:r0x1174:NULL+0:4466:0
	;--cdb--W:r0x1177:NULL+0:4461:0
	;--cdb--W:r0x1178:NULL+0:4464:0
	;--cdb--W:r0x1179:NULL+0:0:0
	;--cdb--W:r0x1179:NULL+0:-1:1
	;--cdb--W:r0x1175:NULL+0:0:0
	;--cdb--W:r0x117A:NULL+0:0:0
	;--cdb--W:r0x1175:NULL+0:-1:1
	;--cdb--W:r0x117A:NULL+0:-1:1
	;--cdb--W:r0x1176:NULL+0:0:0
	;--cdb--W:r0x1176:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:r0x116B:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:0:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:-1:1
	end
