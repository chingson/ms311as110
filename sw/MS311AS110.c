#include <ms311.h>
#include <ms311sphlib.h>
#include "MS311AS110.h"

BYTE system_state = 0;
BYTE key_state = 0;
volatile BYTE key_code;
BYTE key_code_pre;
BYTE key_timer;
volatile BYTE led_timer;
volatile BYTE sleep_timer;

// initially, load the configuration
//BYTE config[48];
BYTE idInSPIFlashOffset;
BYTE lastID;
#define config ((const unsigned char *)0x1000)

void init(void)
{
    //unsigned char i;

    if(MS110_USE3V)
    {
        LVDCON=0x20; // LDO PASS, before switch to ERC
        ROMPUW=0x8100;
        ROMPINC=0;
        ROMPINC=0;
        ROMPINC=0;
        ROMPINC=0;
    }
    API_USE_ERC;

    //API_SPI_READ_PAGE(0x0010, 1); // put to 0x100

    // for (i = 0; i < sizeof(config); i++)
    // {
    //     ROMPUW = 0x8100 + i;
    //     PTRCL = ROMPINC; // careful temp var will be optimized here
    //     ROMPLH = config + i;
    //     ROMP = PTRCL;
    // }

    api_timer_on(MS110_THRLD);
    IO = 0xff;    // output value is high!!
    IOR = 0xff;
    IODIR = 0x00; // 6,5 is LED, out low when need
    

    if (MS110_EXTPULLHIGH(0))
        IOR &= 0xfe;
    if (MS110_EXTPULLHIGH(1))
        IOR &= 0xfd;
    if (MS110_EXTPULLHIGH(2))
        IOR &= 0xfb;
    if (MS110_EXTPULLHIGH(3))
        IOR &= 0xf7;
    sleep_timer = TIME_TO_SLEEP;
}


void led1_on()
{
    IODIR|=0x40;
    IO&=0xbf;
}
void led1_off()
{
    IO|=0x40;
    IODIR&=0xbf;
}
void led0_on()
{
    IODIR|=0x20;
    IO&=0xdf;
}
void led0_off()
{
    IO|=0x20;
    IODIR&=0xdf;
}
void getLastID(void)
{
    API_SPI_READ_PAGE(0x20, 1); // read page 0x2000 to 0x100~0x1FF
    ROMPUW = 0x8100;            // RAM 8100
    do
    {
        lastID = ROMPINC & 3;
        if (ROMP & 0x80)
            break;
    } while (ROMPH == 0x81);
    idInSPIFlashOffset = ROMPL - 1;
}
unsigned char get_key(void)
{
    unsigned char k = (IO | MS110_VOICE_DIS) & 0x0f;
    if (k == 0xe)
        return KEY_CODE0;
    if (MS110_RANDORSEQMODE)
        return 0;
    if (k == 0xd)
        return KEY_CODE1;
    if (k == 0xb)
        return KEY_CODE2;
    if (k == 0x7)
        return KEY_CODE3;
    return 0;
}
void key_machine(void)
{
    BYTE k; // following is key machine
    k = get_key();
    switch (key_state)
    {
    case KEYS_NOKEY:
        if (!key_code && k)
        {
            key_code_pre = k;
            key_state = KEYS_DEB;
            key_timer = KEY_WAIT;
        }
        break;
    case KEYS_DEB:
        if (k != key_code_pre)
        {
            key_state = KEYS_NOKEY;
            break;
        }
        if (!--key_timer)
        {
            sleep_timer = TIME_TO_SLEEP;
            key_code = key_code_pre;
            key_state = KEYS_WAIT_RELEASE;
        }
        break;
    case KEYS_WAIT_RELEASE:
        if (!k)
            key_state = KEYS_NOKEY;
        break;
    };
}

void timer_routine(void)
{
    if (!TOV)
        return;
    TOV = 0;
    key_machine();
    if (sleep_timer)
        --sleep_timer;
    if (led_timer)
        --led_timer;
}
void enter_idle_mode(void)
{
    api_play_stop();

    if (MS110_RANDORSEQMODE && (system_state & 0x0f) != 4)
    {
        if (idInSPIFlashOffset == 0xff)
        {
            // erase the sector
            API_SPI_ERASE(0x20);
            // idInSPIFlashOffset = 0xff; //ff->00
        }

        API_SPI_READ_PAGE(0x20, 1); // read page 0x2000 to 0x100~0x1FF
        ROMPL = idInSPIFlashOffset + 1;
        ROMPH = 0x81;
        ROMPINC = lastID;
        API_SPI_WRITE_PAGE(0x20, 1);
    }
    system_state = SYS_IDLE;
    LED0_OFF;
    LED1_OFF;
    sleep_timer = TIME_TO_SLEEP;
}
unsigned char startPlay(unsigned char id)
{
    USHORT rate = MS110_RATES[id];
    USHORT rateadj = *MS110_RATE_ADJ;
    if (rateadj > 2048 && rateadj < 8192)               // it should not exceed 50%
        rate = (((unsigned long)rate) * rateadj) >> 12; // 4096 is 1.0
    api_set_vol(API_PAGV_DEFAULT, MS110_VOICE_VOL);
    return api_play_start(MS110_STARTPAGES[id], MS110_ENDPAGES[id], rate, 0, API_DAOSR_HIGH);
}
void sys_idle(void)
{

    if (key_code)
    {
        unsigned char songID = key_code - 1;
        // if no sound, we always play
        if (MS110_RANDMODE)
        {
            // no finish
            // use adc to generate rand
            getLastID();
            api_rec_prepare(API_AD_OSR128, 0xff, API_EN5K_ON);
            do
            {
                sleep_timer = 5; // 20ms enough?
                while (sleep_timer)
                {
                    timer_routine();
                    api_enter_stdby_mode(0, 0, 0);
                }
                // note that 3 bits of ADL is zero
                songID = (ADL >> 3) & 3;
            } while ((lastID == songID) || ((1 << songID) & MS110_VOICE_DIS));

            api_rec_stop(0);
            lastID = songID;
        }
        else if (MS110_SEQMODE)
        {
            // find new song ID
            // read spi first
            getLastID();
            do
            {
                lastID = (lastID + 1) & 3;
            } while ((1 << lastID) & MS110_VOICE_DIS);
            songID = lastID;
        }
        if (!startPlay(songID))
        {
            key_code = 0;
            return;
        }
        system_state = songID | 0x10;
        // if led 3 times on/off, 250/6 ~=
        led_timer = LED_TIMER_RELOAD;
        if (MS110_LED0OPT & MS110_LED_PLAY_ON)
        {
            LED0_ON;
        }
        if (MS110_LED1OPT & MS110_LED_PLAY_ON)
        {
            LED1_ON;
        }
        key_code = 0;
        return;
    }
    if (!sleep_timer)
    {
        unsigned char k = (IO & 0x8f);
        if (MS110_USE3V || (k != 0x8f))
            api_normal_sleep(0x8f, k ^ 0x8f, 1);
        else
        {
            api_enter_dsleep_mode();
        }
        sleep_timer = TIME_TO_SLEEP;
    }
    else
    {
        api_enter_stdby_mode(0x0f, 0, 0); // timer will wake it  up
    }
}
void sys_play(void)
{
    unsigned char playmode;
    unsigned char result = api_play_job();
    BYTE poweronmsg = ((system_state & 0x0f) == 4);
    if (poweronmsg)
    {
        playmode = 0;
    }
    else if (MS110_RANDORSEQMODE)
    {
        playmode = MS110_IOCTRLS[0];
    }
    else
    {
        playmode = MS110_IOCTRLS[system_state & 0x0f];
    }
    if (result == 2) // update LED
    {
        if (MS110_LED0OPT & MS110_LED_PLAY_LEV)
        {
            if (PWRH >= MS110_LED0LEV) // PWRH 16 level per 3db
            {
                LED0_ON;
            }
            else
            {
                LED0_OFF;
            }
        }
        if (MS110_LED1OPT & MS110_LED_PLAY_LEV)
        {
            if (PWRH >= MS110_LED1LEV)
            {
                LED1_ON;
            }
            else
            {
                LED1_OFF;
            }
        }
    }
    else if (!result) // play finish
    {

        api_play_stop();

        if ( (MS110_GLOBALOPT & MS110_AUTORPT ) && ((!poweronmsg) || ((MS110_VOICE_DIS&0x0f)==0x0f)))
        {
            startPlay(system_state & 0x0f);
            return;
        }
        enter_idle_mode(); // other options should enter idle mode
        return;
    }
    if (!led_timer)
    {
        led_timer = LED_TIMER_RELOAD;
        if (MS110_LED0OPT & MS110_LED_PLAY_FLASH)
            LED0_TOGGLE;
        if (MS110_LED1OPT & MS110_LED_PLAY_FLASH)
            LED1_TOGGLE;
    } // no return here
    // finally we deal with key code

    if ((playmode & MS110_LEVELHOLD) && !key_state && !(playmode & MS110_NONRETRIGGER) && !poweronmsg)
    {
        enter_idle_mode();
        return;
    }
    // finally deal with keys
    if (key_code)
    {
        // consider non-retrigger and key overwrite
        // if same key -> if non retregger, give up
        /* 	NONR=0,overw=0	nonr=0,overw=1	nonr=1,overw=0	nonr=1,overw=1
SAME IO/BUTTON	STOP play	STOP play	CONTINUE PLAY	CONTINUE PLAY
DIFFERENT IO BUTTON 	STOP play	new-voice	CONTINUE PLAY	new-Voice

*/

        if (((BYTE)(key_code - 1)) == ((BYTE)(system_state & 0x0f)) && !(MS110_RANDORSEQMODE) // rand/seq mode, view as different key
            )                                                                                 // if same key, depends on nonret
        {
            if (playmode & MS110_NONRETRIGGER)
            {
                key_code = 0;
                //enter_idle_mode();
                return; // continue play
            }
            else
            {
                enter_idle_mode();
                key_code = 0; // stop play!!
                return;
            }
        }
        else // different key
        {
            if (MS110_KEYOVERWRITE)
            {
                enter_idle_mode();
                return; // key key-code to play new voice
            }
            else if (playmode & MS110_NONRETRIGGER)
            {
                key_code = 0;
                return; // continue play
            }
            else // stop play
            {
                enter_idle_mode();
                key_code = 0;
                return; // key key-code to play new voice
            }
        }

        // if( (playmode & MS110_NONRETRIGGER ) || (!MS110_KEYOVERWRITE && (key_code-1)!=(system_state&0x0f)))
        // {
        //     key_code=0;
        //     return;
        // }

        // api_play_stop();

        // startPlay(key_code-1);
        // system_state = 0x10|(key_code-1);
        // key_code=0;
    }

    if (key_state)
        api_enter_stdby_mode(0, 0, 0);
    else
    {
        api_enter_stdby_mode(0x0f, 0, 0); // keep timer
    }
}
main()
{

    init();
    if (((IO & 0x80) == 0x0) && !(MS110_VOICE_DIS & 0x10))
    {
        while (!(IO & 0x80))
            ; // it will not last long, wait its charge ok
        if ((IO & 0x0f) == 0x0f)
        {
            if (startPlay(4))
            {
                system_state = 0x14;
                if (MS110_LED0OPT & MS110_LED_PLAY_ON)
                {
                    LED0_ON;
                }
                if (MS110_LED1OPT & MS110_LED_PLAY_ON)
                {
                    LED1_ON;
                }
            }
        }
    }

    while (1)
    {
        timer_routine();
        if (system_state == SYS_IDLE)
            sys_idle();
        else
            sys_play();
    }
}