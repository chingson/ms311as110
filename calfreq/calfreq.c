#include <ms311.h>
#include <ms311sphlib.h>
#include "calfreq.h"

int count;
#define markp ((unsigned char*)0x1000)
void main()
{
    // cs low is the length to count
    API_USE_ERC;
    IO=0xff;
    IOR=0xff;
    IODIR = 0x00; // IO6 is output
    if(((*markp) & (*(markp+1)))!=0xff)
    {
        while(1)
            api_enter_stdby_mode(0,0,0);
    }
    count = 0;
    while(!(IO&0x01)); // wait IO1, when write spiflash, it is CS
    while((IO&0x01)); // wait IO1, when write spiflash, it is CS
    api_timer_on(0);
    while(!(IO&0x01)) // check high pulse width
    {
        if(TOV)
        {
            TOV=0;
            count++;
        }
    }
    TIMERC = 0x04; // latch the high byte
    ROMPUW = 0x8100;
    ROMPINC = TIMERC; // read is the counter
    ROMPINC = count;
    ROMPINC = count>>8;
    ROMPINC = 0x55;
    ROMPINC = 0xaa;
    API_SPI_WRITE_PAGE(0x10,1);
    
    while(1);

}