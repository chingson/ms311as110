001                     ;--------------------------------------------------------
002                     ; File Created by SDCC : free open source ANSI-C Compiler
003                     ; Version 4.0.3 #bbc754a74 (MSVC)
004                     ;--------------------------------------------------------
005                     ; Port for MSHINE CPU
006                     ;--------------------------------------------------------
007                     ;	;CCFROM:"C:\work\ms311as110\calfreq"
008                     ;;	.file	"calfreq.c"
009                     	.module calfreq
010                     	;.list	p=MS311
011                     	.include "ms311sfr.def"
001                     	.area ms311sfr (SFR,OVR)
002                     .globl _IOR  	
003                     .globl _IODIR	
004                     .globl _IO	
005                     .globl _IOWK	
006                     .globl _IOWKDR 
007                     .globl _TIMERC 
008                     .globl _THRLD	
009                     .globl _RAMP0L 
010                     .globl _RAMP0LH
011                     .globl _RAMP0H 
012                     .globl _RAMP1L 
013                     .globl _RAMP1LH
014                     .globl _RAMP1H 
015                     .globl _PTRCL	
016                     .globl _PTRCH	
017                     .globl _ROMPL 	
018                     .globl _ROMPLH
019                     .globl _ROMPH 	
020                     .globl _BEEPC 	
021                     .globl _FILTERG 	
022                     .globl _ULAWC 	
023                     .globl _STACKL 
024                     .globl _STACKH 
025                     .globl _ADCON	
026                     .globl _DACON  
027                     .globl _SYSC 	
028                     .globl _SPIM	
029                     .globl _SPIMH
030                     .globl _SPIH	
031                     .globl _SPIOP	
032                     .globl _SPI_BANK
033                     .globl _ADP_IND
034                     .globl _ADP_VPL
035                     .globl _ADP_VPH
036                     .globl _ADP_VPLH
037                     .globl _ADL	
038                     .globl _ADH	
039                     .globl _ZC	
040                     .globl _ADCG	
041                     .globl _DAC_PL	
042                     .globl _DAC_PH 
043                     .globl _PAG	
044                     .globl _DMAL	
045                     .globl _DMAH	
046                     .globl _DMAHL
047                     .globl _SPIL	
048                     .globl _IOMASK 
049                     .globl _IOCMP  
050                     .globl _IOCNT  
051                     .globl _LVDCON 
052                     .globl _LVRCON 
053                     .globl _OFFSETL
054                     .globl _OFFSETLH
055                     .globl _OFFSETH
056                     .globl _RCCON  
057                     .globl _BGCON  
058                     .globl _PWRL	
059                     .globl _PWRHL	
060                     .globl _CRYPT  
061                     .globl _PWRH	
062                     .globl _IROMDL 
063                     .globl _IROMDH 
064                     .globl _IROMDLH 
065                     .globl _RECMUTE
066                     .globl _SPKC
067                     .globl _DCLAMP
068                     .globl _SSPIC
069                     .globl _SSPIL
070                     .globl _SSPIM
071                     .globl _SSPIH
072                     ; fake registers below
073                     .globl _RAMP0UW
074                     .globl _RAMP1UW
075                     .globl _ROMPUW
076                     .globl _RAMP0	
077                     .globl _RAMP0INC
078                     .globl _RAMP1  
079                     .globl _RAMP1INC
080                     .globl _RAMP1INC2
081                     .globl _ROMP	
082                     .globl _ROMPINC
083                     .globl _ROMPINC2
084                     .globl _ACC	
085                     .globl _ICE0
086                     .globl _ICE1
087                     .globl _ICE2
088                     .globl _ICE3
089                     .globl _ICE4
090                     
091                     
092                     	.MACRO BCF A,BB
093                     	LDA	#(0xff-(1<<BB))
094                     	AND	A
095                     	STA	A
096                     	.ENDM
097                     
098                     	.MACRO BSF A,B
099                     	LDA	#(1<<B)
100                     	ORA	A
101                     	STA	A
102                     	.ENDM
103                     
104                     	.MACRO BTST A,B
105                     	LDA	#(1<<B)
106                     	AND	A
107                     	.ENDM
108                     	
109                     	.MACRO BTGF A,B
110                     	LDA	#(1<<B)
111                     	XOR	A
112                     	STA	A
113                     	.ENDM
114                     
115                     
116                     
117                     
118                     
119                     
120                     
121                     
122                     
123                     
124                     
125                     
126                     
127                     
128                     
129                     
012                     	;--cdb--T:Fcalfreq$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
013                     	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
014                     	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
015                     	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
016                     	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
017                     	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
018                     	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
019                     	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
020                     	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
021                     	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
022                     	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
023                     	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
024                     	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
025                     	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
026                     	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
027                     	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
028                     	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
029                     	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
030                     	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
031                     	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
032                     	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
033                     	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
034                     	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
035                     	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
036                     	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
037                     	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
038                     	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
039                     	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
040                     	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
041                     	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
042                     	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
043                     	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
044                     	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
045                     	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
046                     	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
047                     	;--cdb--S:G$main$0$0({2}DF,SV:S),C,0,0
048                     	;--cdb--F:G$main$0$0({2}DF,SV:S),C,0,0,0,0,0
049                     ; *** BIT REGION *** (pseudo)
050                     	.area UDATA (DATA,REL,CON)
051                     ; *** BIT REGION ***END
052                     ;--------------------------------------------------------
053                     ; overlayable items in internal ram 
054                     ;--------------------------------------------------------
055                     ;	udata_ovr
056                     	.globl _main
057                     ;--------------------------------------------------------
058                     ; reset vector 
059                     ;--------------------------------------------------------
060                     	.area STARTUP	(code,REL,CON) 
061 0000 F0CA           	jmp	__sdcc_gsinit_startup
062                     .area CODE (code,REL,CON) ; calfreq-code 
063                     .globl _main
064                     
065                     ;--------------------------------------------------------
066                     	.FUNC _main:$PNUM 0:$C:_api_enter_stdby_mode:$C:_api_timer_on\
067                     :$L:r0x115E
068                     ;--------------------------------------------------------
069                     ;	.line	10; "calfreq.c"	API_USE_ERC;
070                     _main:	;Function start
071 0002 022F           	LDA	_RCCON
072 0004 7098           	AND	#0x98
073 0006 6003           	ORA	#0x03
074 0008 122F           	STA	_RCCON
075                     ;	;.line	11; "calfreq.c"	IO=0xff;
076 000A 00FF           	LDA	#0xff
077 000C 1202           	STA	_IO
078                     ;	;.line	12; "calfreq.c"	IOR=0xff;
079 000E 1200           	STA	_IOR
080                     ;	;.line	13; "calfreq.c"	IODIR = 0x00; // IO6 is output
081 0010 CE             	CLRA	
082 0011 1201           	STA	_IODIR
083                     ;	;.line	14; "calfreq.c"	if(((*markp) & (*(markp+1)))!=0xff)
084 0013 120D           	STA	_ROMPL
085 0015 0010           	LDA	#0x10
086 0017 120E           	STA	_ROMPH
087 0019 0E             	LDA	@_ROMPINC
088 001A 1303           	STA	r0x115E
089 001C 0001           	LDA	#0x01
090 001E 120D           	STA	_ROMPL
091 0020 0010           	LDA	#0x10
092 0022 120E           	STA	_ROMPH
093 0024 0E             	LDA	@_ROMPINC
094 0025 7303           	AND	r0x115E
095 0027 CC             	INCA	
096 0028 E207           	JC	_00109_DS_
097                     _00106_DS_:
098                     ;	;.line	17; "calfreq.c"	api_enter_stdby_mode(0,0,0);
099 002A CE             	CLRA	
100 002B 1C             	STA	_api_enter_stdby_mode_STK01
101 002C 1C             	STA	_api_enter_stdby_mode_STK00
102 002D B092           	CALL	_api_enter_stdby_mode
103 002F F02A           	JMP	_00106_DS_
104                     _00109_DS_:
105                     ;	;.line	19; "calfreq.c"	count = 0;
106 0031 CE             	CLRA	
107 0032 1301           	STA	_count
108 0034 1302           	STA	(_count + 1)
109                     _00110_DS_:
110                     ;	;.line	20; "calfreq.c"	while(!(IO&0x01)); // wait IO1, when write spiflash, it is CS
111 0036 0202           	LDA	_IO
112 0038 92             	SHR	
113 0039 E1FB           	JNC	_00110_DS_
114                     _00113_DS_:
115                     ;	;.line	21; "calfreq.c"	while((IO&0x01)); // wait IO1, when write spiflash, it is CS
116 003B 0202           	LDA	_IO
117 003D 92             	SHR	
118 003E E3FB           	JC	_00113_DS_
119                     ;	;.line	22; "calfreq.c"	api_timer_on(0);
120 0040 CE             	CLRA	
121 0041 B08B           	CALL	_api_timer_on
122                     _00118_DS_:
123                     ;	;.line	23; "calfreq.c"	while(!(IO&0x01)) // check high pulse width
124 0043 0202           	LDA	_IO
125 0045 92             	SHR	
126 0046 E210           	JC	_00120_DS_
127                     ;	;.line	25; "calfreq.c"	if(TOV)
128 0048 DC             	LDC	_TOV
129 0049 E1F8           	JNC	_00118_DS_
130                     ;	;.line	27; "calfreq.c"	TOV=0;
131 004B 3C             	CLRB	_TOV
132                     ;	;.line	28; "calfreq.c"	count++;
133 004C 0301           	LDA	_count
134 004E CC             	INCA	
135 004F 1301           	STA	_count
136 0051 CE             	CLRA	
137 0052 4302           	ADDC	(_count + 1)
138 0054 1302           	STA	(_count + 1)
139 0056 F043           	JMP	_00118_DS_
140                     _00120_DS_:
141                     ;	;.line	31; "calfreq.c"	TIMERC = 0x04; // latch the high byte
142 0058 0004           	LDA	#0x04
143 005A 1205           	STA	_TIMERC
144                     ;	;.line	32; "calfreq.c"	ROMPUW = 0x8100;
145 005C CE             	CLRA	
146 005D 120D           	STA	_ROMPUW
147 005F 0081           	LDA	#0x81
148 0061 120E           	STA	(_ROMPUW + 1)
149                     ;	;.line	33; "calfreq.c"	ROMPINC = TIMERC; // read is the counter
150 0063 0205           	LDA	_TIMERC
151 0065 1E             	STA	@_ROMPINC
152                     ;	;.line	34; "calfreq.c"	ROMPINC = count;
153 0066 0301           	LDA	_count
154 0068 1E             	STA	@_ROMPINC
155                     ;	;.line	35; "calfreq.c"	ROMPINC = count>>8;
156 0069 0302           	LDA	(_count + 1)
157 006B 1E             	STA	@_ROMPINC
158                     ;	;.line	36; "calfreq.c"	ROMPINC = 0x55;
159 006C 0055           	LDA	#0x55
160 006E 1E             	STA	@_ROMPINC
161                     ;	;.line	37; "calfreq.c"	ROMPINC = 0xaa;
162 006F 00AA           	LDA	#0xaa
163 0071 1E             	STA	@_ROMPINC
164                     ;	;.line	38; "calfreq.c"	API_SPI_WRITE_PAGE(0x10,1);
165 0072 CE             	CLRA	
166 0073 1217           	STA	_SPIH
167 0075 0010           	LDA	#0x10
168 0077 1216           	STA	_SPIM
169 0079 CE             	CLRA	
170 007A 1226           	STA	_SPIL
171 007C CC             	INCA	
172 007D 1218           	STA	_SPIOP
173 007F 0044           	LDA	#0x44
174 0081 1218           	STA	_SPIOP
175                     _00122_DS_:
176                     ;	;.line	40; "calfreq.c"	while(1);
177 0083 F083           	JMP	_00122_DS_
178                     ;	;.line	42; "calfreq.c"	}
179 0085 C0             	RET	
180                     ; exit point of _main
181                     	.ENDFUNC _main
182                     	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
183                     	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
184                     	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
185                     	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
186                     	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
187                     	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
188                     	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
189                     	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
190                     	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
191                     	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
192                     	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
193                     	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
194                     	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
195                     	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
196                     	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
197                     	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
198                     	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
199                     	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
200                     	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
201                     	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
202                     	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
203                     	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
204                     	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
205                     	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
206                     	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
207                     	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
208                     	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
209                     	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
210                     	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
211                     	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
212                     	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
213                     	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
214                     	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
215                     	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
216                     	;--cdb--S:G$main$0$0({2}DF,SV:S),C,0,0
217                     	;--cdb--S:G$IOR$0$0({1}SC:U),E,0,0
218                     	;--cdb--S:G$IODIR$0$0({1}SC:U),E,0,0
219                     	;--cdb--S:G$IO$0$0({1}SC:U),E,0,0
220                     	;--cdb--S:G$IOWK$0$0({1}SC:U),E,0,0
221                     	;--cdb--S:G$IOWKDR$0$0({1}SC:U),E,0,0
222                     	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
223                     	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
224                     	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
225                     	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
226                     	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
227                     	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
228                     	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
229                     	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
230                     	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
231                     	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
232                     	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
233                     	;--cdb--S:G$FILTERG$0$0({1}SC:U),E,0,0
234                     	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
235                     	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
236                     	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
237                     	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
238                     	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
239                     	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
240                     	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
241                     	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
242                     	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
243                     	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
244                     	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
245                     	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
246                     	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
247                     	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
248                     	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
249                     	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
250                     	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
251                     	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
252                     	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
253                     	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
254                     	;--cdb--S:G$DMAL$0$0({1}SC:U),E,0,0
255                     	;--cdb--S:G$DMAH$0$0({1}SC:U),E,0,0
256                     	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
257                     	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
258                     	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
259                     	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
260                     	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
261                     	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
262                     	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
263                     	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
264                     	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
265                     	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
266                     	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
267                     	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
268                     	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
269                     	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
270                     	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
271                     	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
272                     	;--cdb--S:G$IROMDLH$0$0({2}SI:U),E,0,0
273                     	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
274                     	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
275                     	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
276                     	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
277                     	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
278                     	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
279                     	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
280                     	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
281                     	;--cdb--S:G$RAMP0LH$0$0({2}DG,SC:U),E,0,0
282                     	;--cdb--S:G$RAMP1LH$0$0({2}DG,SC:U),E,0,0
283                     	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
284                     	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
285                     	;--cdb--S:G$DMAHL$0$0({2}DG,SC:U),E,0,0
286                     	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
287                     	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
288                     	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
289                     	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
290                     	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
291                     	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
292                     	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
293                     	;--cdb--S:G$RAMP0UW$0$0({2}SI:U),E,0,0
294                     	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
295                     	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
296                     	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
297                     	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
298                     	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
299                     	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
300                     	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
301                     	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
302                     	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
303                     	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
304                     	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
305                     	;--cdb--S:G$init_io_state$0$0({1}SC:U),E,0,0
306                     	;--cdb--S:G$count$0$0({2}SI:S),E,0,0
307                     ;--------------------------------------------------------
308                     ; external declarations
309                     ;--------------------------------------------------------
310                     	.globl	_api_timer_on
311                     	.globl	_api_enter_stdby_mode
312                     	.globl	_IOR
313                     	.globl	_IODIR
314                     	.globl	_IO
315                     	.globl	_IOWK
316                     	.globl	_IOWKDR
317                     	.globl	_TIMERC
318                     	.globl	_THRLD
319                     	.globl	_RAMP0L
320                     	.globl	_RAMP0H
321                     	.globl	_RAMP1L
322                     	.globl	_RAMP1H
323                     	.globl	_PTRCL
324                     	.globl	_PTRCH
325                     	.globl	_ROMPL
326                     	.globl	_ROMPH
327                     	.globl	_BEEPC
328                     	.globl	_FILTERG
329                     	.globl	_ULAWC
330                     	.globl	_STACKL
331                     	.globl	_STACKH
332                     	.globl	_ADCON
333                     	.globl	_DACON
334                     	.globl	_SYSC
335                     	.globl	_SPIM
336                     	.globl	_SPIH
337                     	.globl	_SPIOP
338                     	.globl	_SPI_BANK
339                     	.globl	_ADP_IND
340                     	.globl	_ADP_VPL
341                     	.globl	_ADP_VPH
342                     	.globl	_ADL
343                     	.globl	_ADH
344                     	.globl	_ZC
345                     	.globl	_ADCG
346                     	.globl	_DAC_PL
347                     	.globl	_DAC_PH
348                     	.globl	_PAG
349                     	.globl	_DMAL
350                     	.globl	_DMAH
351                     	.globl	_SPIL
352                     	.globl	_IOMASK
353                     	.globl	_IOCMP
354                     	.globl	_IOCNT
355                     	.globl	_LVDCON
356                     	.globl	_LVRCON
357                     	.globl	_OFFSETL
358                     	.globl	_OFFSETH
359                     	.globl	_RCCON
360                     	.globl	_BGCON
361                     	.globl	_PWRL
362                     	.globl	_CRYPT
363                     	.globl	_PWRH
364                     	.globl	_PWRHL
365                     	.globl	_IROMDL
366                     	.globl	_IROMDH
367                     	.globl	_IROMDLH
368                     	.globl	_RECMUTE
369                     	.globl	_SPKC
370                     	.globl	_DCLAMP
371                     	.globl	_SSPIC
372                     	.globl	_SSPIL
373                     	.globl	_SSPIM
374                     	.globl	_SSPIH
375                     	.globl	_RAMP0
376                     	.globl	_RAMP0LH
377                     	.globl	_RAMP1LH
378                     	.globl	_RAMP0INC
379                     	.globl	_RAMP1
380                     	.globl	_DMAHL
381                     	.globl	_RAMP1INC
382                     	.globl	_RAMP1INC2
383                     	.globl	_ROMP
384                     	.globl	_ROMPINC
385                     	.globl	_ROMPLH
386                     	.globl	_ROMPINC2
387                     	.globl	_ACC
388                     	.globl	_RAMP0UW
389                     	.globl	_RAMP1UW
390                     	.globl	_ROMPUW
391                     	.globl	_SPIMH
392                     	.globl	_OFFSETLH
393                     	.globl	_ADP_VPLH
394                     	.globl	_ICE0
395                     	.globl	_ICE1
396                     	.globl	_ICE2
397                     	.globl	_ICE3
398                     	.globl	_ICE4
399                     	.globl	_TOV
400                     	.globl	_init_io_state
401                     	.globl	__sdcc_gsinit_startup
402                     ;--------------------------------------------------------
403                     ; global -1 declarations
404                     ;--------------------------------------------------------
405                     	.globl	_count
406                     	.globl	_memcpy
407                     
408                     	.globl WSAVE
409                     	.globl STK06
410                     	.globl STK05
411                     	.globl STK04
412                     	.globl STK03
413                     	.globl STK02
414                     	.globl STK01
415                     	.globl STK00
416                     
417                     .area UDATA (DATA,REL,CON)
418                     WSAVE:	.ds 1
419                     STK06:	.ds	 1
420                     STK05:	.ds	 1
421                     STK04:	.ds	 1
422                     STK03:	.ds	 1
423                     STK02:	.ds	 1
424                     STK01:	.ds	 1
425                     STK00:	.ds	 1
426                     
427                     ;--------------------------------------------------------
428                     ; global -2 definitions
429                     ;--------------------------------------------------------
430                     	.area DSEG(DATA)
431 8001 -- --          _count:	.ds	2
432                     
433                     ;--------------------------------------------------------
434                     ; absolute symbol definitions
435                     ;--------------------------------------------------------
436                     ;--------------------------------------------------------
437                     ; compiler-defined variables
438                     ;--------------------------------------------------------
439                     	.area IDATA (DATA,REL,CON); pre-def
440                     	.area IDATAROM (CODE,REL,CON); pre-def
441                     	.area UDATA (DATA,REL,CON); pre-def
442                     	.area UDATA (DATA,REL,CON) ;UDL_calfreq_0	udata
443 8003 --             r0x115E:	.ds	1
444                     	.area DSEG (DATA); (local stack unassigned) 
445                     	.globl _api_enter_stdby_mode_STK01
446                     	.globl _api_enter_stdby_mode_STK00
447                     ;--------------------------------------------------------
448                     ; initialized data
449                     ;--------------------------------------------------------
450                     ;--------------------------------------------------------
451                     ; initialized data - mirror
452                     ;--------------------------------------------------------
453                     	;Following is optimization info, 
454                     	;xxcdbxxW:dst:src+offset:srclit:just-remove
455                     	;--cdb--W:r0x115E:NULL+0:-1:1
456                     	;--cdb--W:r0x115F:NULL+0:-1:1
457                     	end
